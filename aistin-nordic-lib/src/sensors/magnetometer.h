/**
  @file magnetometer.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <stdbool.h>
#include "twitypes.h"

bool Magnetom_init(void);
int Magnetom_readXYZ3x16(Xyz16 *xyz16);
int Magnetom_readXYZ3x12(uint8_t *buffer, int bufferSize);
bool Magnetom_getInterruptSignal(void);
