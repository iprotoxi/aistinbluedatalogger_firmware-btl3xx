/**
 * \file
 *
 * \brief Kionix KMX62 3-axis accelerometer, 3-axis magnetometer combo.
 *
 * This file contains functions for initializing and reading data
 * from a Kionix KMX62 3-axis accelerometer/magnetometer.
 *
 * Copyright (c) 2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an Atmel
 *    AVR product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#ifndef _kmx62_h_
#define _kmx62_h_

//#include "sensor.h"

#ifdef __cplusplus
extern "C" {
#endif

// TWI/I2C address
#define KMX62_TWI_ADDR          (0x0f)	
/*
Description	Address Pad	7 bit Address	Address	<7>	<6>	<5>	<4>	<3>	<2>	<1>	<0>
I2C Wr	IO_VDD	0Fh	1Eh	0	0	0	1	1	1	1	0
I2C Rd	IO_VDD	0Fh	1Fh	0	0	0	1	1	1	1	1
I2C Wr	GND	0Eh	1Ch	0	0	0	1	1	1	0	0
I2C Rd	GND	0Eh	1Dh	0	0	0	1	1	1	0	1


*/

#define KMX62_ID_VAL            (0x6F)  // device ID

#define KMX62_DATA_RESOLUTION   (16)    // full-scale data resolution, 

// Miscellaneous time values
#define RESET_DELAY_MSEC        (5)     // ram reset polling interval
//#define START_DELAY_MSEC        (20)  // startup time (@200Hz sample)

// Standard registers

#define KMX62_WHO_AM_I          (0x00)
#define KMX62_INS1              (0x01)
#define KMX62_INS2              (0x02)
#define KMX62_INS3							(0x03)
#define KMX62_INL								(0x05)
#define KMX62_ACC_XOUT_L        (0x0A)
#define KMX62_ACC_XOUT_H        (0x0B)
#define KMX62_ACC_YOUT_L        (0x0C)
#define KMX62_ACC_YOUT_H        (0x0D)
#define KMX62_ACC_ZOUT_L        (0x0E)
#define KMX62_ACC_ZOUT_H        (0x0F)
#define KMX62_TEMP_OUT_L        (0x16)
#define KMX62_TEMP_OUT_H        (0x17)
#define KMX62_MAG_XOUT_L        (0x10)
#define KMX62_MAG_XOUT_H        (0x11)
#define KMX62_MAG_YOUT_L        (0x12)
#define KMX62_MAG_YOUT_H        (0x13)
#define KMX62_MAG_ZOUT_L        (0x14)
#define KMX62_MAG_ZOUT_H        (0x15)

#define KMX62_INC1              (0x2A)
#define KMX62_INC2              (0x2B)
#define KMX62_INC3              (0x2C)
#define KMX62_INC4              (0x2D)
#define KMX62_INC5              (0x2E)

#define KMX62_AMI_CNTL1			(0x2F)
#define KMX62_AMI_CNTL2			(0x30)
#define KMX62_AMI_CNTL3			(0x31)

#define KMX62_MMI_CNTL1			(0x32)
#define KMX62_MMI_CNTL2			(0x33)
#define KMX62_MMI_CNTL3			(0x34)

#define KMX62_FFI_CNTL1			(0x35)
#define KMX62_FFI_CNTL2			(0x36)
#define KMX62_FFI_CNTL3			(0x37)

#define KMX62_ODCNTL			(0x38)
#define KMX62_CTRL_REG1         (0x39)
#define KMX62_CTRL_REG2         (0x3A)

#define KMX62_COTR				(0x3C)
#define KMX62_BUF_CTRL_1		(0x77)
#define KMX62_BUF_CTRL_2		(0x78)
#define KMX62_BUF_CTRL_3		(0x79)
#define KMX62_BUF_CLEAR 		(0x7A)
#define KMX62_STATUS_1			(0x7B)
#define KMX62_STATUS_2			(0x7C)
#define KMX62_STATUS_3			(0x7D)
#define KMX62_BUF_READ			(0x7E)


////////////////
// Register Definitions
/* INS1 Registers 0x01 */
#define KMX62_INS1_INT			(0x80)
#define KMX62_INS1_BFI			(0x40)
#define KMX62_INS1_WMI			(0x20)
#define KMX62_INS1_DRDY_A		(0x10)
#define KMX62_INS1_DRDI_M		(0x08)
#define KMX62_INS1_FFI			(0x04)
#define KMX62_INS1_AMI			(0x02)
#define KMX62_INS1_MMI			(0x01)

/* INS2 Registers 0x02 */
#define KMX62_INS2_AXNI		(0x20)
#define KMX62_INS2_AXPI		(0x10)
#define KMX62_INS2_AYNI		(0x08)
#define KMX62_INS2_AYPI		(0x04)
#define KMX62_INS2_AZNI		(0x02)
#define KMX62_INS2_AZPI		(0x01)

/* INS3 Registers 0x03 */
#define KMX62_INS3_MXNI		(0x20)
#define KMX62_INS3_MXPI		(0x10)
#define KMX62_INS3_MYNI		(0x08)
#define KMX62_INS3_MYPI		(0x04)
#define KMX62_INS3_MZNI		(0x02)
#define KMX62_INS3_MZPI		(0x01)

/* INC1 Registers 0x2A */
#define KMX62_INC1_BFI1		(0x40)
#define KMX62_INC1_WMI1		(0x20)
#define KMX62_INC1_DRDY_A1	(0x10)
#define KMX62_INC1_DRDY_M1	(0x08)
#define KMX62_INC1_FFI1		(0x04)
#define KMX62_INC1_AMI1		(0x02)
#define KMX62_INC1_MMI1		(0x01)

/* INC2 Registers 0x2B */
#define KMX62_INC2_BFI2		(0x40)
#define KMX62_INC2_WMI2		(0x20)
#define KMX62_INC2_DRDY_A2	(0x10)
#define KMX62_INC2_DRDY_M2	(0x08)
#define KMX62_INC2_FFI2		(0x04)
#define KMX62_INC2_AMI2		(0x02)
#define KMX62_INC2_MMI2		(0x01)

/* INC3 Registers 0x2C */
#define KMX62_INC3_IED2		(0x80)
//TODO MSB bit is for fifo
#define KMX62_INC3_IEA2		(0x40) 
#define KMX62_INC3_IEL2_1		(0x20)
#define KMX62_INC3_IEL2_0		(0x10)
#define KMX62_INC3_IED1		(0x08)
#define KMX62_INC3_IEA1		(0x04)

//TODO MSB bit is for fifo
#define KMX62_INC3_IEL1_1		(0x02) 
#define KMX62_INC3_IEL1_0		(0x01)

/* KMX62_INC4              (0x2D) */
#define KMX62_INC4_AXNIE	(0x20)
#define KMX62_INC4_AXPIE	(0x10)
#define KMX62_INC4_AYNIE	(0x08)
#define KMX62_INC4_AYPIE	(0x04)
#define KMX62_INC4_AZNIE	(0x02)
#define KMX62_INC4_AZPIE	(0x01)

/* KMX62_INC5              (0x2E) */
#define KMX62_INC4_MXNIE	(0x20)
#define KMX62_INC4_MXPIE	(0x10)
#define KMX62_INC4_MYNIE	(0x08)
#define KMX62_INC4_MYPIE	(0x04)
#define KMX62_INC4_MZNIE	(0x02)
#define KMX62_INC4_MZPIE	(0x01)

/*KMX62_AMI_CNTL3			(0x31) */
#define KMX62_AMI_CNTL3_AMI_EN		(0x80)
#define KMX62_AMI_CNTL3_AMIUL		(0x40)
#define KMX62_AMI_CNTL3_OAMI_0p781	(0b000)
#define KMX62_AMI_CNTL3_OAMI_1p563	(0b001)
#define KMX62_AMI_CNTL3_OAMI_3p125	(0b010)
#define KMX62_AMI_CNTL3_OAMI_6p25	(0b011)
#define KMX62_AMI_CNTL3_OAMI_12p5	(0b100)
#define KMX62_AMI_CNTL3_OAMI_25		(0b101)
#define KMX62_AMI_CNTL3_OAMI_50		(0b110)
#define KMX62_AMI_CNTL3_OAMI_100	(0b111)

/*KMX62_MMI_CNTL3			(0x34) */
#define KMX62_MMI_CNTL3_MMI_EN		(0x80)
#define KMX62_MMI_CNTL3_MMIUL		(0x40)
#define KMX62_MMI_CNTL3_OMMI_0p781	(0b000)
#define KMX62_MMI_CNTL3_OMMI_1p563	(0b001)
#define KMX62_MMI_CNTL3_OMMI_3p125	(0b010)
#define KMX62_MMI_CNTL3_OMMI_6p25	(0b011)
#define KMX62_MMI_CNTL3_OMMI_12p5	(0b100)
#define KMX62_MMI_CNTL3_OMMI_25		(0b101)
#define KMX62_MMI_CNTL3_OMMI_50		(0b110)
#define KMX62_MMI_CNTL3_OMMI_100	(0b111)

/*KMX62_FFI_CNTL3			(0x37) */
#define KMX62_FFI_CNTL3_FFI_EN		(0x80)
#define KMX62_FFI_CNTL3_FFIUL		(0x40)
#define KMX62_FFI_CNTL3_DCRM 		(0x08)
#define KMX62_FFI_CNTL3_OFFI_0p781	(0b000)
#define KMX62_FFI_CNTL3_OFFI_1p563	(0b001)
#define KMX62_FFI_CNTL3_OFFI_3p125	(0b010)
#define KMX62_FFI_CNTL3_OFFI_6p25	(0b011)
#define KMX62_FFI_CNTL3_OFFI_12p5	(0b100)
#define KMX62_FFI_CNTL3_OFFI_25		(0b101)
#define KMX62_FFI_CNTL3_OFFI_50		(0b110)
#define KMX62_FFI_CNTL3_OFFI_100	(0b111)


/* KMX62_ODCNTL            (0x2C) */
#define KMX62_ODCNTL_OSA_6p25		0x0b //(0b1011)
#define KMX62_ODCNTL_OSA_12p5		0x00 //(0b0000)
#define KMX62_ODCNTL_OSA_25  		0x01 //(0b0001)
#define KMX62_ODCNTL_OSA_50  		0x02 //(0b0010)
#define KMX62_ODCNTL_OSA_100 		0x03 //(0b0011)
#define KMX62_ODCNTL_OSA_200 		0x04 //(0b0100)
#define KMX62_ODCNTL_OSA_400 		0x05 //(0b0101)
#define KMX62_ODCNTL_OSA_800 		0x06 //(0b0110)
#define KMX62_ODCNTL_OSA_1600		0x07 //(0b0111)
#define KMX62_ODCNTL_OSA_MASK		0x0f //(0b1111)

#define KMX62_ODCNTL_OSM_6p25		(0x0b<<4) //(0b1011 << 4)
#define KMX62_ODCNTL_OSM_12p5		0 //(0b0000 << 4)
#define KMX62_ODCNTL_OSM_25  		(0x01<<4) //(0b0001 << 4)
#define KMX62_ODCNTL_OSM_50  		(0x02<<4) //(0b0010 << 4)
#define KMX62_ODCNTL_OSM_100 		(0x03<<4) //(0b0011 << 4)
#define KMX62_ODCNTL_OSM_200 		(0x04<<4) //(0b0100 << 4)
#define KMX62_ODCNTL_OSM_400 		(0x05<<4) //(0b0101 << 4)
#define KMX62_ODCNTL_OSM_800 		(0x06<<4) //(0b0110 << 4)
#define KMX62_ODCNTL_OSM_1600		(0x07<<4) //(0b0111 << 4)
#define KMX62_ODCNTL_OSM_MASK		0xf0 //(0b1111 << 4)

/* KMX62_CTRL_REG1         (0x39) */
#define KMX62_CTRL_REG1_SRST         (0x80)
#define KMX62_CTRL_REG1_STEN         (0x40)
#define KMX62_CTRL_REG1_STPOL        (0x20)
#define KMX62_CTRL_REG1_COTC         (0x08)

/* KMX62_CTRL_REG2         (0x3A) */
#define KMX62_CTRL_REG2_TEMP_EN      (0x40)
#define KMX62_CTRL_REG2_GSEL_2G      (0) //(0b00 << 4)
#define KMX62_CTRL_REG2_GSEL_4G      (0x01<<4) //(0b01 << 4)
#define KMX62_CTRL_REG2_GSEL_8G      (0x02<<4) //(0b10 << 4)
#define KMX62_CTRL_REG2_GSEL_16G     (0x03<<4) //(0b11 << 4)
#define KMX62_CTRL_REG2_GSEL_MASK    (0x03<<4) //(0b11 << 4)
#define KMX62_CTRL_REG2_RES_A4M2     0 //(0b00 << 2)
#define KMX62_CTRL_REG2_RES_A32M16   (0x01<<2) //(0b01 << 2)
#define KMX62_CTRL_REG2_RES_MAX      (0x02<<2) //(0b10 << 2)
#define KMX62_CTRL_REG2_MAG_EN       (0x02)
#define KMX62_CTRL_REG2_ACCEL_EN     (0x01)

/* KMX62_BUF_CTRL_2		(0x78) */
#define KMX62_BUF_CTRL_2_BUF_M_FIFO		(0b00 << 1)
#define KMX62_BUF_CTRL_2_BUF_M_STREAM	(0b01 << 1)
#define KMX62_BUF_CTRL_2_BUF_M_TRIGGER	(0b10 << 1)
#define KMX62_BUF_CTRL_2_BUF_M_FILO		(0b11 << 1)

/* KMX62_BUF_CTRL_3		(0x79) */
#define KMX62_BUF_CTRL_3_BFI_EN			(0x80)
#define KMX62_BUF_CTRL_3_BUF_AX			(0x40)
#define KMX62_BUF_CTRL_3_BUF_AY			(0x20)
#define KMX62_BUF_CTRL_3_BUF_AZ			(0x10)
#define KMX62_BUF_CTRL_3_BUF_MX			(0x08)
#define KMX62_BUF_CTRL_3_BUF_MY			(0x04)
#define KMX62_BUF_CTRL_3_BUF_MZ			(0x02)
#define KMX62_BUF_CTRL_3_BUF_TEMP		(0x01)


// Function Prototypes
//extern bool kmx62_init (sensor_t *, int);
		    
#ifdef __cplusplus
}
#endif

#endif /* _kmx62_h_ */
