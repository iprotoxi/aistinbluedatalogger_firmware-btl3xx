/** @file
 *
 * @brief TWI support for multiple TWI busses. Modified from the original
 * Nordic's twi_hw_master.c to support multiple busses.
 *
 * Original copyright (c) 2009 Nordic Semiconductor. All Rights Reserved.
 */


#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "twi_master2.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_assert.h"
#include "nrf_soc.h"
#include "nrf_error.h"
#include "hw_select.h" // TEST
#include "hw_init.h" // TEST
#include "debug.h"


/* Max cycles approximately to wait on RXDREADY and TXDREADY event,
 * this is optimum way instead of using timers. Negetive side is
 * this is not power aware */
//#define MAX_TIMEOUT_LOOPS             (20000UL)        /*!< MAX while loops to wait for RXD/TXD event */
#define MAX_TIMEOUT_LOOPS             (30000UL)        /*!< MAX while loops to wait for RXD/TXD event */
//#define MAX_TIMEOUT_LOOPS             (120000UL)        /*!< MAX while loops to wait for RXD/TXD event */

bool twi_master_write(
	NRF_TWI_Type *twi, uint8_t *data, uint16_t data_length,
	bool issue_stop_condition, bool restart)
{
    uint32_t timeout;// = MAX_TIMEOUT_LOOPS;   /* max loops to wait for EVENTS_TXDSENT event*/

    if (data_length == 0)
    {
        /* gently return false for requesting data of size 0 */
        return false;
    }

    twi->TXD = *data;
    if (restart) twi->TASKS_STARTTX = 1;

    while (true)
    {
				data++;
  			timeout = MAX_TIMEOUT_LOOPS;
        while(twi->EVENTS_TXDSENT == 0 && (--timeout))
        {
        }

        if (timeout == 0)
        {
			twi->EVENTS_STOPPED = 0; 
            twi->TASKS_STOP     = 1; 
            
            /* Wait until stop sequence is sent */
					  /*
					  // JNi commented this away to avoid infinite blocking
            while(twi->EVENTS_STOPPED == 0) 
            { 
                // Do nothing.
            }*/
            /* timeout before receiving event*/
					#if 0
					char str[40];
					sprintf(str, "%02x", *data);
					debug(str);
					debug("-write timeout\r\n");
					#endif
            return false;
        }

        twi->EVENTS_TXDSENT = 0; // moved by JNi - causes jams
        if (--data_length == 0)
        {
            break;
        }

        twi->TXD = *data;
    }
    
    if (issue_stop_condition) 
    { 
        twi->EVENTS_STOPPED = 0; 
        twi->TASKS_STOP = 1; 
        /* wait until stop sequence is sent and clear the EVENTS_STOPPED */ 
				/*
				// JNi commented this away to avoid infinite blocking
        while(twi->EVENTS_STOPPED == 0) 
        { 
        }*/
    }

    return true;
}

bool twi_master_read(NRF_TWI_Type *twi, uint8_t *data, uint16_t data_length, bool issue_stop_condition)
{
		//setInputHi(MCU_BRD_CTS_I2C2_SCL);
		//setInputHi(MCU_BRD_RTS_I2C2_SDA);

	#if 1
    uint32_t timeout;// = MAX_TIMEOUT_LOOPS;   /* max loops to wait for RXDREADY event*/

    if(data_length == 0)
    {
        /* gently return false for requesting data of size 0 */
        return false;
    }


    if (data_length == 1)
    {
        //NRF_PPI->CH[0].TEP = (uint32_t)&twi->TASKS_STOP;
        
        sd_ppi_channel_assign(0,
                                     &(twi->EVENTS_BB),
                                     &(twi->TASKS_STOP));
    }
    else
    {
        //NRF_PPI->CH[0].TEP = (uint32_t)&twi->TASKS_SUSPEND;
        sd_ppi_channel_assign(0,
                                     &(twi->EVENTS_BB),
                                     &(twi->TASKS_SUSPEND));
        
    }
    //NRF_PPI->CHENSET = PPI_CHENSET_CH0_Msk;
    sd_ppi_channel_enable_set(PPI_CHEN_CH0_Msk);
		twi->EVENTS_RXDREADY = 0;
    twi->TASKS_STARTRX = 1;
    while(true)
    {
				timeout = MAX_TIMEOUT_LOOPS;   /* max loops to wait for RXDREADY event*/
        
        while((twi->EVENTS_RXDREADY == 0) && (--timeout))
        {  //nrf_app_event_wait();
        }
				twi->EVENTS_RXDREADY = 0;

        if(timeout == 0)
        {
						twi->EVENTS_STOPPED = 0; 
            twi->TASKS_STOP     = 1; 
					  // JNi commented this away to avoid infinite blocking
						while(twi->EVENTS_STOPPED == 0) 
            { 
                // Do nothing.
            }
            //NRF_PPI->CHENCLR = PPI_CHENCLR_CH0_Msk;
			      sd_ppi_channel_enable_clr(PPI_CHEN_CH0_Msk);
            /* timeout before receiving event*/
						debugln("*** twi read timeout");
            return false;
        }

       
        *data++ = twi->RXD;

        /* configure PPI to stop TWI master before we get last BB event */
        if (--data_length == 1)
        {
          //  NRF_PPI->CH[0].TEP = (uint32_t)&twi->TASKS_STOP;
            sd_ppi_channel_assign(0,
                                     &(twi->EVENTS_BB),
                                     &(twi->TASKS_STOP));
        }

        if (data_length == 0)
            break;

        twi->TASKS_RESUME = 1;
    }

    /* wait until stop sequence is sent and clear the EVENTS_STOPPED */
		twi->EVENTS_STOPPED = 0;
    /*
	  // JNi commented this away to avoid infinite blocking
    while(twi->EVENTS_STOPPED == 0)
    {  //nrf_app_event_wait();
    }
		*/
   
   
  //  NRF_PPI->CHENCLR = PPI_CHENCLR_CH0_Msk;
    sd_ppi_channel_enable_clr(PPI_CHEN_CH0_Msk);
		#endif
    return true;
}

/**
 * Detects stuck slaves (SDA = 0 and SCL = 1) and tries to clear the bus.
 *
 * @return
 * @retval false Bus is stuck.
 * @retval true Bus is clear.
 */
bool twi_master_clear_bus(NRF_TWI_Type *twi, uint8_t sda, uint8_t scl)
{
	  uint32_t twi_state;
    bool bus_clear;
	  uint32_t clk_pin_config;
    uint32_t data_pin_config;
	  twi_state        = twi->ENABLE;
    twi->ENABLE = TWI_ENABLE_ENABLE_Disabled << TWI_ENABLE_ENABLE_Pos;
    
    clk_pin_config                                        =  \
            NRF_GPIO->PIN_CNF[scl];    
    NRF_GPIO->PIN_CNF[scl] =
            (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
          | (GPIO_PIN_CNF_DRIVE_S0D1     << GPIO_PIN_CNF_DRIVE_Pos)
//          | (GPIO_PIN_CNF_DRIVE_H0D1     << GPIO_PIN_CNF_DRIVE_Pos)
          | (GPIO_PIN_CNF_PULL_Pullup    << GPIO_PIN_CNF_PULL_Pos) 
//          | (GPIO_PIN_CNF_PULL_Disabled    << GPIO_PIN_CNF_PULL_Pos) 
          | (GPIO_PIN_CNF_INPUT_Connect  << GPIO_PIN_CNF_INPUT_Pos)
          | (GPIO_PIN_CNF_DIR_Output     << GPIO_PIN_CNF_DIR_Pos);    

    data_pin_config                                      = \
        NRF_GPIO->PIN_CNF[sda];
    NRF_GPIO->PIN_CNF[sda] = \
        (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
      | (GPIO_PIN_CNF_DRIVE_S0D1     << GPIO_PIN_CNF_DRIVE_Pos)
//      | (GPIO_PIN_CNF_DRIVE_H0D1     << GPIO_PIN_CNF_DRIVE_Pos) 
      | (GPIO_PIN_CNF_PULL_Pullup    << GPIO_PIN_CNF_PULL_Pos)  
//      | (GPIO_PIN_CNF_PULL_Disabled    << GPIO_PIN_CNF_PULL_Pos)  
      | (GPIO_PIN_CNF_INPUT_Connect  << GPIO_PIN_CNF_INPUT_Pos) 
      | (GPIO_PIN_CNF_DIR_Output     << GPIO_PIN_CNF_DIR_Pos);    
   
    TWI_SDA_HIGH();
    TWI_SCL_HIGH();
    TWI_DELAY();
		//TWI_SDA_INPUT(); // TEST

    if (TWI_SDA_READ() == 1 && TWI_SCL_READ() == 1)
    {
        bus_clear = true;
    }
    else
    {
        uint_fast8_t i;
        bus_clear = false;

				debugln("clocking to clear twi");
        // Clock max 18 pulses worst case scenario(9 for master to send the rest of command and 9 for slave to respond) to SCL line and wait for SDA come high
        for (i=18; i--;)
        {
            TWI_SCL_LOW();
            TWI_DELAY();
            TWI_SCL_HIGH();
            TWI_DELAY();

            if (TWI_SDA_READ() == 1)
            {
                bus_clear = true;
                break;
            }
        }
    }
    NRF_GPIO->PIN_CNF[scl] = clk_pin_config;
    NRF_GPIO->PIN_CNF[sda]  = data_pin_config;

    twi->ENABLE = twi_state;

    return bus_clear;
}

bool twi_master_init(NRF_TWI_Type *twi, uint8_t sda, uint8_t scl, int32_t freq)
{
    /* To secure correct signal levels on the pins used by the TWI
       master when the system is in OFF mode, and when the TWI master is 
       disabled, these pins must be configured in the GPIO peripheral.
    */
    uint32_t err_code=0;
    NRF_GPIO->PIN_CNF[scl] = 
        (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
      | (GPIO_PIN_CNF_DRIVE_S0D1     << GPIO_PIN_CNF_DRIVE_Pos)
//      | (GPIO_PIN_CNF_DRIVE_H0D1     << GPIO_PIN_CNF_DRIVE_Pos) 
      | (GPIO_PIN_CNF_PULL_Pullup    << GPIO_PIN_CNF_PULL_Pos)
//      | (GPIO_PIN_CNF_PULL_Disabled    << GPIO_PIN_CNF_PULL_Pos)  
      | (GPIO_PIN_CNF_INPUT_Connect  << GPIO_PIN_CNF_INPUT_Pos)
      | (GPIO_PIN_CNF_DIR_Input      << GPIO_PIN_CNF_DIR_Pos);    

    NRF_GPIO->PIN_CNF[sda] = 
        (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
      | (GPIO_PIN_CNF_DRIVE_S0D1     << GPIO_PIN_CNF_DRIVE_Pos)
//        | (GPIO_PIN_CNF_DRIVE_H0D1     << GPIO_PIN_CNF_DRIVE_Pos) 
    | (GPIO_PIN_CNF_PULL_Pullup    << GPIO_PIN_CNF_PULL_Pos)
//      | (GPIO_PIN_CNF_PULL_Disabled    << GPIO_PIN_CNF_PULL_Pos)  
      | (GPIO_PIN_CNF_INPUT_Connect  << GPIO_PIN_CNF_INPUT_Pos)
      | (GPIO_PIN_CNF_DIR_Input      << GPIO_PIN_CNF_DIR_Pos);    

    twi->EVENTS_RXDREADY = 0;
    twi->EVENTS_TXDSENT = 0;
    twi->PSELSCL = scl;
    twi->PSELSDA = sda;
    twi->FREQUENCY = freq << TWI_FREQUENCY_FREQUENCY_Pos;
    //twi->FREQUENCY = TWI_FREQUENCY_FREQUENCY_K100 << TWI_FREQUENCY_FREQUENCY_Pos;
    //twi->FREQUENCY = TWI_FREQUENCY_FREQUENCY_K400 << TWI_FREQUENCY_FREQUENCY_Pos;
		// CONFIG is changed by spi_master.c and seems to affect TWI0, too,
		// so it must be re-set here (added by JNi).
    NRF_SPI_Type *spi = NRF_SPI0;
		spi->CONFIG = 
				//(SPI_CONFIG_CPHA_Trailing << SPI_CONFIG_CPHA_Pos) |
				(SPI_CONFIG_CPHA_Leading << SPI_CONFIG_CPHA_Pos) |
				(SPI_CONFIG_CPOL_ActiveHigh << SPI_CONFIG_CPOL_Pos) |	
				//(SPI_CONFIG_CPOL_ActiveLow << SPI_CONFIG_CPOL_Pos) |	
				(SPI_CONFIG_ORDER_LsbFirst << SPI_CONFIG_ORDER_Pos);

    #if 0
    NRF_PPI->CH[0].EEP = (uint32_t)&twi->EVENTS_BB;
    NRF_PPI->CH[0].TEP = (uint32_t)&twi->TASKS_SUSPEND;
    NRF_PPI->CHENCLR = PPI_CHENCLR_CH0_Msk;
		#else
    err_code = sd_ppi_channel_assign(0,
                                     &(twi->EVENTS_BB),
                                     &(twi->TASKS_SUSPEND));
    ASSERT(err_code == NRF_SUCCESS);

    err_code = sd_ppi_channel_enable_clr(PPI_CHEN_CH0_Msk);
    ASSERT(err_code == NRF_SUCCESS);
		#endif
    
    twi->ENABLE = TWI_ENABLE_ENABLE_Enabled << TWI_ENABLE_ENABLE_Pos;

    return twi_master_clear_bus(twi, sda, scl);
}

bool twi_master_transfer(NRF_TWI_Type *twi, uint8_t address, uint8_t *data, uint16_t data_length, bool issue_stop_condition,
	uint8_t sda, uint8_t scl, bool clear_bus)
{
	#if 1
		bool clear = true;
    bool transfer_succeeded = false;
		if (data_length == 0) {
			debugln("twi: no data");
		}
		else
		{
			if (clear_bus) clear = twi_master_clear_bus(twi, sda, scl);
			if (clear)
			{
					//debugln("twi-bus clear ok");
					twi->ADDRESS = (address >> 1);

					if ((address & TWI_READ_BIT))
					{
							transfer_succeeded = twi_master_read(twi, data, data_length, issue_stop_condition);
					}
					else
					{
							transfer_succeeded = twi_master_write(
								twi, data, data_length, issue_stop_condition, clear_bus);
					}
			}
			else {
				debugln("twi-cannot-clear!");
				nrf_delay_ms(20); // this enables re-flashing
			}
		}
    return transfer_succeeded;
		#else
		return true;
		#endif
}


/*lint --flb "Leave library region" */
