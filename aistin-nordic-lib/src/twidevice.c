/**
  @file twidevice.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include "twidevice.h"
#include "aistin.h"
#include "timing.h"


int Twidevice_readXyz16time8(TwiDevice *t, Xyz16time8 *xyz16time8)
{
	if (!t->ready) return 0;
	t->readXyz16(&xyz16time8->xyz16);
	Aistin_fixEndian(&xyz16time8->xyz16);
	xyz16time8->rtcTime8 = Timing_read1000Hz8();
	return sizeof(Xyz16time8);
}
