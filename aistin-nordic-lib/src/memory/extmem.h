/**
  @file extmem.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdbool.h>
#include <stdint.h>


bool Extmem_init(void);
uint16_t Extmem_readStatus(void);
void Extmem_readDeviceID(uint8_t *id, int idSize);
uint32_t Extmem_read(uint32_t memAddress, uint8_t* data, uint32_t size);
uint32_t Extmem_write(uint32_t memAddress, uint8_t* data, uint32_t size);
uint32_t Extmem_erase(uint32_t memAddress, uint32_t size);
uint32_t Extmem_eraseAll(void);
uint32_t Extmem_getBlockSize(void);
void Extmem_deepSleep(void);
void Extmem_ultraDeepSleep(void);
void Extmem_waitMemReady(void);
void Extmem_dump(uint32_t startAddress, uint32_t size);
bool Extmem_test(uint32_t startAddress, uint32_t size);
