/**
  @file accelerometer_KX122.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/
#ifndef ACCELEROMETER_KX122_H
#define ACCELEROMETER_KX122_H


#include <stdint.h>
#include <stdbool.h>
#include "twitypes.h"


bool KX122_init(void);


#endif
