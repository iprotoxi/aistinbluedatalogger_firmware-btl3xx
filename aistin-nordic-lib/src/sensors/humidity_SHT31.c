/**
  @file humidity_SHT31.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "humidity.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "twiface.h"
#include "sht3x.h"


#ifdef HUM_SHT31


static
TwiDevice SHT31,
	*This = &SHT31;


static
uint32_t readSerialNumber(void);


/**@brief Initializes Humidity-c-module (this).
 */
bool Humidity_init()
{
	uint32_t serialNo;
	Twiface_initDevice(
		This, SHT31_NAME, SHT31_TWIUNIT, SHT31_TWIFREQ,
		SHT31_DEVADDR, SHT31_SDA, SHT31_SCL);
	// no initialization needed, just read serial number to see if it works 
	// -> prints debug and disables it if no answer received
	serialNo = readSerialNumber();
	#ifdef DEBUG
	char str[80];
	sprintf(str, "%s serialno=%08x", This->name, serialNo);
	debugln(str);
	#endif
	return true;
}


static
uint32_t readSerialNumber(void)
{
  bool ok;
  uint16_t serialNumWords[2];  
	uint8_t cmd[2];
	uint8_t data[6];
	cmd[0] = CMD_READ_SERIALNBR >> 8;
	cmd[1] = CMD_READ_SERIALNBR & 0xff;
	ok = Twiface_writeByteReg(This, cmd[0], cmd[1], true);
	nrf_delay_us(1000);
	ok |= Twiface_readData(This, data, sizeof data, true);
  if (ok) {
    serialNumWords[0] = ((uint16_t)data[0]<<8) | data[1];
    serialNumWords[1] = ((uint16_t)data[3]<<8) | data[4];
  }
	else {
		serialNumWords[0] = 0;
		serialNumWords[1] = 0;
	}
  return ((uint32_t)serialNumWords[0] << 16) | serialNumWords[1];
}


static
uint16_t calcTemperature88(uint16_t rawValue)
{
  // calculate temperature [�C]
  // T = -45 + 175 * rawValue / (2^16-1)
  // use integers only
	// note: using shift instead of dividing by 255 causes a small error
	// compared to spec
  return (int16_t)((((int32_t)rawValue * 175)>>8) - (45L<<8));
  //return 175.0f * (ft)rawValue / 65535.0f - 45.0f;
}


static 
uint16_t calcHumidity88(u16t rawValue)
{
  // calculate relative humidity [%RH]
  // RH = rawValue / (2^16-1) * 100
  // use integers only
	// note: using shift instead of dividing by 255 causes a small error
	// compared to spec
  return (int16_t)(((int32_t)rawValue * 100)>>8);
  //return 100.0f * (ft)rawValue / 65535.0f;
}


static
void readSensor(uint16_t *temperature, uint16_t *humidity)
{
  bool	ok;
  uint16_t    rawValueTemp = 0;    // temperature raw value from sensor
  uint16_t    rawValueHumi = 0;    // humidity raw value from sensor
	uint8_t cmd[2];
	uint8_t data[6];
	//cmd[0] = CMD_MEAS_POLLING_L >> 8;
	//cmd[1] = CMD_MEAS_POLLING_L & 0xff;
	cmd[0] = CMD_MEAS_CLOCKSTR_H >> 8;
	cmd[1] = CMD_MEAS_CLOCKSTR_H & 0xff;
	ok = Twiface_writeByteReg(This, cmd[0], cmd[1], true);
	nrf_delay_us(1000);
	ok |= Twiface_readData(This, data, sizeof data, true);
  // if no error, calculate temperature in �C and humidity in %RH
  if (ok) {
    rawValueTemp = ((uint16_t)data[0]<<8) | data[1];
    rawValueHumi = ((uint16_t)data[3]<<8) | data[4];
    *temperature = calcTemperature88(rawValueTemp);
    *humidity = calcHumidity88(rawValueHumi);
		#if 0
		char str[80];
		sprintf(str, "temp=%0.2f hum=%0.2f\r\n", *temperature / 256.0, *humidity / 256.0);
		debug(str);
		#endif
  }
}


int Humidity_read2x16(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 4 || !This->ready) return 0;
	uint16_t humidity;
	uint16_t temperature;
	readSensor(&temperature, &humidity);
	buffer[0] = humidity>>8;
	buffer[1] = humidity & 0xff;
	buffer[2] = temperature>>8;
	buffer[3] = temperature & 0xff;
	return 4;
}


#endif
