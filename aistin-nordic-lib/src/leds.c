/**
  @file leds.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "hw_select.h"
#include "nrf_gpio.h"
#include "leds.h"


void Leds_off()
{
    nrf_gpio_pin_clear(MCU_RED_LED);
    nrf_gpio_pin_clear(MCU_GREEN_LED);
    nrf_gpio_pin_clear(MCU_BLUE_LED);
}


static
void initLeds()
{
		nrf_gpio_cfg_output(MCU_RED_LED);
		nrf_gpio_cfg_output(MCU_GREEN_LED);
		nrf_gpio_cfg_output(MCU_BLUE_LED);
}


/**@brief Initializes Leds-c-module (this).
 */
void Leds_init()
{
		initLeds();
		Leds_off();
}


void Leds_setColor(uint8_t r, uint8_t g, uint8_t b)
{
    if (r) nrf_gpio_pin_set(MCU_RED_LED);
		else nrf_gpio_pin_clear(MCU_RED_LED);
    if (g) nrf_gpio_pin_set(MCU_GREEN_LED);
		else nrf_gpio_pin_clear(MCU_GREEN_LED);
    if (b) nrf_gpio_pin_set(MCU_BLUE_LED);
		else nrf_gpio_pin_clear(MCU_BLUE_LED);
}


/**@brief Set led color to indicate the system is booting.
 */
void Leds_indicateBooting()
{
		Leds_setColor(0x1, 0x1, 0x10);
}


/**@brief Set led color to indicate the system is running.
 */
void Leds_indicateRunning()
{
		Leds_setColor(0x0, 0x1, 0x00);
}


/**@brief Adjust led color to indicate connection is on.
 */
void Leds_indicateConnection()
{
		Leds_setColor(0x0, 0x0, 0x1);
    //setLedReg(BLUE, 0x10);
}


/**@brief Adjust led color to indicate connection is off.
 */
void Leds_indicateNoConnection()
{
		Leds_setColor(0x0, 0x0, 0x00);
    //setLedReg(BLUE, 0x00);
}
