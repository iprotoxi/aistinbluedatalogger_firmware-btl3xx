/**
  @file timing.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>

void Timing_run(void);
uint16_t Timing_read32768Hz16(void);
uint8_t Timing_read1024Hz8(void);
uint8_t Timing_read1000Hz8(void);


/* Some possible time codings, not currently used: */

// Transferring 24-bit millisecond counter with a single byte:
//
// A. values   0.. 63	= (milliseconds & 0x003f)
// B. values  64..127 = ((milliseconds >> 6) & 0x003f) | 64
// C. values 128..191 = ((milliseconds >> 12) & 0x003f) | 128
// D. values 192..255 = ((milliseconds >> 18) & 0x003f) | 192
//
// When the time elapsed after previous message is less than 64 milliseconds,
// A. value is sent. Otherwise, if the time elapsed is less than 4.096 seconds,
// B. is sent. If the time elapsed is less than 262.144 seconds, C. is sent.
//
// When moving from longer to shorter message intervals, the time is gradually
// made more accurate until the precision matches the message interval. I.e.
// after sending e.g. the C, the next message comes with B, and the next
// will have the most accurate byte, A.
//
// Because of lack of room, we define a specified message type without ID
// code in it. This requires the other end knows what to expect.


// Transferring real time just with a single byte - the 8-bit real time offset concept:
//
// F. values   0.. 99	 offset of current second in 1/100 seconds
// S. values 128..187  offset of current minute in seconds (0..59)
// M. values 192..251  offset of current hour in minutes (0..59)
// H. values 100..123  offset of current day in hours (0..23)
//
// When the time elapsed after previous message is less than a second,
// offset of the current second is sent, in 1/100 seconds. Otherwise,
// if the time elapsed is less than a minute, offset of the current minute
// is sent, in seconds. If the time is greater than a minute but less
// than an hour, offset of the current hour is sent, in minutes.
// Finally, if an hour or more is elapsed since the previously sent message,
// offset of the current day is sent, in hours.
//
// When moving from longer to shorter message intervals, the time is gradually
// made more accurate until the precision matches the message interval. I.e.
// after sending e.g. the hour offset, the next message contains the minute offset
// and so on.
