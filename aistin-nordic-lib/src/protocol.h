#ifndef Protocol_H
#define Protocol_H


#include <stdint.h>
#include "aistin.h"

void Protocol_handleMessage(uint8_t *msg, uint16_t len);
int Protocol_sendBle(AistinItem *ai);
void Protocol_sendUsb_asciiAistin(AistinItem *ai);


#endif
