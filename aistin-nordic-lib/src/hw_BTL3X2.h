#ifndef __HW_BTL3X2__
#define __HW_BTL3X2__


// MCU pin defines from schematics "Nordic_SOC_114_sch.pdf"
#define MCU_GENIO1_INT1				 0		
#define MCU_1WIRE 						 1
#define MCU_BRD_MISO 					 2
#define MCU_BRD_MOSI  				 3
#define MCU_BRD_SCLK 					 4
#define MCU_VBAT_MEAS  				 5
#define MCU_VBAT_MEAS_ADC			 ADC_CONFIG_PSEL_AnalogInput6
#define MCU_AD0 							 6
#define MCU_I2C_SCL 					 7
#define MCU_RINT 							 8
#define MCU_PCTRL 						 9
#define MCU_SEN 				 			10
#define MCU_SWSTAT 			 			11	
#define MCU_GREEN_LED 	 			12
#define MCU_SINT 				 			13
#define MCU_BMEAS 			 			14
#define MCU_RPOW 				 			15
#define MCU_DINT 				 			16
#define MCU_BRD_TX 		 			  17
#define MCU_BRD_RX 						18
#define MCU_BRD_CTS_I2C2_SCL	19 
#define MCU_BRD_RTS_I2C2_SDA 	20
#define MCU_RED_LED 					21
#define MCU_BRD_SS 						22
#define MCU_LED_BLUE_RPOW2 		23
#define MCU_USB_CTS 					25
#define MCU_USB_RTS 					24
#define MCU_USB_RX 						29
#define MCU_USB_TX 						28
#define MCU_I2C_SDA 					30

// Aliases
#define MCU_BLUE_LED					MCU_LED_BLUE_RPOW2

// Aistin Bus24 defines
#define BUS24_RPOW			MCU_RPOW
#define BUS24_SEN				MCU_SEN
#define BUS24_SDA				MCU_I2C_SDA
#define BUS24_SCL				MCU_I2C_SCL
#define BUS24_SINT			MCU_SINT
#define BUS24_SS				MCU_BRD_SS
#define BUS24_SCLK			MCU_BRD_SCLK
#define BUS24_MOSI			MCU_BRD_MOSI
#define BUS24_MISO			MCU_BRD_MISO
#define BUS24_RESET			MCU_SWDIO_NRESET
#define BUS24_1WIRE			MCU_1WIRE
#define BUS24_ADC				MCU_AD0
#define BUS24_RINT			MCU_RINT
#define BUS24_DINT			MCU_DINT
#define BUS24_RTS				MCU_BRD_RTS_I2C2_SDA
#define BUS24_CTS				MCU_BRD_CTS_I2C2_SCL
#define BUS24_RX				MCU_BRD_RX
#define BUS24_TX				MCU_BRD_TX

// External memory
#define M24M02_NAME				"Extmem_M24M02"
#define M24M02_TWIUNIT		0 // could be 1 to get more speed with sensors
#define M24M02_TWIFREQ		TWI_FREQUENCY_FREQUENCY_K400
#define M24M02_SDA 				MCU_BRD_RTS_I2C2_SDA
#define M24M02_SCL 	 			MCU_BRD_CTS_I2C2_SCL
#define M24M02_DEVADDR		0x50 // base
#define M24M02_SIZE				262144
#define M24M02_PAGESIZE		256

// Sensors

#define KMX62_NAME				"Magacc_KMX62"
#define KMX62_TWIUNIT			0
#define KMX62_TWIFREQ			TWI_FREQUENCY_FREQUENCY_K400
#define KMX62_SDA 				MCU_I2C_SDA
#define KMX62_SCL 				MCU_I2C_SCL
#define KMX62_DEVADDR		  0x0e
#define KMX62_INT_PIN			MCU_GENIO1_INT1

#define KX122_NAME				"Accel_KX122"
#define KX122_TWIUNIT			0
#define KX122_TWIFREQ			TWI_FREQUENCY_FREQUENCY_K400
#define KX122_SDA 				MCU_I2C_SDA
#define KX122_SCL 				MCU_I2C_SCL
#define KX122_DEVADDR		  0x1f
#define KX122_INT_PIN			MCU_DINT

#define KXG03_NAME				"Gyracc_KXG03"
#define KXG03_TWIUNIT			0
#define KXG03_TWIFREQ			TWI_FREQUENCY_FREQUENCY_K400
#define KXG03_SDA 				MCU_I2C_SDA
#define KXG03_SCL 				MCU_I2C_SCL
#define KXG03_DEVADDR			0x4f
#define KXG03_INT_PIN	    MCU_DINT

#define SHT31_NAME				"Humidity_SHT31"
#define SHT31_TWIUNIT			0
#define SHT31_TWIFREQ			TWI_FREQUENCY_FREQUENCY_K400
#define SHT31_SDA 				MCU_I2C_SDA
#define SHT31_SCL 	 			MCU_I2C_SCL
#define SHT31_DEVADDR			0x45

#define HTS221_NAME				"Humidity_HTS221"
#define HTS221_TWIUNIT		0
#define HTS221_TWIFREQ		TWI_FREQUENCY_FREQUENCY_K400
#define HTS221_SDA 				MCU_I2C_SDA
#define HTS221_SCL 	 			MCU_I2C_SCL
#define HTS221_DEVADDR		0x5c

#define LPS25_NAME				"Barometer_LPS25"
#define LPS25_TWIUNIT			0
#define LPS25_TWIFREQ			TWI_FREQUENCY_FREQUENCY_K400
#define LPS25_SDA 				MCU_I2C_SDA
#define LPS25_SCL 	 			MCU_I2C_SCL
#define LPS25_DEVADDR			0x5c

#define BM1383_NAME				"Barometer_BM1383"
#define BM1383_TWIUNIT			0
#define BM1383_TWIFREQ			TWI_FREQUENCY_FREQUENCY_K400
#define BM1383_SDA 				MCU_I2C_SDA
#define BM1383_SCL 	 			MCU_I2C_SCL
#define BM1383_DEVADDR			0x5d


// FIXME: there's no SPI memory
#define MEM_CS_PIN		MCU_BRD_SS
#define MEM_SCK_PIN		MCU_BRD_SCLK
#define MEM_SI_PIN		MCU_BRD_MOSI
#define MEM_SO_PIN		MCU_BRD_MISO

#define EXTMEM_SIZE 			M24M02_SIZE
#define EXTMEM_PAGESIZE		M24M02_PAGESIZE


#endif
