/**
  @file aistin_ble.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#ifndef AISTIN_BLE
#define AISTIN_BLE


#include <stdint.h>
#include "twitypes.h"


// Maximum size of aistin item's data part as stored into memory
#define AISTIN_ITEM_DATASIZE_MAX	64

// Maximum size of standard BLE packet is 20 bytes
#define AISTIN_BLE_MESSAGESIZE_MAX  20


/* Different kind of messages used in Aistin BLE messaging: */

// TWI (I2C) compatible messages
typedef
struct {
    uint8_t code; // read, write, data or scan
    uint8_t dataSize; // sizeof data[]
    uint8_t seqNr; // this is incremented between each message
    uint8_t dataID[2]; // I2C device address + register address
    uint8_t data[1]; // variable length data
} AistinCommand;

#define AISTIN_COMMANDCODE_DATA     (0<<5) // return data for "read" or "scan"
#define AISTIN_COMMANDCODE_READ     (1<<5) // read a device register
#define AISTIN_COMMANDCODE_WRITE    (2<<5) // write to device register
#define AISTIN_COMMANDCODE_SCAN     (3<<5) // scan for existing devices
// NOTE! DATA code is not used with this structure when returning data,
// if the message format defined below is used. Instead, all data is returned
// using the structure AistinBleMessageID8 defined below.


// DATA messages

// Kionix case
typedef
	struct {
		Xyz16time8 accelerationWithTime;
		Xyz16 		 magneticfield;
		Xyz16time8 rotationWithTime;
	} AistinData_B730;

// Aistin Blue case
typedef
	struct {
		uint8_t acceleration[5];
		uint8_t magneticfield[5];
		uint8_t rotation[5];
		uint8_t irTemperature[2];
	} AistinData_B710;

typedef
	struct {
		uint8_t pressure[4];
		uint8_t humidity[4];
		uint8_t temperature[4];
	} AistinData_B720;

	
/* Message with 8-bit Aistin Data ID and sequence number with a code */
typedef
struct {
		uint8_t codeAndSeqNr;
		uint8_t dataID8;
		union {
			AistinData_B710 _B710;
			AistinData_B720 _B720;
		} data;
} AistinBleMessageID8;

#define AISTIN_BLE_SEQNR_MASK		 (0x1f)
#define AISTIN_BLE_CODE_MASK		 (0xc0)
#define AISTIN_BLE_CODE_DATA     (0<<5)
#define AISTIN_BLE_CODE_READ     (1<<5)
#define AISTIN_BLE_CODE_WRITE    (2<<5)
#define AISTIN_BLE_CODE_SCAN     (3<<5)
// There's one bit left... for future use :)

	
/* Message with no data ID included, for motion packests, ID16 = 0xB730 */
// Kionix case
typedef
	struct {
		AistinData_B730 _B730;
	} AistinBleMessage_B730;
	
	
/* All aistin message DATA collected into a union */
typedef
union {
	uint8_t bytes[AISTIN_ITEM_DATASIZE_MAX];
	AistinData_B710 _B710;
	AistinData_B720 _B720;
	AistinData_B730 _B730; // Kionix case
} AistinData;


/* All BLE MESSAGES collected into a union */
typedef
union {
	AistinBleMessageID8 blue;
	AistinBleMessage_B730 kionix;
} AistinBleMessage;


#endif
