/**
  @file sensors_kionix.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "hw_select.h"
#include "debug.h"
#include "accelerom.h"
#include "magnetometer.h"
#include "gyroscope.h"
#include "humidity.h"
#include "barometer.h"
#include "accelerometer_KX122.h"


void Sensors_init(void)
{
	debugln("Initializing sensors...");
	Accelerom_init();
	Magnetom_init();
	Accelerom_setOutputRate(100);
	Gyroscope_init();
	Humidity_init();
	Barometer_init();
	KX122_init(); // TEST ONLY
}
