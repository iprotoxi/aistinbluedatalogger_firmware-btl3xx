/**
  @file sampler.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <string.h>
#include "debug.h"
#include "hw_select.h"
#include "sampler.h"
#include "accelerom.h"
#include "gyroscope.h"
#include "magnetometer.h"
#include "barometer.h"
#include "softdevice_handler.h"
#include "leds.h"
#include "nrf_delay.h"
#include "app_timer.h"
#include "ble_control.h"
#include "protocol.h"
#include "humidity.h"
#include "aistin.h"
#include "aistin_ble.h"
#include "nrf_gpio.h"
#include "ledc_lp55231.h"
#include "infrared.h"


#define APP_TIMER_PRESCALER        0 /**< Value of the RTC1 PRESCALER register. */
#define PASSIVE_INTERVAL_ms  	 	2000
#define ACTIVE_INTERVAL_ms    	 100
#define ENVIRONMENT_DECIMATOR		   1

// makes some of the code a bit cleaner...
#define withSizeof(member)		member, sizeof(member)
#define clear(member)					memset(&member, 0, sizeof(member))

static app_timer_id_t m_timer_id;
static bool Timeout;


static
void timeoutHandler(void * p_context)
{
	static int ms;
	if (Ble_control_isConnected())
		ms += ACTIVE_INTERVAL_ms;
	else
		ms += PASSIVE_INTERVAL_ms;
	while (ms >= 1000) {
		ms -= 1000;
	}
	UNUSED_PARAMETER(p_context);
	Timeout = true;
}


static
void initTimer(void)
{
	uint32_t err_code;
	// here we assume APP_TIMER_INIT is already done
	err_code = app_timer_create(&m_timer_id,
															APP_TIMER_MODE_REPEATED,
															timeoutHandler);
	APP_ERROR_CHECK(err_code);
}


static
void startTimer(int interval_ms)
{
	uint32_t err_code;
	uint32_t interval = APP_TIMER_TICKS(interval_ms, APP_TIMER_PRESCALER);
	err_code = app_timer_start(m_timer_id, interval, NULL);
	APP_ERROR_CHECK(err_code);
	Timeout = false;
}


static
void stopTimer()
{
	uint32_t err_code;
	err_code = app_timer_stop(m_timer_id);
	APP_ERROR_CHECK(err_code);
}


void restartTimer(int interval_ms)
{
	stopTimer();
	startTimer(interval_ms);
}


#ifdef SAMPLE_BY_SENSOR_INTERRUPT
static
	AistinItem ai;
static
	int8_t hasAccmagData, hasGyroData;

static
	void motionReady(bool isConnected)
	{
		Aistin_initItem(&ai, AISTIN_ID_B730_ACC_MAG_ROT_2TIME8);
		ai.dataSize = sizeof(ai.data._B730);
		Protocol_sendUsb_asciiAistin(&ai);
		if (isConnected) Protocol_sendBle(&ai);
		hasAccmagData = hasGyroData = 0;
	}

	
static
	void readMotion(bool isConnected)
	{
		static uint8_t seq;
		if (Accelerom_getInterruptSignal()) {
			Accelerom_readXYZ3x16andTime8(&ai.data._B730.accelerationWithTime);
			Magnetom_readXYZ3x16(&ai.data._B730.magneticfield);
			hasAccmagData++;
		}
		if (Gyroscope_getInterruptSignal()) {
			Gyroscope_readXYZ3x16andTime8(&ai.data._B730.rotationWithTime);
			hasGyroData++;
		}
		// We need to send the 'ai' when all data is there,
		// or the other sensor is interrupting second time:
		if (hasAccmagData && hasGyroData) {
			motionReady(isConnected);
			ai.data._B730.rotationWithTime.rtcTime8 = seq++; // remove this line to get time stamp back
		}
		else
		if (hasAccmagData > 1) {
			clear(ai.data._B730.rotationWithTime.xyz16);
			ai.data._B730.rotationWithTime.rtcTime8 = 0xff; // failed
			motionReady(isConnected);
		}
		else
		if (hasGyroData > 1) {
			clear(ai.data._B730.accelerationWithTime.xyz16);
			clear(ai.data._B730.magneticfield);
			ai.data._B730.accelerationWithTime.rtcTime8 = 0xff; // failed
			motionReady(isConnected);
		}
	}
	

#else
#ifdef AISTIN_PROTOCOL_SKIIOT
static
	void readEnvironment(bool isConnected)
	{
		int i;
		AistinItem ai;
		Aistin_initItem(&ai, AISTIN_ID_B720_PRESS_HUM);
		i  = Barometer_read2x16(withSizeof(ai.data._B720.pressure));
		i += Humidity_read2x16(withSizeof(ai.data._B720.humidity));
		ai.dataSize = i;
		Protocol_sendUsb_asciiAistin(&ai); // debug
		if (isConnected) Protocol_sendBle(&ai);
	}
#endif
	
	
static
	void checkEnvironment(bool connected)
	{
		#ifdef AISTIN_PROTOCOL_SKIIOT
		static int decimate = 0;
		if (decimate == 0) {
			nrf_delay_ms(10); // Due to Basic-For-Android slowness...
			readEnvironment(connected);
		}
		decimate++;
		if (decimate >= ENVIRONMENT_DECIMATOR) decimate = 0;
		#endif
	}
	
		
static
	void readMotion(bool isConnected)
	{
		AistinItem ai;
		if (Timeout) {
			Timeout = false;
			#ifdef AISTIN_PROTOCOL_SKIIOT
			Aistin_initItem(&ai, AISTIN_ID_B710_ACC12_MAG12_ROT12_IR);
			Accelerom_readXYZ3x12(withSizeof(ai.data._B710.acceleration));
			Magnetom_readXYZ3x12(withSizeof(ai.data._B710.magneticfield));
			Gyroscope_readXYZ3x12(withSizeof(ai.data._B710.rotation));
			Infrared_read1x16(withSizeof(ai.data._B710.irTemperature));
			ai.dataSize = sizeof(ai.data._B710);
			#else
			//Aistin_initItem(&ai, AISTIN_ID_B316_ACC_MAG_ROT);
			Aistin_initItem(&ai, AISTIN_ID_B730_ACC_MAG_ROT_2TIME8);
			Accelerom_readXYZ3x16andTime8(&ai.data._B730.accelerationWithTime);
			Magnetom_readXYZ3x16(&ai.data._B730.magneticfield);
			Gyroscope_readXYZ3x16andTime8(&ai.data._B730.rotationWithTime);
			ai.dataSize = sizeof(ai.data._B730);
			#endif
			Protocol_sendUsb_asciiAistin(&ai);
			if (isConnected) Protocol_sendBle(&ai);
			checkEnvironment(isConnected);
		}
	}
#endif


static
	void showStatus()
	{
		//if (Humidity_TempLimitExceeded) Ledc_setColor(1, 0, 0);	
		//else Ledc_setColor(0, 1, 0);
	}
	
	
#ifdef HW_BTL211
static
	void readButton()
	{
		static int ButtonDown = 0;
		if (nrf_gpio_pin_read(BUS24_ADC) == 0) {
			debug("button down\r\n");
			if (ButtonDown == 0) Ledc_init();
			ButtonDown++;
			showStatus();
			if (ButtonDown >= 5) {
				Humidity_TempLimitExceeded = false;
				for (int i = 0; i < 3; i++) {
					Ledc_setColor(1, 0, 0);
					nrf_delay_ms(500);
					Ledc_setColor(0, 1, 0);
					nrf_delay_ms(500);
				}
				ButtonDown = 0;
				Ledc_close();
			}
		}
		else {
			debug("button up\r\n");
			ButtonDown = 0;
			Ledc_close();
		}
	}
#endif
	

static
bool checkConnection()
{
	static bool wasConnected;
	if (Ble_control_isConnected()) 
	{
		Leds_setColor(0, 1, 1);
		if (!wasConnected)
		{
			wasConnected = true;
			debug("connected!\r\n");
			//debug("sending data\r\n");
			//Ledc_init();
			showStatus();
			//nrf_delay_ms(200);
			//Ledc_close();
			//Protocol_sendDeviceData();
			//debug("data sent\r\n");
			nrf_delay_ms(200);
			#ifdef SAMPLE_BY_SENSOR_INTERRUPT
			#else
			restartTimer(ACTIVE_INTERVAL_ms);
			#endif
			//sendHistory();
		}
		return true;
	}
	else
	{
		if (wasConnected)
		{
			wasConnected = false;
			debug("disconnected!\r\n");
			showStatus();
			nrf_delay_ms(200);
			#ifdef SAMPLE_BY_SENSOR_INTERRUPT
			#else
			restartTimer(PASSIVE_INTERVAL_ms);
			#endif
		}
		Leds_setColor(1, 0, 0);
		//Leds_off();
	}
	return false;
}


void Sampler_init(void)
{
	initTimer();
	#ifdef SAMPLE_BY_SENSOR_INTERRUPT
	#else
	startTimer(PASSIVE_INTERVAL_ms);
	#endif
	#ifdef HW_BTL211
	nrf_gpio_cfg_input(BUS24_ADC, NRF_GPIO_PIN_NOPULL);
	#endif
}


void Sampler_run(void)
{
	bool connected;
	connected = checkConnection();
	//readButton();
	//nrf_gpio_cfg_input(KXG03_INT_PIN, NRF_GPIO_PIN_PULLUP);
	//nrf_gpio_pin_clear(KXG03_INT_PIN);
	//if (nrf_gpio_pin_read(KXG03_INT_PIN)) debugln("g1");
	//else debug("g0");
	readMotion(connected);
	Leds_off();
}
