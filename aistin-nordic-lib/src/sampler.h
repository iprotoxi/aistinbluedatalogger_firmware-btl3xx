/**
  @file sampler.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdbool.h>


void Sampler_init(void);
void Sampler_run(void);
