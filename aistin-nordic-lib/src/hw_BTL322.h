#ifndef __HW_BTL322__
#define __HW_BTL322__


#include "hw_BTL3X2.h"


#define BLE_NAME "BTL322"


// Protocol selection
//#define AISTIN_PROTOCOL_BLUE
#define AISTIN_PROTOCOL_KIONIX


// External memory selection
//#define MEM_NONE
#define MEM_M24M02


// Sensor selections

// Accelerometer
//#define ACC_NONE					
#define ACC_KMX62				
//#define ACC_KX122				

// Magnetometer
//#define MAG_NONE					
#define	MAG_KMX62				

// Gyroscope
//#define GYR_NONE					
#define GYR_KXG03				

// Humidity sensor
#define HUM_NULL					
//#define HUM_NONE					
//#define HUM_HTS221			
//#define HUM_SHT31				

// Barometer
//#define BAR_NULL				
//#define BAR_NODE				
//#define BAR_BM1383				
#define BAR_BM1383


#endif
