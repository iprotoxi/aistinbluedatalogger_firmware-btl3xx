#ifndef HUMIDITY_H
#define HUMIDITY_H


#include <stdint.h>
#include <stdbool.h>

bool Humidity_init(void);
int Humidity_read2x16(uint8_t *buffer, int bufferSize);

//extern bool Humidity_TempLimitExceeded;


#endif
