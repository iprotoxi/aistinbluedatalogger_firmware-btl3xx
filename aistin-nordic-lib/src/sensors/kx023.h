/*
 * Copyright (C) 2011 Kionix, Inc.
 * Written by Chris Hudson <chudson@kionix.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

#ifndef __KX023_H__
#define __KX023_H__

#define KX023_G_MAX				 8000
/* OUTPUT REGISTERS */
#define KX023_XOUT_L			 0x06
#define KX023_WHO_AM_I		 0x0F
#define KX023_WHOAMI_VALUE 0x15
/* CONTROL REGISTERS */
#define KX023_STATUS_REG		0x15
#define KX023_INT_REL			  0x17
/*  Kx023 reg */
#define KX023_CTRL_REG1		0x18
#define KX023_CTRL_REG2		0x19
#define KX023_INT_CTRL1		0x1C
#define KX023_INT_CTRL2		0x1D
#define KX023_INT_CTRL3		0x1E
#define KX023_INT_CTRL4		0x1F
#define KX023_INT_CTRL5		0x20
#define KX023_INT_CTRL6		0x21
#define KX023_DATA_CTRL		0x1B

/* CONTROL REGISTER 1 BITS */
#define KX023_PC1_OFF			0x7F
#define KX023_PC1_ON			(1 << 7)

/* Data ready funtion enable bit: set during probe if using irq mode */
#define KX023_DRDYE				(1 << 5)
#define KX023_RES_12bit		(1 << 6)
#define KX023_RES_16bit		(1 << 6)
#define KX023_RES_8bit		0xBF

#define KX023_GRP4_G_4G		(1 << 3)

/* DATA CONTROL REGISTER BITS */
#define KX023_ODR0_781F		8
#define KX023_ODR12_5F		0
#define KX023_ODR25F			1
#define KX023_ODR50F			2
#define KX023_ODR100F			3
#define KX023_ODR200F			4
#define KX023_ODR400F			5
#define KX023_ODR800F			6
#define KX023_ODR1600F			7
/* INTERRUPT CONTROL REGISTER 1 BITS */
#define KX023_DRDYI1		(1 << 4)
/* CTRL bitmasks */
#define KX023_CTRL_REG2_SRST         (0x80)

#endif  /* __KX023_H__ */
