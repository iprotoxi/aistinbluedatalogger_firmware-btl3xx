/**
  @file humidity_HST221.c
  @brief Part of Aistin library
  @copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <stdio.h>
#include "nrf_delay.h"
#include "hw_select.h"
#include "twiface.h"
#include "debug.h"


#ifdef HUM_HTS221


TwiDevice HTS221;
static
TwiDevice *This = &HTS221;


// TODO: Use heater at init!


static float RHf, RHc;
static float CTf, CTc;


void HTS221_readCalibration(void)
{
	int16_t C0; //= ((parseInt(data.substr(10,2), 16) & 3)<<8) | parseInt(data.substr(4,2), 16);
    int16_t C1; //= (((parseInt(data.substr(10,2), 16)>>2)&3)<<8) | parseInt(data.substr(6,2), 16);
    int16_t T0; //= twoCompliment(parseInt(data.substr(26,2)+data.substr(24,2), 16),16);
    int16_t T1; //= twoCompliment(parseInt(data.substr(30,2)+data.substr(28,2), 16),16);
    int16_t RH0;//= parseInt(data.substr(0,2), 16);
    int16_t RH1;//= parseInt(data.substr(2,2), 16);
    int16_t H0; //= twoCompliment(parseInt(data.substr(14,2)+data.substr(12,2),16),16);
    int16_t H1; //= twoCompliment(parseInt(data.substr(22,2)+data.substr(20,2),16),16);
	
	uint8_t calibrationRegs[16];
	Twiface_readReg(This, 0xB0, calibrationRegs, 16);
	
    RH0 = calibrationRegs[0]; // e.g. 35.5
    RH1 = calibrationRegs[1]; // e.g. 70.5
    C0  = ((((uint16_t)(calibrationRegs[5] & 3))<<8) | ((uint16_t)calibrationRegs[2]));
    C1  = ((((uint16_t)(calibrationRegs[5] & 12))<<6) | ((uint16_t)calibrationRegs[3]));
    T0  = ((((uint16_t)calibrationRegs[13])<<8) | ((uint16_t)calibrationRegs[12]));
    T1  = ((((uint16_t)calibrationRegs[15])<<8) | ((uint16_t)calibrationRegs[14]));
    H0  = ((((uint16_t)calibrationRegs[7])<<8) | ((uint16_t)calibrationRegs[6])); // e.g. 4085
    H1  = ((((uint16_t)calibrationRegs[11])<<8) | ((uint16_t)calibrationRegs[10])); // e.g. 203.5
	
    // CTf = (float)((C1 - C0) / (T1 - T0) / 8);
    // CTc = (float)(-CTf * T0 + C0 / 8);
    // RHf = (float)((RH1 - RH0) / (H1 - H0) / 2);
    // RHc = (float)(-RHf * H0 + RH0 / 2);
    CTf = ((float)C1 - (float)C0) / ((float)T1 - (float)T0) / 8.;
    CTc = ((-(float)CTf * (float)T0) + (float)C0 )/ 8.;
    RHf = ((float)RH1 - (float)RH0) / ((float)H1 - (float)H0) / 2.;
    RHc = ((-(float)RHf * (float)H0) + (float)RH0 )/ 2.;
}


static
bool init(void)
{
	uint8_t ctrl_reg_values[3] = {0x84, 0x01, 0x40};
	uint8_t ok;
	ok = Twiface_writeReg(This, 0x20, ctrl_reg_values[0]);
	ok |= Twiface_writeReg(This, 0x21, ctrl_reg_values[1]);
	ok |= Twiface_writeReg(This, 0x22, ctrl_reg_values[2]);
	//debug("HTS221 running\r\n");
	return ok;
}


bool Humidity_init(void)
{
	Twiface_initDevice(
		This, HTS221_NAME, HTS221_TWIUNIT, HTS221_TWIFREQ,
		HTS221_DEVADDR, HTS221_SDA, HTS221_SCL);
	uint8_t ok = init();
	HTS221_readCalibration();
	debug("HTS221 calibration read\r\n");
	nrf_delay_ms(2000);
	return ok;
}


int16_t HTS221_readHumidityRaw(void)
{
	uint8_t sensorData[2];
	//debugln("readHumidityRaw()");
	Twiface_readReg(This, 0xA8, sensorData, 2);
	return ((int8_t)sensorData[1])*256+sensorData[0]; 
}


uint16_t HTS221_readTemperatureRaw(void)
{
	uint8_t sensorData[2];
	//debugln("readTemperatureRaw()");
	Twiface_readReg(This, 0xAA, sensorData, 2);
	return ((int8_t)sensorData[1])*256+sensorData[0]; 
}


int Humidity_read2x16(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 4 || !This->ready) return 0;
	Twiface_writeReg(This, 0x21, 0x01);
	nrf_delay_ms(1);
	int16_t rhRaw = HTS221_readHumidityRaw();
	int16_t tempRaw = HTS221_readTemperatureRaw();
	//sprintf(str, "raw : %d %d\r\n", rhRaw, tempRaw);
	//debug(str);
	float rhp = (rhRaw * RHf) + RHc;
	float celcius = (tempRaw * CTf) + CTc;
	#if 0
	char str[80];
	sprintf(str, "true: %f %f\r\n", rhp, celcius);
	debug(str);
	#endif
	uint16_t rh = rhp * 256;
	int16_t temp = celcius * 256;
	buffer[0] = rh >> 8;
	buffer[1] = rh & 0xff;
	buffer[2] = temp >> 8;
	buffer[3] = temp & 0xff;
	return 4;
}


#endif
