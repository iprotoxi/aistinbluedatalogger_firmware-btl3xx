/**
  @file battery_nrf51822.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "nrf_gpio.h"
#include "nrf51_bitfields.h"
#include "softdevice_handler.h"
#include "ble_bas.h"
#include "battery.h"
#include "app_util.h"
#include "hw_select.h"
#include "ble_control.h"
#include "nrfface.h"
#include "debug.h"


#define ADC_REFERENCE_mV				1200
#define ADC_PRESCALING						 3
#define ADC_VALUE_MAX						1023
#define ADC_RESISTOR_UP						 5
#define ADC_RESISTOR_DOWN					10
#define ADC_CAPACITOR_MULTIPLIER 	 1 // 465 // due to t = R*C
#define ADC_DIVISOR								 1 // 100


// To ease debugging, these are not local
static uint16_t bat_mV;
static uint16_t batRaw;
static uint8_t  batPercent;


static
int get_mV(int raw)
{
	return (int32_t)
		raw * ADC_REFERENCE_mV * ADC_PRESCALING
		* (ADC_RESISTOR_DOWN + ADC_RESISTOR_UP) / ADC_RESISTOR_DOWN
		* ADC_CAPACITOR_MULTIPLIER / ADC_VALUE_MAX / ADC_DIVISOR;
}


#define LIION_VHIGH		4100
#define LIION_VLOW		3200

static
int liionBatteryVoltsToPercents(int mVolts)
{
	if (mVolts >= LIION_VHIGH) return 100;
	if (mVolts <= LIION_VLOW) return 0;
	long dif = mVolts - LIION_VLOW;
	return 100 * dif / (LIION_VHIGH - LIION_VLOW);
}


/**@brief Function for handling the ADC interrupt.
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
void ADC_IRQHandler(void)
{
    if (NRF_ADC->EVENTS_END != 0)
    {
        uint32_t    err_code;
        NRF_ADC->EVENTS_END     = 0;
        batRaw		              = NRF_ADC->RESULT;
        NRF_ADC->TASKS_STOP     = 1;
				bat_mV = get_mV(batRaw);
				// close measurement switch on the board
				//nrf_gpio_pin_set(MCU_BMEAS);
				nrf_gpio_pin_clear(MCU_BMEAS);
				batPercent = liionBatteryVoltsToPercents(bat_mV);
        err_code = ble_bas_battery_level_update(&m_bas, batPercent);
        if ((err_code != NRF_SUCCESS) &&
            (err_code != NRF_ERROR_INVALID_STATE) &&
            (err_code != BLE_ERROR_NO_TX_BUFFERS) &&
            (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
        {
            APP_ERROR_HANDLER(err_code);
        }
    }
}


/**@brief Function for starting battery voltage measurement
 */
void battery_start(void)
{
		//debugln(__func__);
    //uint32_t err_code;
		//nrf_gpio_cfg_input(VBATREF_PIN, NRF_GPIO_PIN_NOPULL);
		nrf_gpio_cfg_input(MCU_VBAT_MEAS, NRF_GPIO_PIN_NOPULL);
		nrf_gpio_cfg_output(MCU_BMEAS);
    // Configure ADC
    NRF_ADC->INTENSET   = ADC_INTENSET_END_Msk;
    NRF_ADC->CONFIG     = (ADC_CONFIG_RES_10bit                        << ADC_CONFIG_RES_Pos)     |
                          (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) | 
                          //(ADC_CONFIG_INPSEL_AnalogInputNoPrescaling << ADC_CONFIG_INPSEL_Pos) | 
                          //(ADC_CONFIG_INPSEL_SupplyOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) | 
													//(ADC_CONFIG_REFSEL_SupplyOneHalfPrescaling << ADC_CONFIG_REFSEL_Pos)  |
                          (ADC_CONFIG_REFSEL_VBG                      << ADC_CONFIG_REFSEL_Pos)  |
													//(ADC_CONFIG_PSEL_Disabled                   << ADC_CONFIG_PSEL_Pos)    |
                          (MCU_VBAT_MEAS_ADC               << ADC_CONFIG_PSEL_Pos)    |
                          //ADC_CONFIG_PSEL_AnalogInput2               << ADC_CONFIG_PSEL_Pos)    |
                          (ADC_CONFIG_EXTREFSEL_None                  << ADC_CONFIG_EXTREFSEL_Pos);
    NRF_ADC->EVENTS_END = 0;
    NRF_ADC->ENABLE     = ADC_ENABLE_ENABLE_Enabled;
		// open measurement switch on the board
		nrf_gpio_pin_set(MCU_BMEAS);
    // Enable ADC interrupt
    nrfAssert(sd_nvic_ClearPendingIRQ(ADC_IRQn));
    nrfAssert(sd_nvic_SetPriority(ADC_IRQn, NRF_APP_PRIORITY_LOW));
    nrfAssert(sd_nvic_EnableIRQ(ADC_IRQn));

    NRF_ADC->EVENTS_END  = 0;    // Stop any running conversions.
    NRF_ADC->TASKS_START = 1;

		//debuglnf("bat=%d mV (%d) - %d %%", bat_mV, batRaw, batPercent);
}
