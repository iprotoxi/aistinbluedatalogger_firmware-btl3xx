/**
  @file twiface.c
  @brief Part of Aistin library
  @descr TWI i.e. Two Wire Interface more easy API.
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "accelerom.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "twiface.h"
#include "twidevice.h"


// This shouldn't be needed?
#define twiDelay()  // nrf_delay_us(10)

typedef
struct {
	NRF_TWI_Type *twi;
	TwiDevice *currentDevice;
} TwiConnection;

static
	TwiConnection TwiConnections[2];


void Twiface_init()
{
	TwiConnections[0].twi = NRF_TWI0;
	TwiConnections[0].currentDevice = 0;
	TwiConnections[1].twi = NRF_TWI1;
	TwiConnections[1].currentDevice = 0;
}


static
	bool readXyz16_notSet(Xyz16 *xyz16)
	{
		debugln(__func__);
		return false;
	}


// TODO: move this function into the twidevice.c
void Twiface_initDevice(
	TwiDevice *twiDevice, const char *name, uint8_t twiUnit,
	uint32_t busFreq, uint8_t devAddr, uint8_t sdaPin, uint8_t sclPin)
{
	twiDevice->name = name;
	twiDevice->twiUnit = twiUnit;
	twiDevice->busFreq = busFreq;
	twiDevice->devAddr = devAddr;
	twiDevice->sdaPin = sdaPin;
	twiDevice->sclPin = sclPin;
	twiDevice->ready = true;
	twiDevice->readXyz16 = readXyz16_notSet;
	#ifdef DEBUG
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	debuglnf("%-16s: devAddr=%02x twi-unit=%p sda-pin=%-2d scl-pin=%-2d freq=%d",
		twiDevice->name, twiDevice->devAddr, twic->twi, twiDevice->sdaPin,
		twiDevice->sclPin, twiDevice->busFreq);
	#endif
}


void Twiface_error(TwiDevice *twiDevice, const char *error)
{
	twiDevice->ready = false;
	#ifdef DEBUG
	debuglnf("*** %s: %s - %s", __func__, twiDevice->name, error);
	#endif
}


#if 0
bool Twiface_check(TwiDevice *twiDevice)
{
	if (!twiDevice->ready) Twiface_error(twiDevice, "not ready");
	return twiDevice->ready;
}
#endif


static
void setCurrentDevice(TwiDevice *twiDevice)
{
	//debugln(__func__);
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	TwiDevice *twid = twic->currentDevice;
	if (twid != twiDevice) {
		twic->currentDevice = twiDevice;
		if (twid->sdaPin != twiDevice->sdaPin ||
				twid->sclPin != twiDevice->sclPin ||
				twid->busFreq != twiDevice->busFreq) 
		{
			// we need to re-configure the twi unit
			twiDelay();
			if (!twi_master_init(
					twic->twi, twiDevice->sdaPin,
					twiDevice->sclPin, twiDevice->busFreq))
			{
				#ifdef DEBUG
				char str[100];
				sprintf(str, "cannot init twi, unit=%p sda-pin=%d scl-pin=%d freq=%d",
					twic->twi, twiDevice->sdaPin,
					twiDevice->sclPin, twiDevice->busFreq);
				Twiface_error(twiDevice, str);
				#endif
			}
			twiDelay();
		}
	}
}


bool Twiface_readReg(
	TwiDevice *twiDevice, uint8_t reg, uint8_t *data, uint8_t size)
{
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	NRF_TWI_Type *twi = twic->twi;
	bool err = false;
	setCurrentDevice(twiDevice);
	uint8_t devAddr2 = twiDevice->devAddr << 1;
	uint8_t sdaPin = twiDevice->sdaPin;
	uint8_t sclPin = twiDevice->sclPin;
	err |= !twi_master_transfer(twi, devAddr2, &reg, sizeof reg, true, sdaPin, sclPin, true);
	twiDelay();
	if (err) Twiface_error(twiDevice, __func__);
	if (!err) {
		err = !twi_master_transfer(twi, devAddr2 | 1, data, size, true, sdaPin, sclPin, true);
		twiDelay();
		if (err) Twiface_error(twiDevice, __func__);
	}
	return !err;
}


bool Twiface_readData(
	TwiDevice *twiDevice, uint8_t *data, uint16_t size, bool clear_bus)
{
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	bool err = false;
	setCurrentDevice(twiDevice);	
	err = !twi_master_transfer(
		twic->twi, (twiDevice->devAddr<<1) | 1, data, size,
		true, twiDevice->sdaPin, twiDevice->sclPin, clear_bus);
  twiDelay();
	if (err) Twiface_error(twiDevice, __func__);
	return !err;
}


bool Twiface_readByte(TwiDevice *twiDevice, uint8_t *byte)
{
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	bool err = false;
	setCurrentDevice(twiDevice);
	err = !twi_master_transfer(
		twic->twi, (twiDevice->devAddr<<1) | 1, byte, 1,
		true, twiDevice->sdaPin, twiDevice->sclPin, true);
  twiDelay();
	if (err) Twiface_error(twiDevice, __func__);
	return !err;
}


bool Twiface_writeByteReg(TwiDevice *twiDevice, uint8_t reg, uint8_t value, bool stop)
{
	//debugln(__func__);
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	bool err = false;
	setCurrentDevice(twiDevice);
	uint8_t data[2];
	data[0] = reg;
	data[1] = value;
	err |= !twi_master_transfer(
		twic->twi, twiDevice->devAddr<<1, data, sizeof data,
		stop, twiDevice->sdaPin, twiDevice->sclPin, true);
  twiDelay();
	if (err) Twiface_error(twiDevice, __func__);
	return !err;
}


bool Twiface_writeByte(TwiDevice *twiDevice, uint8_t byte)
{
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	bool err = false;
	setCurrentDevice(twiDevice);
	err |= !twi_master_transfer(
		twic->twi, twiDevice->devAddr<<1, &byte, sizeof byte,
		true, twiDevice->sdaPin, twiDevice->sclPin, true);
  twiDelay();
	if (err) Twiface_error(twiDevice, __func__);
	return !err;
}


bool Twiface_writeData(TwiDevice *twiDevice, uint8_t *data, uint16_t size, bool clear_bus)
{
	TwiConnection *twic = &TwiConnections[twiDevice->twiUnit];
	bool err = false;
	setCurrentDevice(twiDevice);
	err |= !twi_master_transfer(
		twic->twi, twiDevice->devAddr<<1, data, size,
		true, twiDevice->sdaPin, twiDevice->sclPin, clear_bus);
  twiDelay();
	if (err) Twiface_error(twiDevice, __func__);
	return !err;
}


#ifdef DEBUG

static
bool readReg(
	NRF_TWI_Type *twi, uint8_t sdaPin, uint8_t sclPin,
	uint8_t devAddr, uint8_t reg, uint8_t *data, uint8_t size)
{
	bool err = false;
	err |= !twi_master_transfer(twi, devAddr, &reg, sizeof reg, true, sdaPin, sclPin, true);
	twiDelay();
	if (!err) {
		err = !twi_master_transfer(twi, devAddr | 1, data, size, true, sdaPin, sclPin, true);
		twiDelay();
	}
	return !err;
}


#define SCAN_READTEST_REG  0x0f

void Twiface_scanForDevices(int sdaPin, int sclPin, uint32_t busFreq)
{
	debuglnf("%-12s: twi-unit=%p sda-pin=%d scl-pin=%d freq=%d",
		__func__, NRF_TWI0, sdaPin, sclPin, busFreq);
	if (!twi_master_init(NRF_TWI0, sdaPin, sclPin, busFreq)) {
		debugln("*** error: cannot init twi");
	}
	else {
		uint8_t value = 0x00;
		for (int addr = 0; addr < 128; addr++) {
			bool success = readReg(
				NRF_TWI0, sdaPin, sclPin, addr<<1,
				SCAN_READTEST_REG, &value, sizeof value);
			debugf("%02x%c%s", addr, success? '*': ' ', (addr & 0x0f) == 0x0f? "\r\n": " ");
		}
	}
}

#endif
