/**
  @file sampletimer.h
  @brief Part of Aistin library
	@copyright 2015-2016 iProtoXi Oy
*/

#include <stdbool.h>


void Sampletimer_init(void);
void Sampletimer_start(int interval_ms);
void Sampletimer_restart(int interval_ms);
bool Sampletimer_getTimeout(void);
