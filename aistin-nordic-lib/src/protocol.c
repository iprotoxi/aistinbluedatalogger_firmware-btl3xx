/**
  @file protocol.c
  @brief Part of Aistin library for nRF51822
	@descr Protocols for Aistin nRF51822 firmware.
	@copyright 2015 iProtoXi Oy
*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "protocol.h"
#include "ble_nus.h"
#include "nrf_delay.h"
#include "ble_control.h"
#include "softdevice_handler.h"
#include "aistin.h"
#include "hw_select.h"
#include "datalog.h"
#include "timing.h"
#include "debug.h"


static uint8_t SeqNrs[2];
#define MESSAGE_CHANNEL_BLE		0
#define MESSAGE_CHANNEL_USB		1
#define incrementSeqNr(channel) SeqNrs[channel]++


// Start logging: 04 01 01 86 07 03
// Stop logging:  04 01 01 86 07 00
void Protocol_handleMessage(uint8_t *msg, uint16_t len)
{
	if (len >= sizeof(AistinCommand) - 1) {
		debugln("*** cmd!");
		AistinCommand *ac = (AistinCommand*)msg;
		uint16_t id16 = (ac->dataID[0]<<8) | ac->dataID[1];
		if (id16 == AISTIN_ID_8607_LOGGER_CONFIG) {
			debuglnf("- log: %d", ac->data[0]);
			if (!(ac->data[0] & _AISTIN_8607bit__LOG) && Datalog_isLogging())
				Datalog_stopLogging();
			else
			if ((ac->data[0] & _AISTIN_8607bit__LOG) && !Datalog_isLogging())
				Datalog_startNewLog();
		}
	}
}


static
	void sendBle(uint8_t *data, int dataSize, uint16_t maxTimeToTry)
	{
		int tries = 0;
		int32_t err;
		uint16_t start = Timing_read32768Hz16();
		uint16_t elapsed;
		do {
			tries++;
			err = ble_nus_string_send(&m_nus, data, dataSize);
			if (err == NRF_SUCCESS) break;
			elapsed = Timing_read32768Hz16() - start;
			sd_app_evt_wait();
		} while (elapsed < maxTimeToTry);
		if (err != NRF_SUCCESS) {
			debugf("%d", tries);
		}
		else {
			if (tries > 1) debug(":");
		}
	}


#define MAX_TRY_TIME   (5L * 32768 / 1000)  // 5 ms

int Protocol_sendBle(AistinItem *ai)
{
	int err = 0;
	if (Ble_control_isConnected()) {
		AistinBleMessage msg;
		int size = ai->dataSize;
		// TODO: use different BTLE characteristic for different data types
		switch (ai->dataID16)
		{
			case AISTIN_ID_B710_ACC12_MAG12_ROT12_IR:
			case AISTIN_ID_B711_ACC12_MAG12_ROT12_IR:
			case AISTIN_ID_B720_PRESS_HUM:
			case AISTIN_ID_B721_PRESS_HUM:
				msg.blue.codeAndSeqNr = SeqNrs[MESSAGE_CHANNEL_BLE] & 31;
				if (ai->dataID16 & AISTIN_ID_0001_NOTREALTIMEbit) msg.blue.codeAndSeqNr |= 0x80;
				msg.blue.dataID8 = Aistin_getDataID8(ai->dataID16);
				memcpy(&msg.blue.data, &ai->data, size);
				size += sizeof(msg.blue.codeAndSeqNr) + sizeof(msg.blue.dataID8);
			break;
			case AISTIN_ID_B730_ACC_MAG_ROT_2TIME8:
			case AISTIN_ID_B731_ACC_MAG_ROT_2TIME8:
				memcpy(&msg.kionix._B730, &ai->data, size);
			break;
			default: ;
				/* not supported */
		}
		sendBle((uint8_t*)&msg, size, MAX_TRY_TIME);
		incrementSeqNr(MESSAGE_CHANNEL_BLE);
	}
	return err;
}


void Protocol_sendUsb_asciiAistin(AistinItem *ai)
{
	if (Debug) {
		char str[500];
		Aistin_asciiFormat(str, ai, SeqNrs[MESSAGE_CHANNEL_USB]);
		debugln(str);
		incrementSeqNr(MESSAGE_CHANNEL_USB);
	}
}
