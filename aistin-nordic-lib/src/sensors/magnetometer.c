/** @file
 *
 * @brief Magnetometer support for Runteq-iProtoXi nRF51822 firmware.
 */

#include <stdint.h>
#include "hw_select.h"


#if defined(MAG_NONE) || defined(MAG_NULL)


uint8_t Magnetom_init()
{
	return 0;
}


int Magnetom_readXYZ3x16(uint8_t *buffer, int bufferSize)
{
	#ifdef MAG_NULL
	if (bufferSize < 6) return 0;
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = 0;
	buffer[5] = 0;
	return 6;
	#else
	return 0;
	#endif
}


int Magnetom_readXYZ3x12(uint8_t *buffer, int bufferSize)
{
	#ifdef MAG_NULL
	if (bufferSize < 5) return 0;
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = 0;
	return 5;
	#else
	return 0;
	#endif
}


#endif
