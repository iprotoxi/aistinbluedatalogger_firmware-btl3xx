/**
  @file datalog.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdbool.h>
#include "aistin.h"

void Datalog_init(void);
void Datalog_log(AistinItem *ai);
void Datalog_setLogging(bool loggingOn);
bool Datalog_isLogging(void);
bool Datalog_isSendingLog(void);
bool Datalog_getNextItem(AistinItem *ai);
void Datalog_clearLog(void);
void Datalog_startNewLog(void);
void Datalog_stopLogging(void);
void Datalog_rereadLog(void);
