/**
  @file hw_init.c
  @brief Part of Aistin library for nRF51822
	@descr Board-dependent hardware initialization
	@copyright 2015 iProtoXi Oy
*/

#include "hw_select.h"
#include "hw_init.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"


void setInputHi(int pin)
{
		nrf_gpio_cfg_input(pin, NRF_GPIO_PIN_PULLUP);
}


void setInputLow(int pin)
{
		nrf_gpio_cfg_input(pin, NRF_GPIO_PIN_PULLDOWN);
}


void setInputNo(int pin)
{
		nrf_gpio_cfg_input(pin, NRF_GPIO_PIN_NOPULL);
}


void setOutputHi(int pin)
{
		nrf_gpio_cfg_output(pin);
    nrf_gpio_pin_set(pin);
}


void setOutputLow(int pin)
{
		nrf_gpio_cfg_output(pin);
    nrf_gpio_pin_clear(pin);
}


static
	void pulseRINT()
	{
		// this clocks RPOW state into the power latch of WLN211 board
		setOutputLow(MCU_RINT);
		nrf_delay_ms(1);
		setOutputHi(MCU_RINT);
		nrf_delay_ms(1);
		setOutputLow(MCU_RINT);
	}
	

void Hw_setAistinWifiWLN211_On()
{
	setOutputHi(MCU_RPOW);
	pulseRINT();
}

void Hw_setAistinWifiWLN211_Off()
{
	setOutputHi(MCU_RPOW);
	pulseRINT();
}



void Hw_setUsbLinesOff()
{
		setInputNo(MCU_USB_CTS);
		setInputNo(MCU_USB_RTS);
		setInputNo(MCU_USB_RX);
		setInputNo(MCU_USB_TX);
		#ifdef MCU_USB_RESET
		setInputNo(MCU_USB_RESET);
		#endif
}


void Hw_setSensorLinesOff()
{
	setInputNo(MCU_I2C_SCL);
	setInputNo(MCU_I2C_SDA);
	setInputNo(MCU_BRD_CTS_I2C2_SCL);
	setInputNo(MCU_BRD_RTS_I2C2_SDA);
	setInputNo(MCU_SINT);
	setInputNo(MCU_DINT);
	setInputNo(MCU_GENIO1_INT1);
}


void Hw_setSensorPowers(bool on)
{
	if (on) setOutputHi(MCU_SEN);
	else {
		setOutputLow(MCU_SEN);
		Hw_setSensorLinesOff();
	}
}


/**@brief Initializes nRF51822 and related things.
 */
void Hw_init()
{
		setInputHi(MCU_GENIO1_INT1);
		setInputLow(MCU_1WIRE);
		setInputLow(MCU_BRD_MISO);
		setInputLow(MCU_BRD_MOSI); 
		setInputLow(MCU_BRD_SCLK);
		setInputNo(MCU_VBAT_MEAS); 
		setInputLow(MCU_AD0);
		setOutputLow(MCU_I2C_SCL);
		setOutputLow(MCU_RINT);
	
		setInputNo(MCU_PCTRL);
		//setInputHi(MCU_PCTRL); // this might cause a current loop!

		setOutputHi(MCU_SEN);
		setInputNo(MCU_SWSTAT);
		setOutputLow(MCU_GREEN_LED);
		setOutputLow(MCU_BLUE_LED);
		setInputHi(MCU_SINT);
	
		setOutputLow(MCU_BMEAS);
		//setOutputHi(MCU_BMEAS);
	
		setOutputHi(MCU_RPOW);
		setInputHi(MCU_DINT);
  	setOutputHi(MCU_BRD_TX);
		setInputHi(MCU_BRD_RX);
		setInputHi(MCU_BRD_CTS_I2C2_SCL);
		setInputHi(MCU_BRD_RTS_I2C2_SDA);
		setInputHi(MCU_RED_LED);
		setOutputHi(MCU_BRD_SS);
		#ifdef MCU_USB_RESET
		setInputNo(MCU_USB_RESET);
		#endif
		setInputNo(MCU_USB_CTS);
		setOutputLow(MCU_USB_RTS);
		setInputNo(MCU_USB_RX);
		setOutputHi(MCU_USB_TX);
		setOutputHi(MCU_I2C_SDA);
}
