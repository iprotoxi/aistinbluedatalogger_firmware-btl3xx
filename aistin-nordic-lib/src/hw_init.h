#ifndef HW_INIT_H
#define HW_INIT_H


#include <stdbool.h>


void setInputHi(int pin);
void setInputLow(int pin);
void setInputNo(int pin);
void setOutputHi(int pin);
void setOutputLow(int pin);
void Hw_init(void);
void Hw_setUsbLinesOff(void);
void Hw_setSensorPowers(bool on);
void Hw_setAistinWifiWLN211_On(void);
void Hw_setAistinWifiWLN211_Off(void);


#endif
