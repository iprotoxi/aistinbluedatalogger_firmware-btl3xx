/**
  @file accmagncombo_KMX62.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "debug.h"
#include "kmx62.h"
#include "twiface.h"
#include "aistin.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "accmagncombo_KMX62.h"


#if defined(ACC_KMX62) || defined(MAG_KMX62)


TwiDevice KMX62;
static
TwiDevice *This = &KMX62;


/**@brief Set-up sensor chip with default configurations
 * @return true in cases of success, otherwise false
 */
bool Accmagncombo_KMX62_setupChip(void)
{	
	bool ret;
	if (This->ready) return true;
	Twiface_initDevice(
		This,	KMX62_NAME, KMX62_TWIUNIT, KMX62_TWIFREQ,
		KMX62_DEVADDR, KMX62_SDA, KMX62_SCL);
	/* Reset RAM */
	ret = Twiface_writeReg(This, KMX62_CTRL_REG1, KMX62_CTRL_REG1_SRST);
	if (!ret) return ret;
	nrf_delay_ms(RESET_DELAY_MSEC);	
	/* Wait for the reset bit to clear. */
	bool reset_pending = true;
	uint8_t reg;
	do {
			 Twiface_readReg(This, KMX62_CTRL_REG1, &reg, sizeof reg);
			 if ((reg & KMX62_CTRL_REG1_SRST)) {
						nrf_delay_ms(RESET_DELAY_MSEC);
				} else {
					reset_pending = false;
				}
	} while (reset_pending);
	/* Wake up the device in standby mode. */
	Twiface_writeReg(This, KMX62_CTRL_REG2, 0);
	/* Enable interrupt to pin pin1, push/pull, active high, latching */	
	//Twiface_writeReg(This, KMX62_INC1, KMX62_INC1_DRDY_A1);	
	//Twiface_writeReg(This, KMX62_INC3, 0x99);	 // JNi
	/* test using mag data ready */
	//Twiface_writeReg(This, KMX62_INC3, KMX62_INC3_IEA1);
	/* Set the default output data rate. */
	Twiface_writeReg(This, KMX62_ODCNTL, KMX62_ODCNTL_OSA_100 | KMX62_ODCNTL_OSM_100);
	/* Clear any outstanding interrupt signal. */
	Twiface_readReg(This, KMX62_INL, &reg, sizeof reg);
	/* 2g, enable acc + mag, max resolution */
	Twiface_writeReg(This, KMX62_CTRL_REG2,
		KMX62_CTRL_REG2_GSEL_2G | KMX62_CTRL_REG2_MAG_EN |
		KMX62_CTRL_REG2_ACCEL_EN | KMX62_CTRL_REG2_RES_MAX);
	
	return ret;
}


/**@brief Set sensor resolution.
 * @param[in]   Resolution, 0 = lowest, 3 = best possible.
 * @return      Resolution that was actually set.
 */
int Accmagncombo_KMX62_setResolution(int resolution)
{
	return 0;
}

	
#endif
