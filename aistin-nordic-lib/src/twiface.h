/**
  @file twiface.h
  @brief Part of Aistin library
	
	This program is free software under the MIT License (MIT)

	Copyright 2015 iProtoXi Oy

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

#include <stdint.h>
#include "twidevice.h"

void Twiface_init(void);
void Twiface_initDevice(
	TwiDevice *twiDevice, const char *name, uint8_t twiUnit,
	uint32_t busFreq, uint8_t devAddr, uint8_t sdaPin, uint8_t sclPin);
void Twiface_error(TwiDevice *twiDevice, const char *error);
bool Twiface_readReg(
	TwiDevice *twiDevice, uint8_t reg, uint8_t *data, uint8_t size);
bool Twiface_readByte(TwiDevice *twiDevice, uint8_t *byte);
bool Twiface_readData(TwiDevice *twiDevice, uint8_t *data, uint16_t size, bool clear_bus);
bool Twiface_writeByteReg(TwiDevice *twiDevice, uint8_t reg, uint8_t value, bool stop);
bool Twiface_writeByte(TwiDevice *twiDevice, uint8_t byte);
bool Twiface_writeData(TwiDevice *twiDevice, uint8_t *data, uint16_t size, bool clear_bus);
void Twiface_scanForDevices(int sdaPin, int sclPin, uint32_t busFreq);

#define Twiface_writeReg(twi, reg, val) Twiface_writeByteReg(twi, reg, val, true)

