/**
  @file accelerometer_KMX62.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "softdevice_handler.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "accelerom.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "leds.h"
#include "kmx62.h"
#include "twiface.h"
#include "aistin.h"
#include "accmagncombo_KMX62.h"
#include "timing.h"


#ifdef ACC_KMX62


extern TwiDevice KMX62;
static TwiDevice *This = &KMX62;


#define INT_PIN	KMX62_INT_PIN

static
app_gpiote_user_id_t m_app_gpiote_my_id;
static
int8_t interruptSignal = 0;

static
void accelerationInterrupt(uint32_t event_pins_low_to_high, uint32_t event_pins_high_to_low)
{
	//if (event_pins_high_to_low & (1 << INT_PIN)) {
		interruptSignal = 1;
	//}
}


static
void KMX62_setAccelerationDataReadyInterrupt(void)
{
	uint8_t v;
	Twiface_readReg(&KMX62, KMX62_INC1, &v, sizeof v);
	v |= KMX62_INC1_DRDY_A1;
	Twiface_writeReg(&KMX62, KMX62_INC1, v);	
	// set interrupt lines as open drain, active low, pulsed
	Twiface_writeReg(&KMX62, KMX62_INC3, 0x99);	
	// set interrupt lines as open drain, active low, latched
	//Twiface_writeReg(&KMX62, KMX62_INC3, 0x88);	
}


static
void KMX62_clearInterrupt(void)
{
	uint8_t v;
	Twiface_readReg(&KMX62, KMX62_INL, &v, sizeof v);
}


/**@brief Initialize accelerometer interrupt.
 */
static
void initInterrupt(void)
{
	uint32_t err_code;
	nrf_gpio_cfg_sense_input(
		INT_PIN, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);
	err_code = app_gpiote_user_register(
		&m_app_gpiote_my_id, 0, (1 << INT_PIN), accelerationInterrupt);
	APP_ERROR_CHECK(err_code);
	err_code = app_gpiote_user_enable(m_app_gpiote_my_id);
	APP_ERROR_CHECK(err_code);
}


/**@brief Return true if interrupt has been occurred since the previous call.
 */
bool Accelerom_getInterruptSignal()
{
	uint8_t nested;
	sd_nvic_critical_region_enter(&nested);
	bool is = (bool)interruptSignal;
	if (is) interruptSignal = 0;
	sd_nvic_critical_region_exit(nested);
	if (is) KMX62_clearInterrupt();
	return is;
}


/**@brief Set sensor resolution.
 * @param[in]   Resolution, 0 = lowest, 3 = best possible.
 * @return      Resolution that was actually set.
 */
int Accelerom_setResolution(int resolution)
{
	return Accmagncombo_KMX62_setResolution(resolution);
}

	
/**@brief Set sensor scale.
 * @param[in]   mingScale		Minimum scale that is required in g's (positive side).
 * @return      Scale that was actually set.
 */
int Accelerometer_setScale(int mingScale)
{
	uint8_t v;
	uint8_t s = KMX62_CTRL_REG2_GSEL_16G;
	int g = 2;
	Twiface_readReg(&KMX62, KMX62_CTRL_REG2, &v, sizeof v);
	if (mingScale <= 2) {
		s = KMX62_CTRL_REG2_GSEL_2G;
	}
	else if (mingScale <= 4) {
		g = 4;
		s = KMX62_CTRL_REG2_GSEL_4G;
	}
	else if (mingScale <= 8) {
		g = 8;
		s = KMX62_CTRL_REG2_GSEL_8G;
	}
	else if (mingScale <= 16) {
		g = 16;
	}
	else {
		debugln("requested scale out of range");
	}
	v &= ~KMX62_CTRL_REG2_GSEL_MASK;
	Twiface_writeReg(&KMX62, KMX62_CTRL_REG2, v | s);
	return g;
}

	
/**@brief Set sensor output rate.
 * @param[in]   minOutputHz  Minimum rate that is required
 * @return      Rate that was actually set.
 */
int Accelerom_setOutputRate(int minOutputHz)
{
	const int supportedHz[] = {
		6, 12, 25, 50, 100, 200, 400, 800, 1600
	};
	const int odcntlForHz[] = {
		KMX62_ODCNTL_OSA_6p25, KMX62_ODCNTL_OSA_12p5,
		KMX62_ODCNTL_OSA_25, KMX62_ODCNTL_OSA_50,
		KMX62_ODCNTL_OSA_100, KMX62_ODCNTL_OSA_200,
		KMX62_ODCNTL_OSA_400, KMX62_ODCNTL_OSA_800,
		KMX62_ODCNTL_OSA_1600
	};
	const int n = sizeof(supportedHz) / sizeof(int);
	int i;
	uint8_t get, set;
	uint8_t ov, dv;
	for (i = 0; i < n; i++) {
		if (minOutputHz <= supportedHz[i]) break;
	}
	if (i >= n) {
		debugln("requested output data rate out of range");
		i--;
	}
	set = odcntlForHz[i];
	// according to spec, sensors must be disabled first
	Twiface_readReg(&KMX62, KMX62_CTRL_REG2, &ov, sizeof ov);
	dv &= ~(KMX62_CTRL_REG2_MAG_EN | KMX62_CTRL_REG2_ACCEL_EN);
	Twiface_writeReg(&KMX62, KMX62_CTRL_REG2, dv);
	Twiface_readReg(&KMX62, KMX62_ODCNTL, &get, sizeof get);
	set |= get & (~KMX62_ODCNTL_OSA_MASK);
	Twiface_writeReg(&KMX62, KMX62_ODCNTL, set);
	Twiface_writeReg(&KMX62, KMX62_CTRL_REG2, ov);
	return supportedHz[i];
}


static
bool init()
{
	bool ret = Accmagncombo_KMX62_setupChip();
	#ifdef SAMPLE_BY_SENSOR_INTERRUPT
	initInterrupt();
	KMX62_setAccelerationDataReadyInterrupt();
	#endif
	return ret;
}


/**@brief Initializes Accelerom-c-module (this).
 */
bool Accelerom_init()
{
	return init();
}


#if 0
/**@brief Function for reading 3D-acceleration values X, Y and Z.
 */
void Accelerom_readXYZCXYZ(uint8_t *_12Bytes)
{
	uint8_t xyz[12];	
	readReg(KMX62_ACC_XOUT_L, xyz, sizeof xyz);
	
	_12Bytes[0] = xyz[1];
	_12Bytes[1] = xyz[0];
	_12Bytes[2] = xyz[3];
	_12Bytes[3] = xyz[2];
	_12Bytes[4] = xyz[5];
	_12Bytes[5] = xyz[4];
	_12Bytes[6] = xyz[7];
	_12Bytes[7] = xyz[6];
	_12Bytes[8] = xyz[9];
	_12Bytes[9] = xyz[8];
	_12Bytes[10] = xyz[11];
	_12Bytes[11] = xyz[10];

}
#endif


/**@brief Function for reading 3D-values X, Y and Z.
 */
static
bool readXYZ(uint8_t *xyz16)
{
	//uint8_t v;
	//Twiface_readReg(&KMX62, KMX62_INL, &v, sizeof v); // release interrupt
	return Twiface_readReg(&KMX62, KMX62_ACC_XOUT_L, xyz16, 6);
}


int Accelerom_readXyz16(Xyz16 *xyz16)
{
	if (!This->ready) return 0;
	readXYZ(&xyz16->x[0]);
	return Aistin_fixEndian(xyz16);
}


int Accelerom_readXYZ3x16andTime8(Xyz16time8 *xyz16time8)
{
	if (!This->ready) return 0;
	readXYZ(&xyz16time8->xyz16.x[0]);
	Aistin_fixEndian(&xyz16time8->xyz16);
	xyz16time8->rtcTime8 = Timing_read1000Hz8();
	return sizeof(Xyz16time8);
}


int Accelerom_readXYZandTime4x16(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 8 || !This->ready) return 0;
	uint8_t xyzAndTime[8];
	readXYZ(xyzAndTime);
	*((uint16_t*)&xyzAndTime[6]) = Timing_read32768Hz16();
	return Aistin_writeDataWithTime4x16(buffer, xyzAndTime);
}


int Accelerom_readXYZ3x12(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 5 || !This->ready) return 0;
	uint8_t xyz[6];
	readXYZ(xyz);
	return Aistin_writeData3x12(buffer, xyz);
}


#endif
