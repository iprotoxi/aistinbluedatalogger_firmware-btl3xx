#ifndef __HW_BTL211__
#define __HW_BTL211__


#define RX_PIN_NUMBER   29
#define TX_PIN_NUMBER   28
#define RTS_PIN_NUMBER  24
#define CTS_PIN_NUMBER  25

#define LED_R   12
#define LED_G   17

#define ACC_SDA 	30
#define ACC_SCL 	 7
#define ACC_DEVADDR0		0x1D //0x19
//#define ACC_DEVADDR1		0x1D
#define ACC_BUS		NRF_TWI1
#define ACC_MEMS_INT_PIN	4
#define ACC_MEMS_INT2_PIN	5

#define INFRARED_SDA 				30
#define INFRARED_SCL 	 			 7
#define INFRARED_DEVADDR		0x5A
#define INFRARED_BUS		NRF_TWI1

#define HUM_SDA 				30
#define HUM_SCL 	 			 7
#define HUM_DEVADDR		0x40
#define HUM_BUS		NRF_TWI1

#define BAR_SDA 				30
#define BAR_SCL 	 			 7
#define BAR_DEVADDR		0x5c
#define BAR_BUS		NRF_TWI1

#define MAG_SDA 			30
#define MAG_SCL 		     7
#define MAG_DEVADDR		0x1C
#define MAG_BUS		NRF_TWI0
#define MAG_INT_PIN	    3

#define GYR_SDA 				30
#define GYR_SCL 				 7
#define GYR_DEVADDR		0x6b
#define GYR_BUS		NRF_TWI0
#define GYR_INT_PIN	    2

#define LED_SDA 				30
#define LED_SCL 				 7
#define LED_DEVADDR		0x32
#define LED_BUS		NRF_TWI1

#define BUS24_ADC			  6	
#define BUS24_SEN			 10	

#define MEM_CS_PIN		21
#define MEM_SCK_PIN		21
#define MEM_SI_PIN		21
#define MEM_SO_PIN		21
//#define MEM_WP_PIN		29

#define VBATREF_PIN     21
#define C_VBAT_PIN      21


#endif
