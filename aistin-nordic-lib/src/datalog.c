/**
  @file datalog.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <string.h>
#include "nrf_delay.h"
#include "debug.h"
#include "hw_select.h"
#include "aistin.h"
#include "protocol.h"
#include "extmem.h"
#include "datalog.h"


// Double-buffered FAT makes sure no data is lost even if power-fail
// happens on writing FAT. That's why we use 2 * PAGESIZE FAT.
#define FATAREA_SIZE								(2 * EXTMEM_PAGESIZE)
// FAT must be updated frequently, but flash type memory is quite limited
// on number of writes. We do not update FAT after every data write.
#define FAT_UPDATE_FREQUENCY		10	// update after this many items have been written


// EXTMEM_PAGESIZE must be dividable by sizeof(Fat)! 
typedef
	struct
	{
		uint32_t in, out;
		uint32_t valid; // this should be written last to flash/eeprom
	} Fat;

#define FAT_MAGIC					0xA2D2C3B0	
#define fatIsValid(fat)  ((fat).valid == FAT_MAGIC)
#define fatValidate(fat)  ((fat).valid = FAT_MAGIC)
	
static Fat Fat_;
static uint16_t fatPos = 0;
static uint16_t prevFatPos = 0;
static int fatUpdateDecimator;
static bool LoggingOn = false;

	
static
	bool fatIsReallyValid(Fat *fat)
	{
		if (fatIsValid(*fat)) {
			if (fat->in >= FATAREA_SIZE && fat->in < EXTMEM_SIZE &&
					fat->out >= FATAREA_SIZE && fat->out < fat->in) return true;
		}
		return false;
	}
	

static
	void writeFat(void)
	{
		uint32_t fatInvalid = 0x0f0f0f0f;
		char str[80];
		sprintf(str, "fatPos=%d", fatPos);
		debugln(str);
		if (fatPos == 0 || fatPos == FATAREA_SIZE / 2) {
			Extmem_erase(fatPos, EXTMEM_PAGESIZE); // this might be slow!
		}
		Extmem_write(fatPos, (uint8_t*)&Fat_, sizeof(Fat));
		// Invalidate old fat record
		if (prevFatPos != fatPos) {
			Extmem_write(prevFatPos, (uint8_t*)&fatInvalid, sizeof(fatInvalid));
		}
		prevFatPos = fatPos;
		fatPos += sizeof(Fat);
		if (fatPos > FATAREA_SIZE - sizeof(Fat)) fatPos = 0;
		fatUpdateDecimator = 0;
	}


static
void erasePage(uint32_t address)
{	
	//debuglnf("erase @%d (sz=%d)", address, EXTMEM_PAGESIZE);
	Extmem_erase(address, EXTMEM_PAGESIZE); // this might be slow!		
}


void Datalog_clearLog(void)
{
	Fat_.in = FATAREA_SIZE; // skip FAT area
	Fat_.out = Fat_.in;
	fatValidate(Fat_);
	prevFatPos = 0;
	fatPos = 0;
	writeFat();
	erasePage(Fat_.in);
}
	

static
	void findFat(void)
	{
		// read until a valid FAT is found
		uint32_t a;
		debug("find FAT ");
		for (a = 0; a <= FATAREA_SIZE - sizeof(Fat); a += sizeof(Fat))
		{
			fatPos = a;
			Extmem_read(a, (uint8_t*)&Fat_, sizeof(Fat));
			if (fatIsReallyValid(&Fat_)) {
				debuglnf(" -> i=%d o=%d", Fat_.in, Fat_.out);
				return;
			}
			debug(".");
		}
		debugln("no FAT");
		// no fat found, initialize
		Datalog_clearLog();
	}


void Datalog_init(void)
{
	findFat();
	//Extmem_dump(8192, 1024);
}


static
AistinItem AistinItem_;

static
void queueItem(AistinItem *ai)
{
	// put previous item
	if (AistinItem_.dataSize > 0) {
		int s = Aistin_itemSize(&AistinItem_);
		Extmem_write(Fat_.in, (uint8_t*)&AistinItem_, s);
		debuglnf("item(%d)->%d", s, Fat_.in);
		Fat_.in += s; // NOTE! We are not using ring-buffering.
		AistinItem_.dataSize = 0;
	}
	// check if we need to erase next page for this one
	int s = Aistin_itemSize(ai);
	int page = Fat_.in / EXTMEM_PAGESIZE;
	// check that there's room for maximum sized ai (eases reading)
	if (Fat_.in <= EXTMEM_SIZE - sizeof(*ai)) {
		if ((Fat_.in + s - 1) / EXTMEM_PAGESIZE != page) {
				// start erasing next page, hope it's async...
				erasePage(Fat_.in + s - 1);
		}
		// copy to be written to memory
		memcpy(&AistinItem_, ai, sizeof(AistinItem_));
		AistinItem_.dataID16 |= AISTIN_ID_0001_NOTREALTIMEbit;
	}
}


void updateFat()
{
	fatUpdateDecimator++;
	if (fatUpdateDecimator >= FAT_UPDATE_FREQUENCY) {
		writeFat();
	}
}


bool Datalog_getNextItem(AistinItem *ai)
{
	ai->dataSize = 0;
	if (Fat_.out < Fat_.in)
	{
		int s = sizeof(*ai); // read maximum size
		Extmem_read(Fat_.out, (uint8_t*)ai, s);
		// do a simple validity check
		int sz = Aistin_itemSize(ai);
		if (sz <= s) s = sz;
		else {
			debugln("err: data item size");
		}
		debuglnf("FAT: i=%d, o=%d, isz=%d", Fat_.in, Fat_.out, sz);
		Fat_.out += s;
		if (Fat_.out >= Fat_.in) {
			writeFat(); // all records has been read out
			debugln("*** LOG END ***");
		}
		//else updateFat(); // TEST! REMOVE THIS
		return true;
	}
	return false;
}


void Datalog_rereadLog(void)
{
	Fat_.out = FATAREA_SIZE;
}


void Datalog_startNewLog(void)
{
	Datalog_clearLog();
	LoggingOn = true;
}


void Datalog_stopLogging()
{
	LoggingOn = false;
	writeFat();
}


bool Datalog_isLogging(void)
{
	return LoggingOn;
}


bool Datalog_isSendingLog(void)
{
	// we are sending log when not logging, and the log is not yet sent
	return !LoggingOn && (Fat_.out < Fat_.in);
}


void Datalog_log(AistinItem *ai)
{
	if (LoggingOn) {
		queueItem(ai);
		updateFat();
	}
}
