/**
  @file twidevice.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#ifndef TWIDEVICE_H
#define TWIDEVICE_H


#include <stdint.h>
#include <stdbool.h>
#include "twitypes.h"


typedef
struct {
	bool ready;
	const char *name;
	uint8_t twiUnit;
	uint32_t busFreq;
	uint8_t devAddr;
	uint8_t sdaPin;
	uint8_t sclPin;
	bool (*readXyz16)(Xyz16 *xyz16);
} TwiDevice;


int Twidevice_readXyz16time8(TwiDevice *t, Xyz16time8 *xyz16time8);


#endif
