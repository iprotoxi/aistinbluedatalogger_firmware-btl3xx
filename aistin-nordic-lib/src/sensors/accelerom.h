/**
  @file accelerom.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/
#ifndef ACCELEROM_H
#define ACCELEROM_H


#include <stdint.h>
#include <stdbool.h>
#include "twitypes.h"


bool Accelerom_init(void);
int Accelerom_setOutputRate(int minOutputHz);
void Accelerom_readXYZCXYZ(uint8_t *_12Bytes);
int Accelerom_readXYZ3x16(uint8_t *buffer, int bufferSize);
int Accelerom_readXYZandTime4x16(uint8_t *buffer, int bufferSize);
int Accelerom_readXYZ3x12(uint8_t *buffer, int bufferSize);
int Accelerom_readXYZ3x16andTime8(Xyz16time8 *Xyz16time8);
bool Accelerom_getInterruptSignal(void);


#endif

