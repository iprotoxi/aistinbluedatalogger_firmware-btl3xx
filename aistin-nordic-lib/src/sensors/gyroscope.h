/**
  @file gyroscope.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#ifndef GYROSCOPE_H
#define GYROSCOPE_H


#include <stdint.h>
#include <stdbool.h>
#include "twitypes.h"


bool Gyroscope_init(void);
int Gyroscope_readXYZ3x16(uint8_t *buffer, int bufferSize);
int Gyroscope_readXYZ3x16andTime8(Xyz16time8 *xyz16time8);
int Gyroscope_readXYZ3x12(uint8_t *buffer, int bufferSize);
bool Gyroscope_getInterruptSignal(void);


#endif
