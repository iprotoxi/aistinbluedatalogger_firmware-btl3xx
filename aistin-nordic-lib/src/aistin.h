/**
  @file aistin.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/
#ifndef AISTIN_H
#define AISTIN_H


#include <stdint.h>


#include "aistin_ids.h"
#include "aistin_ble.h"


typedef
struct {
	uint16_t dataID16; // data identification code, see aistin_ids.h
  uint8_t dataSize;
	AistinData data;
} AistinItem;

/* codes are not used, currently
#define AISTIN_CODE_MASK		 (0xc0)
#define AISTIN_CODE_DATA     (0<<5)
#define AISTIN_CODE_READ     (1<<5)
#define AISTIN_CODE_WRITE    (2<<5)
#define AISTIN_CODE_SCAN     (3<<5)
*/

#define Aistin_itemSize(ai) ((ai)->dataSize + sizeof((ai)->dataSize) + sizeof((ai)->dataID16))
void Aistin_initItem(AistinItem *ai, uint16_t dataID16);
uint8_t Aistin_getDataID8(uint16_t dataID16);
int Aistin_asciiFormat(char *str, AistinItem *ai, uint8_t seqNr);
int Aistin_fixEndian(Xyz16 *xyz16);
int Aistin_writeData3x16(uint8_t *buffer, uint8_t *bigEndian16);
int Aistin_writeDataWithTime4x16(uint8_t *buffer, uint8_t *bigEndian16);
int Aistin_writeData3x12(uint8_t *buffer, uint8_t *bigEndian16);


#endif
