/**
  @file timing.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>

#include "timing.h"
#include "app_timer.h"
#include "debug.h"


uint32_t EpochTime;
static uint32_t _32768Hz;
static uint32_t past;
static uint32_t pastE;


void Timing_run(void)
{
	uint32_t now;
	uint32_t dif;
	app_timer_cnt_get(&now); // 24 bit counter, actually
	dif = now - past;
	if (dif >= (1UL<<31)) dif += (1UL<<24);
	_32768Hz += dif;
	past = now;
	dif = now - pastE;
	if (dif >= (1UL<<31)) dif += (1UL<<24);
	dif /= 32768; // let the compiler notice the >> shift ;)
	if (dif > 0) {
		EpochTime += dif;
		pastE = now;
		#if 0
		char str[80];
		sprintf(str, "%d s %d", EpochTime, _32768Hz);
		debugln(str);
		#endif
	}
}


uint16_t Timing_read32768Hz16(void)
{
	uint32_t now;
	app_timer_cnt_get(&now); // 24 bit counter, actually
	return now;
}


uint8_t Timing_read1024Hz8(void)
{
	uint32_t now;
	app_timer_cnt_get(&now); // 24 bit counter, actually
	return now >> 5;
}


uint8_t Timing_read1000Hz8(void)
{
	uint32_t now;
	app_timer_cnt_get(&now); // 24 bit counter
	return (((now >> 2) * 1000) >> 13) % 250;
}
