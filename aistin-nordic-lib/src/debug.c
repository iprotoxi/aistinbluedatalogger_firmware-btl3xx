/**
  @file debug.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdarg.h>

#include "debug.h"
#include "hw_select.h"
#include "nrf_gpio.h"
#include "hw_init.h"


bool DebugMode = true;


void Debug_init(void)
{
	simple_uart_config(
		MCU_USB_RTS, MCU_USB_TX,
		MCU_USB_CTS, MCU_USB_RX, false);
	DebugMode = true;
}


void Debug_close(void)
{
	debugln("Closing debug output.");
	NRF_UART0->TASKS_STOPTX = 1;
	NRF_UART0->TASKS_STOPRX = 1;
	NRF_UART0->ENABLE = 0;
	Hw_setUsbLinesOff();
	DebugMode = false;
}


bool Debug_check(void)
{
	// this does not work if UART is disabled for each sd_app_evt_wait() call
	uint8_t buf[80];
	buf[0] = 0;
	simple_uart_get_with_timeout(0, buf);
	if (buf[0] == 27) DebugMode = false;
	else if (buf[0]) DebugMode = true;
	return DebugMode;
}


uint8_t Debug_getCh(void)
{
	// this does not work if UART is disabled for each sd_app_evt_wait() call
	uint8_t buf[80];
	buf[0] = 0;
	simple_uart_get_with_timeout(0, buf);
	return buf[0];
}


int debugf(const char *format, ...)
{
	int n = 0;
	if (DebugMode) {
		char str[250]; // be sure to have enough room!
		va_list ap;
		va_start(ap, format);
		n = vsprintf(str, format, ap);
		simple_uart_putstring((const uint8_t*)str);
	}
	return n;
}


int debugfln(const char *format, ...)
{
	int n = 0;
	if (DebugMode) {
		char str[250]; // be sure to have enough room!
		va_list ap;
		va_start(ap, format);
		n = vsprintf(str, format, ap);
		simple_uart_putstring((const uint8_t*)str);
		simple_uart_putstring((const uint8_t*)&"\r\n");
	}
	return n;
}


void Debug_string(uint8_t *str)
{
	if (DebugMode) {
		simple_uart_putstring(str);
	}
}


void Debug_stringln(uint8_t *str)
{
	if (DebugMode) {
		simple_uart_putstring(str);
		simple_uart_putstring("\r\n");
	}
}


void Debug_char(uint8_t c)
{
	if (DebugMode) {
		simple_uart_put(c);
	}
}


static
char hexToText(char nibble)
{
	if (nibble < 10) return '0' + nibble;
	return 'a' - 10 + nibble;
}


/**@brief Print out hexadecimal data.
 */
void Debug_hex(uint8_t *data, int size)
{
	int i;
	if (DebugMode) {
		for (i = 0; i < size; i++)
		{
			char str[10];
			str[0] = hexToText(data[i]>>4);
			str[1] = hexToText(data[i]&15);
			str[2] = 0;
			debug(str);
		}
	}
}
