#ifndef BLE_CONTROL_H
#define BLE_CONTROL_H

#include "ble_nus.h"
#include "ble_bas.h"

extern bool Ble_control_connected;
extern ble_nus_t m_nus;
extern ble_bas_t m_bas;

#define Ble_control_isConnected() Ble_control_connected
// For debug purposes only:
#define Ble_control_setConnected(ft) Ble_control_connected = ft

void Ble_control_init(void);

#endif
