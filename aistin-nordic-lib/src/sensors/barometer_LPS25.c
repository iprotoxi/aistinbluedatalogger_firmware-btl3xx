/**
  @file barometer_LPS25.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "barometer.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "twiface.h"


#ifdef BAR_LPS25


static
TwiDevice LPS25;
static
TwiDevice *This = &LPS25;


#define LPS25_REG_PRESSURE			0xa8
#define LPS25_REG_TEMPERATURE		0xab
#define LPS25_CTRL_REG3					0x22


static
void setupChip()
{
  Twiface_writeReg(This, 0x20, 0xc4); // ODR
  Twiface_writeReg(This, 0x10, 0x0f); // resolution
	Twiface_writeReg(This, LPS25_CTRL_REG3, 0xc0); // interrupt active low, hi-imp.
  Twiface_writeReg(This, 0x21, 0x00);
}


/**@brief Initializes Barometer-c-module (this).
 */
bool Barometer_init()
{
	Twiface_initDevice(
		This, LPS25_NAME, LPS25_TWIUNIT, LPS25_TWIFREQ,
		LPS25_DEVADDR, LPS25_SDA, LPS25_SCL);
	setupChip();
	return true;
}


static
void printValues(uint8_t *pres, int16_t temp)
{
	if (DebugMode) {
		char str[80];
		float tempf = 42.5 + temp / 480.0;
		int16_t temp88 = tempf * 256;
		float bars = pres[2] * 16.0 + pres[1] / 16.0 + pres[0] / 4096.0;
		sprintf(str, "bars=%f temp=%f\r\n", bars, tempf);
		debug(str);
	}
}


int Barometer_read2x16(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 4 || !This->ready) return 0;
	uint8_t bar[3];
	int16_t temp;
	Twiface_readReg(This, LPS25_REG_PRESSURE, bar, 3);
	Twiface_readReg(This, LPS25_REG_TEMPERATURE, (uint8_t*)&temp, 2);
	//printValues(bar, temp);
	int16_t temp88 = (42.5 * 256) + (int32_t)temp * 256 / 480;
	buffer[0] = (bar[2] << 1) | (bar[1] >> 7);
	buffer[1] = (bar[1] << 1) | (bar[0] >> 7);
	buffer[2] = temp88 >> 8;
	buffer[3] = temp88 & 0xff;
	return 4;
}


#endif
