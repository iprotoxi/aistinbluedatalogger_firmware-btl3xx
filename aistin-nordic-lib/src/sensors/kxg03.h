
/* registers */
#define KXG03_TEMP_OUT_L 0x00
#define KXG03_TEMP_OUT_H 0x01
#define KXG03_GYRO_XOUT_L 0x02
#define KXG03_GYRO_XOUT_H 0x03
#define KXG03_GYRO_YOUT_L 0x04
#define KXG03_GYRO_YOUT_H 0x05
#define KXG03_GYRO_ZOUT_L 0x06
#define KXG03_GYRO_ZOUT_H 0x07
#define KXG03_ACC_XOUT_L 0x08
#define KXG03_ACC_XOUT_H 0x09
#define KXG03_ACC_YOUT_L 0x0A
#define KXG03_ACC_YOUT_H 0x0B
#define KXG03_ACC_ZOUT_L 0x0C
#define KXG03_ACC_ZOUT_H 0x0D
//Auxiliary Sensor #1 output data bytes AUX1_OUT1 through AUX1_OUT6
#define KXG03_AUX1_OUT1 0x0E
#define KXG03_AUX1_OUT2 0x0F
#define KXG03_AUX1_OUT3 0x10
#define KXG03_AUX1_OUT4 0x11
#define KXG03_AUX1_OUT5 0x12
#define KXG03_AUX1_OUT6 0x13
//Auxiliary Sensor #2 output data bytes AUX2_OUT1 through AUX2_OUT6
#define KXG03_AUX2_OUT1 0x14
#define KXG03_AUX2_OUT2 0x15
#define KXG03_AUX2_OUT3 0x16
#define KXG03_AUX2_OUT4 0x17
#define KXG03_AUX2_OUT5 0x18
#define KXG03_AUX2_OUT6 0x19
//Number of ODR cycles spent in wake state as measured in accelerometer ODRa_wake/ODRa_sleep periods. Data byte WAKE_CNT_L and WAKE_CNT_H.
#define KXG03_WAKE_CNT_L 0x1A
#define KXG03_WAKE_CNT_H 0x1B
//Number of ODR cycles spent in sleep state as measured in accelerometer ODRa_wake/ODRa_sleep periods. Data byte SLEEP_CNT_L and SLEEP_CNT_H.
#define KXG03_SLEEP_CNT_L 0x1C
#define KXG03_SLEEP_CNT_H 0x1D
//Reports the number of data packets (ODR cycles) currently stored in the buffer.
#define KXG03_BUF_SMPLEV_L 0x1E
#define KXG03_BUF_SMPLEV_H 0x1F
//Reports the number of data packets lost since buffer has been filled.
#define KXG03_BUF_PAST_L 0x20
#define KXG03_BUF_PAST_H 0x21
//Reports the status of Auxiliary Sensors AUX1 and AUX2.
#define KXG03_AUX_STATUS 0x22
#define KXG03_WHO_AM_I 0x30
//Individual Identification (serial number).
#define KXG03_SN1_MIR 0x31
#define KXG03_SN2_MIR 0x32
#define KXG03_SN3_MIR 0x33
#define KXG03_SN4_MIR 0x34
#define KXG03_STATUS1 0x36
#define KXG03_INT1_SRC1 0x37
#define KXG03_INT1_SRC2 0x38
//Reading this register releases int1 source registers
#define KXG03_INT1_L 0x39
#define KXG03_STATUS2 0x3A
#define KXG03_INT2_SRC1 0x3B
#define KXG03_INT2_SRC2 0x3C
//Reading this register releases int2 source registers
#define KXG03_INT2_L 0x3D
//Accelerometer Wake Mode Control register.
#define KXG03_ACCEL_ODR_WAKE 0x3E
#define KXG03_ACCEL_ODR_SLEEP 0x3F
#define KXG03_ACCEL_CTL 0x40
//Gyroscope Wake Mode Control register.
#define KXG03_GYRO_ODR_WAKE 0x41
#define KXG03_GYRO_ODR_SLEEP 0x42
#define KXG03_STDBY 0x43
//Special control register 1
#define KXG03_CTL_REG_1 0x44
#define KXG03_INT_PIN_CTL 0x45
//Physical interrupt pin INT1 select register.
#define KXG03_INT_PIN1_SEL 0x46
#define KXG03_INT_PIN2_SEL 0x47
//Buffer Full Interrupt enable/mask bit.
#define KXG03_INT_MASK1 0x48
//which axis and direction of detected motion can cause an interrupt.
#define KXG03_INT_MASK2 0x49
//External Synchronous control register.
#define KXG03_FSYNC_CTL 0x4A
#define KXG03_WAKE_SLEEP_CTL1 0x4B
//WUF and BTS threshold mode.
#define KXG03_WAKE_SLEEP_CTL2 0x4C
#define KXG03_WUF_TH 0x4D
#define KXG03_WUF_COUNTER 0x4E
#define KXG03_BTS_TH 0x4F
#define KXG03_BTS_COUNTER 0x50
#define KXG03_AUX_I2C_CTL_REG 0x51
#define KXG03_AUX_I2C_SAD1 0x52
#define KXG03_AUX_I2C_REG1 0x53
#define KXG03_AUX_I2C_CTL1 0x54
#define KXG03_AUX_I2C_BIT1 0x55
#define KXG03_AUX_I2C_ODR1_W 0x56
#define KXG03_AUX_I2C_ODR1_S 0x57
#define KXG03_AUX_I2C_SAD2 0x58
#define KXG03_AUX_I2C_REG2 0x59
#define KXG03_AUX_I2C_CTL2 0x5A
#define KXG03_AUX_I2C_BIT2 0x5B
#define KXG03_AUX_I2C_ODR2_W 0x5C
#define KXG03_AUX_I2C_ODR2_S 0x5D
//Buffer watermark threshold level L
#define KXG03_BUF_WMITH_L 0x75
//Buffer watermark threshold level H
#define KXG03_BUF_WMITH_H 0x76
//Buffer Trigger mode threshold L
#define KXG03_BUF_TRIGTH_L 0x77
//Buffer Trigger mode threshold H
#define KXG03_BUF_TRIGTH_H 0x78
//Wake mode channels to buffer
#define KXG03_BUF_CTL2 0x79
//Sleep mode channels to buffer
#define KXG03_BUF_CTL3 0x7A
#define KXG03_BUF_CTL4 0x7B
#define KXG03_BUF_EN 0x7C
#define KXG03_BUF_STATUS 0x7D
#define KXG03_BUF_CLEAR 0x7E
#define KXG03_BUF_READ 0x7F

 /* registers bits */ 
//Aux2 command sequence failure flag
#define KXG03_AUX_STATUS_AUX2FAIL (0x01 << 7)
//Aux2 data read error flag.
#define KXG03_AUX_STATUS_AUX2ERR (0x01 << 6)
//Aux1 has not been enabled or ASIC has successfully sent disable cmd.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_DISABLED (0x0 << 4)
//ASIC is attempting to enable aux sensor via enable sequence.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_WAITING_ENABLE (0x1 << 4)
//ASIC is attempting to disable aux sensor via disable sequence.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_WAITING_DISABLE (0x2 << 4)
//ASIC has successfully sent aux enable cmd.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_RUNNING (0x3 << 4)
//Aux1 command sequence failure flag
#define KXG03_AUX_STATUS_AUX1FAIL (0x01 << 3)
//Aux1 data read error flag.
#define KXG03_AUX_STATUS_AUX1ERR (0x01 << 2)
//Aux1 has not been enabled or ASIC has successfully sent disable cmd.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_DISABLED (0x0 << 0)
//ASIC is attempting to enable aux sensor via enable sequence.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_WAITING_ENABLE (0x1 << 0)
//ASIC is attempting to disable aux sensor via disable sequence.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_WAITING_DISABLE (0x2 << 0)
//ASIC has successfully sent aux enable cmd.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_RUNNING (0x3 << 0)
//Reports logical OR of non-masked interrupt sources sent to INT1
#define KXG03_STATUS1_INT1 (0x01 << 7)
//Reset indicator.
#define KXG03_STATUS1_POR (0x01 << 6)
//Auxiliary sensor #2 active flag.
#define KXG03_STATUS1_AUX2_ACT (0x01 << 5)
//Auxiliary sensor #1 active flag.
#define KXG03_STATUS1_AUX1_ACT (0x01 << 4)
#define KXG03_STATUS1_AUX_ERR (0x01 << 3)
//Sleep mode status
#define KXG03_STATUS1_WAKE_SLEEP_SLEEP_MODE (0x0 << 2)
//Wake mode status
#define KXG03_STATUS1_WAKE_SLEEP_WAKE_MODE (0x1 << 2)
//Gyro's run status
#define KXG03_STATUS1_GYRO_RUN (0x01 << 1)
//Gyro's start status
#define KXG03_STATUS1_GYRO_START (0x01 << 0)
//Buffer full interrupt.
#define KXG03_INT1_SRC1_INT1_BFI (0x01 << 7)
//Buffer water mark interrupt.
#define KXG03_INT1_SRC1_INT1_WMI (0x01 << 6)
//Wake up function interrupt.
#define KXG03_INT1_SRC1_INT1_WUFS (0x01 << 5)
//Back to sleep interrupt.
#define KXG03_INT1_SRC1_INT1_BTS (0x01 << 4)
//Aux2 data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_AUX2 (0x01 << 3)
//Aux1 data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_AUX1 (0x01 << 2)
//Accel data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_ACC (0x01 << 1)
//Gyro data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_GYRO (0x01 << 0)
//"Wake up event detected on x-axis
#define KXG03_INT1_SRC2_INT1_XNWU (0x01 << 5)
//"Wake up event detected on x-axis
#define KXG03_INT1_SRC2_INT1_XPWU (0x01 << 4)
//"Wake up event detected on y-axis
#define KXG03_INT1_SRC2_INT1_YNWU (0x01 << 3)
//"Wake up event detected on y-axis
#define KXG03_INT1_SRC2_INT1_YPWU (0x01 << 2)
//"Wake up event detected on z-axis
#define KXG03_INT1_SRC2_INT1_ZNWU (0x01 << 1)
//"Wake up event detected on z-axis
#define KXG03_INT1_SRC2_INT1_ZPWU (0x01 << 0)
#define KXG03_STATUS2_INT2 (0x01 << 7)
#define KXG03_STATUS2_POR (0x01 << 6)
#define KXG03_STATUS2_AUX2_ACT (0x01 << 5)
#define KXG03_STATUS2_AUX1_ACT (0x01 << 4)
#define KXG03_STATUS2_AUX_ERR (0x01 << 3)
#define KXG03_STATUS2_WAKE_SLEEP_SLEEP_MODE (0x0 << 2)
#define KXG03_STATUS2_WAKE_SLEEP_WAKE_MODE (0x1 << 2)
#define KXG03_STATUS2_GYRO_RUN (0x01 << 1)
#define KXG03_STATUS2_GYRO_START (0x01 << 0)
#define KXG03_INT2_SRC1_INT2_BFI (0x01 << 7)
#define KXG03_INT2_SRC1_INT2_WMI (0x01 << 6)
#define KXG03_INT2_SRC1_INT2_WUFS (0x01 << 5)
#define KXG03_INT2_SRC1_INT2_BTS (0x01 << 4)
#define KXG03_INT2_SRC1_INT2_DRDY_AUX2 (0x01 << 3)
#define KXG03_INT2_SRC1_INT2_DRDY_AUX1 (0x01 << 2)
#define KXG03_INT2_SRC1_INT2_DRDY_ACC (0x01 << 1)
#define KXG03_INT2_SRC1_INT2_DRDY_GYRO (0x01 << 0)
//"Wake up event detected on x-axis
#define KXG03_INT2_SRC2_INT2_XNWU (0x01 << 5)
//"Wake up event detected on x-axis
#define KXG03_INT2_SRC2_INT2_XPWU (0x01 << 4)
//"Wake up event detected on y-axis
#define KXG03_INT2_SRC2_INT2_YNWU (0x01 << 3)
//"Wake up event detected on y-axis
#define KXG03_INT2_SRC2_INT2_YPWU (0x01 << 2)
//"Wake up event detected on z-axis
#define KXG03_INT2_SRC2_INT2_ZNWU (0x01 << 1)
//"Wake up event detected on z-axis
#define KXG03_INT2_SRC2_INT2_ZPWU (0x01 << 0)
//Accel low power mode is disabled in wake state. Accel operates at max sampling rate and navg_wake is ignored.
#define KXG03_ACCEL_ODR_WAKE_LPMODE_W_DISABLED (0x0 << 7)
//Accel low power mode is enabled in wake state. Accel operates in duty cycle mode with number of samples set by navg_wake.
#define KXG03_ACCEL_ODR_WAKE_LPMODE_W_ENABLED (0x1 << 7)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG1 (0x0 << 4)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG2 (0x1 << 4)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG4 (0x2 << 4)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG8 (0x3 << 4)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG16 (0x4 << 4)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG32 (0x5 << 4)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG64 (0x6 << 4)
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG128 (0x7 << 4)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_0P781 (0x0 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_1P563 (0x1 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_3P125 (0x2 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_6P25 (0x3 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_12P5 (0x4 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_25 (0x5 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_50 (0x6 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_100 (0x7 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_200 (0x8 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_400 (0x9 << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_800 (0xa << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_1600 (0xb << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_3K2 (0xc << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_6K4 (0xd << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_12K8 (0xe << 0)
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_51K2 (0xf << 0)
#define KXG03_ACCEL_ODR_SLEEP_LPMODE_S_DISABLED (0x0 << 7)
#define KXG03_ACCEL_ODR_SLEEP_LPMODE_S_ENABLED (0x1 << 7)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG1 (0x0 << 4)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG2 (0x1 << 4)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG4 (0x2 << 4)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG8 (0x3 << 4)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG16 (0x4 << 4)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG32 (0x5 << 4)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG64 (0x6 << 4)
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG128 (0x7 << 4)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_0P781 (0x0 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_1P563 (0x1 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_3P125 (0x2 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_6P25 (0x3 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_12P5 (0x4 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_25 (0x5 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_50 (0x6 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_100 (0x7 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_200 (0x8 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_400 (0x9 << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_800 (0xa << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_1600 (0xb << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_3K2 (0xc << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_6K4 (0xd << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_12K8 (0xe << 0)
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_51K2 (0xf << 0)
#define KXG03_ACCEL_CTL_ACC_FS_S_2G (0x0 << 6)
#define KXG03_ACCEL_CTL_ACC_FS_S_4G (0x1 << 6)
#define KXG03_ACCEL_CTL_ACC_FS_S_8G (0x2 << 6)
#define KXG03_ACCEL_CTL_ACC_FS_S_16G (0x3 << 6)
#define KXG03_ACCEL_CTL_ACC_FS_W_2G (0x0 << 2)
#define KXG03_ACCEL_CTL_ACC_FS_W_4G (0x1 << 2)
#define KXG03_ACCEL_CTL_ACC_FS_W_8G (0x2 << 2)
#define KXG03_ACCEL_CTL_ACC_FS_W_16G (0x3 << 2)
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_256 (0x0 << 6)
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_512 (0x1 << 6)
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_1024 (0x2 << 6)
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_2048 (0x3 << 6)
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_10 (0x0 << 4)
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_20 (0x1 << 4)
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_40 (0x2 << 4)
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_160 (0x3 << 4)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_0P781 (0x0 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_1P563 (0x1 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_3P125 (0x2 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_6P25 (0x3 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_12P5 (0x4 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_25 (0x5 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_50 (0x6 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_100 (0x7 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_200 (0x8 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_400 (0x9 << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_800 (0xa << 0)
#define KXG03_GYRO_ODR_WAKE_ODRG_W_1600 (0xb << 0)
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_256 (0x0 << 6)
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_512 (0x1 << 6)
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_1024 (0x2 << 6)
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_2048 (0x3 << 6)
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_10 (0x0 << 4)
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_20 (0x1 << 4)
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_40 (0x2 << 4)
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_160 (0x3 << 4)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_0P781 (0x0 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_1P563 (0x1 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_3P125 (0x2 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_6P25 (0x3 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_12P5 (0x4 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_25 (0x5 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_50 (0x6 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_100 (0x7 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_200 (0x8 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_400 (0x9 << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_800 (0xa << 0)
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_1600 (0xb << 0)
//Aux2 sensor is enabled in sleep state.
#define KXG03_STDBY_AUX2_STDBY_S_ENABLED (0x0 << 7)
//Aux2 sensor is disabled in sleep state.
#define KXG03_STDBY_AUX2_STDBY_S_DISABLED (0x1 << 7)
//Aux1 sensor is enabled in sleep state.
#define KXG03_STDBY_AUX1_STDBY_S_ENABLED (0x0 << 6)
//Aux1 sensor is disabled in sleep state.
#define KXG03_STDBY_AUX1_STDBY_S_DISABLED (0x1 << 6)
//Gyro sensor is enabled in sleep state.
#define KXG03_STDBY_GYRO_STDBY_S_ENABLED (0x0 << 5)
//Gyro sensor is disabled in sleep state.
#define KXG03_STDBY_GYRO_STDBY_S_DISABLED (0x1 << 5)
//Aux2 sensor is enabled in wake state.
#define KXG03_STDBY_AUX2_STDBY_W_ENABLED (0x0 << 3)
//Aux2 sensor is disabled in wake state.
#define KXG03_STDBY_AUX2_STDBY_W_DISABLED (0x1 << 3)
//Aux1 sensor is enabled in wake state.
#define KXG03_STDBY_AUX1_STDBY_W_ENABLED (0x0 << 2)
//Aux1 sensor is disabled in wake state.
#define KXG03_STDBY_AUX1_STDBY_W_DISABLED (0x1 << 2)
//Gyro sensor is enabled in wake state.
#define KXG03_STDBY_GYRO_STDBY_W_ENABLED (0x0 << 1)
//Gyro sensor is disabled in wake state.
#define KXG03_STDBY_GYRO_STDBY_W_DISABLED (0x1 << 1)
//Accel sensor is enabled.
#define KXG03_STDBY_ACC_STDBY_ENABLED (0x0 << 0)
//Accel sensor is disabled.
#define KXG03_STDBY_ACC_STDBY_DISABLED (0x1 << 0)
//Active high soft reset.
#define KXG03_CTL_REG_1_RST (0x01 << 7)
//Temperature output is enabled in sleep mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_S_ENABLED (0x0 << 4)
//Temperature output is disabled in sleep mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_S_DISABLED (0x1 << 4)
//Temperature output is enabled in wake mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_W_ENABLED (0x0 << 3)
//Temperature output is disabled in wake mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_W_DISABLED (0x1 << 3)
//Accel self-test polarity is not inverted..
#define KXG03_CTL_REG_1_ACC_STPOL_NOT_INVERTED (0x0 << 1)
//Accel self-test polarity is inverted..
#define KXG03_CTL_REG_1_ACC_STPOL_INVERTED (0x1 << 1)
//Accel self-test is enabled.
#define KXG03_CTL_REG_1_ACC_ST (0x01 << 0)
//Active high enable for INT2 pin.
#define KXG03_INT_PIN_CTL_IEN2 (0x01 << 7)
#define KXG03_INT_PIN_CTL_IEA2_ACTIVE_LOW (0x0 << 6)
#define KXG03_INT_PIN_CTL_IEA2_ACTIVE_HIGH (0x1 << 6)
#define KXG03_INT_PIN_CTL_IEL2_LATCHED (0x0 << 4)
#define KXG03_INT_PIN_CTL_IEL2_PULSED_50US (0x1 << 4)
#define KXG03_INT_PIN_CTL_IEL2_PULSED_200US (0x2 << 4)
#define KXG03_INT_PIN_CTL_IEL2_REALTIME (0x3 << 4)
//Active high enable for INT1 pin.
#define KXG03_INT_PIN_CTL_IEN1 (0x01 << 3)
//Interrupt polarity select for INT1 pin.
#define KXG03_INT_PIN_CTL_IEA1 (0x01 << 2)
#define KXG03_INT_PIN_CTL_IEA1_ACTIVE_LOW (0x0 << 2)
#define KXG03_INT_PIN_CTL_IEA1_ACTIVE_HIGH (0x1 << 2)
#define KXG03_INT_PIN_CTL_IEL1_LATCHED (0x0 << 0)
#define KXG03_INT_PIN_CTL_IEL1_PULSED_50US (0x1 << 0)
#define KXG03_INT_PIN_CTL_IEL1_PULSED_200US (0x2 << 0)
#define KXG03_INT_PIN_CTL_IEL1_REALTIME (0x3 << 0)
//Buffer Full Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_BFI_P1 (0x01 << 7)
//Water Mark Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_WMI_P1 (0x01 << 6)
//Wake Up Function Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_WUF_P1 (0x01 << 5)
//Back To Sleep Function Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_BTS_P1 (0x01 << 4)
//Data Ready Aux2 Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_AUX2_P1 (0x01 << 3)
//Data Ready AUX1 Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_AUX1_P1 (0x01 << 2)
//Data Ready Accelerometer Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_ACC_P1 (0x01 << 1)
//Data Ready Gyroscope Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_GYRO_P1 (0x01 << 0)
#define KXG03_INT_PIN2_SEL_BFI_P2 (0x01 << 7)
#define KXG03_INT_PIN2_SEL_WMI_P2 (0x01 << 6)
#define KXG03_INT_PIN2_SEL_WUF_P2 (0x01 << 5)
#define KXG03_INT_PIN2_SEL_BTS_P2 (0x01 << 4)
#define KXG03_INT_PIN2_SEL_DRDY_AUX2_P2 (0x01 << 3)
#define KXG03_INT_PIN2_SEL_DRDY_AUX1_P2 (0x01 << 2)
#define KXG03_INT_PIN2_SEL_DRDY_ACC_P2 (0x01 << 1)
#define KXG03_INT_PIN2_SEL_DRDY_GYRO_P2 (0x01 << 0)
#define KXG03_INT_MASK1_BFIE (0x01 << 7)
#define KXG03_INT_MASK1_WMIE (0x01 << 6)
#define KXG03_INT_MASK1_WUFE (0x01 << 5)
#define KXG03_INT_MASK1_BTSE (0x01 << 4)
#define KXG03_INT_MASK1_DRDY_AUX2 (0x01 << 3)
#define KXG03_INT_MASK1_DRDY_AUX1 (0x01 << 2)
#define KXG03_INT_MASK1_DRDY_ACC (0x01 << 1)
#define KXG03_INT_MASK1_DRDY_GYRO (0x01 << 0)
//"x negative (x-) mask for WUF/BTS
#define KXG03_INT_MASK2_NXWUE (0x01 << 5)
#define KXG03_INT_MASK2_PXWUE (0x01 << 4)
#define KXG03_INT_MASK2_NYWUE (0x01 << 3)
#define KXG03_INT_MASK2_PYWUE (0x01 << 2)
#define KXG03_INT_MASK2_NZWUE (0x01 << 1)
#define KXG03_INT_MASK2_PZWUE (0x01 << 0)
#define KXG03_FSYNC_CTL_FSYNC_MODE_0 (0x0 << 4)
#define KXG03_FSYNC_CTL_FSYNC_MODE_1 (0x1 << 4)
#define KXG03_FSYNC_CTL_FSYNC_MODE_2 (0x2 << 4)
#define KXG03_FSYNC_CTL_FSYNC_MODE_3 (0x3 << 4)
//SYNC function disabled
#define KXG03_FSYNC_CTL_FSYNC_SEL_DISABLED (0x0 << 0)
//State of SYNC pin is stored in gyro x LSB bit
#define KXG03_FSYNC_CTL_FSYNC_SEL_GYRO_X_LSB (0x1 << 0)
//State of SYNC pin is stored in gyro y LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_GYRO_Y_LSB (0x2 << 0)
//State of SYNC pin is stored in gyro.z LSB bt
#define KXG03_FSYNC_CTL_FSYNC_SEL_GYRO_Z_LSB (0x3 << 0)
//State of SYNC pin is stored in accel x LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_ACCEL_X_LSB (0x4 << 0)
//State of SYNC pin is stored in accel y LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_ACCEL_Y_LSB (0x5 << 0)
//State of SYNC pin is stored in accel z LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_ACCEL_Z_LSB (0x6 << 0)
//State of SYNC pin is stored in temperature LSB bit
#define KXG03_FSYNC_CTL_FSYNC_SEL_TEMPERATURE_LSB (0x7 << 0)
//Active high back-to-sleep function enable
#define KXG03_WAKE_SLEEP_CTL1_BTS_EN (0x01 << 7)
//Active high wake-up function enable.
#define KXG03_WAKE_SLEEP_CTL1_WUF_EN (0x01 << 6)
//Forces transition to sleep state.
#define KXG03_WAKE_SLEEP_CTL1_MAN_SLEEP (0x01 << 5)
//Forces transition to wake state.
#define KXG03_WAKE_SLEEP_CTL1_MAN_WAKE (0x01 << 4)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_0P781 (0x0 << 0)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_1P563 (0x1 << 0)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_3P125 (0x2 << 0)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_6P25 (0x3 << 0)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_12P5 (0x4 << 0)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_25 (0x5 << 0)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_50 (0x6 << 0)
#define KXG03_WAKE_SLEEP_CTL1_OWUF_100 (0x7 << 0)
#define KXG03_WAKE_SLEEP_CTL2_TH_MODE_ABSOLUTE_THRESHOLD (0x0 << 1)
#define KXG03_WAKE_SLEEP_CTL2_TH_MODE_RELATIVE_THRESHOLD (0x1 << 1)
#define KXG03_WAKE_SLEEP_CTL2_C_MODE_COUNTER_CLEAR (0x0 << 0)
#define KXG03_WAKE_SLEEP_CTL2_C_MODE_COUNTER_DECREASE (0x1 << 0)
#define KXG03_BUF_CTL2_BUF_TEMP_W (0x01 << 6)
#define KXG03_BUF_CTL2_BUF_ACC_W_X (0x01 << 5)
#define KXG03_BUF_CTL2_BUF_ACC_W_Y (0x01 << 4)
#define KXG03_BUF_CTL2_BUF_ACC_W_Z (0x01 << 3)
#define KXG03_BUF_CTL2_BUF_GYR_W_X (0x01 << 2)
#define KXG03_BUF_CTL2_BUF_GYR_W_Y (0x01 << 1)
#define KXG03_BUF_CTL2_BUF_GYR_W_Z (0x01 << 0)
#define KXG03_BUF_CTL3_BUF_TEMP_W (0x01 << 6)
#define KXG03_BUF_CTL3_BUF_ACC_W_X (0x01 << 5)
#define KXG03_BUF_CTL3_BUF_ACC_W_Y (0x01 << 4)
#define KXG03_BUF_CTL3_BUF_ACC_W_Z (0x01 << 3)
#define KXG03_BUF_CTL3_BUF_GYR_W_X (0x01 << 2)
#define KXG03_BUF_CTL3_BUF_GYR_W_Y (0x01 << 1)
#define KXG03_BUF_CTL3_BUF_GYR_W_Z (0x01 << 0)
#define KXG03_BUF_CTL4_BUF_AUX2_S (0x01 << 3)
#define KXG03_BUF_CTL4_BUF_AUX1_S (0x01 << 2)
#define KXG03_BUF_CTL4_BUF_AUX2_W (0x01 << 1)
#define KXG03_BUF_CTL4_BUF_AUX1_W (0x01 << 0)
#define KXG03_BUF_EN_BUFE (0x01 << 7)
#define KXG03_BUF_EN_BUF_SYM_SYMBOL_MODE_DISABLED (0x0 << 2)
#define KXG03_BUF_EN_BUF_SYM_SINGLE_SYMBOL_MODE_ENABLED (0x1 << 2)
#define KXG03_BUF_EN_BUF_SYM_DUAL_SYMBOL_TRANS_MODE_ENABLED (0x2 << 2)
#define KXG03_BUF_EN_BUF_SYM_DUAL_SYMBOL_FRAME_MODE_ENABLED (0x3 << 2)
#define KXG03_BUF_EN_BUF_M_FIFO (0x0 << 0)
#define KXG03_BUF_EN_BUF_M_STREAM (0x1 << 0)
#define KXG03_BUF_EN_BUF_M_TRIGGER (0x2 << 0)
#define KXG03_BUF_EN_BUF_M_FILO (0x3 << 0)

 /*registers bit masks */
#define KXG03_AUX_STATUS_AUX2FAIL_MASK 0x80
#define KXG03_AUX_STATUS_AUX2ERR_MASK 0x40
#define KXG03_AUX_STATUS_AUX2ST_MASK 0x30
#define KXG03_AUX_STATUS_AUX1FAIL_MASK 0x8
#define KXG03_AUX_STATUS_AUX1ERR_MASK 0x4
#define KXG03_AUX_STATUS_AUX1ST_MASK 0x3
#define KXG03_STATUS1_INT1_MASK 0x80
#define KXG03_STATUS1_POR_MASK 0x40
#define KXG03_STATUS1_AUX2_ACT_MASK 0x20
#define KXG03_STATUS1_AUX1_ACT_MASK 0x10
#define KXG03_STATUS1_AUX_ERR_MASK 0x8
#define KXG03_STATUS1_WAKE_SLEEP_MASK 0x4
#define KXG03_STATUS1_GYRO_RUN_MASK 0x2
#define KXG03_STATUS1_GYRO_START_MASK 0x1
#define KXG03_INT1_SRC1_INT1_BFI_MASK 0x80
#define KXG03_INT1_SRC1_INT1_WMI_MASK 0x40
#define KXG03_INT1_SRC1_INT1_WUFS_MASK 0x20
#define KXG03_INT1_SRC1_INT1_BTS_MASK 0x10
#define KXG03_INT1_SRC1_INT1_DRDY_AUX2_MASK 0x8
#define KXG03_INT1_SRC1_INT1_DRDY_AUX1_MASK 0x4
#define KXG03_INT1_SRC1_INT1_DRDY_ACC_MASK 0x2
#define KXG03_INT1_SRC1_INT1_DRDY_GYRO_MASK 0x1
#define KXG03_INT1_SRC2_INT1_XNWU_MASK 0x20
#define KXG03_INT1_SRC2_INT1_XPWU_MASK 0x10
#define KXG03_INT1_SRC2_INT1_YNWU_MASK 0x8
#define KXG03_INT1_SRC2_INT1_YPWU_MASK 0x4
#define KXG03_INT1_SRC2_INT1_ZNWU_MASK 0x2
#define KXG03_INT1_SRC2_INT1_ZPWU_MASK 0x1
#define KXG03_STATUS2_INT2_MASK 0x80
#define KXG03_STATUS2_POR_MASK 0x40
#define KXG03_STATUS2_AUX2_ACT_MASK 0x20
#define KXG03_STATUS2_AUX1_ACT_MASK 0x10
#define KXG03_STATUS2_AUX_ERR_MASK 0x8
#define KXG03_STATUS2_WAKE_SLEEP_MASK 0x4
#define KXG03_STATUS2_GYRO_RUN_MASK 0x2
#define KXG03_STATUS2_GYRO_START_MASK 0x1
#define KXG03_INT2_SRC1_INT2_BFI_MASK 0x80
#define KXG03_INT2_SRC1_INT2_WMI_MASK 0x40
#define KXG03_INT2_SRC1_INT2_WUFS_MASK 0x20
#define KXG03_INT2_SRC1_INT2_BTS_MASK 0x10
#define KXG03_INT2_SRC1_INT2_DRDY_AUX2_MASK 0x8
#define KXG03_INT2_SRC1_INT2_DRDY_AUX1_MASK 0x4
#define KXG03_INT2_SRC1_INT2_DRDY_ACC_MASK 0x2
#define KXG03_INT2_SRC1_INT2_DRDY_GYRO_MASK 0x1
#define KXG03_INT2_SRC2_INT2_XNWU_MASK 0x20
#define KXG03_INT2_SRC2_INT2_XPWU_MASK 0x10
#define KXG03_INT2_SRC2_INT2_YNWU_MASK 0x8
#define KXG03_INT2_SRC2_INT2_YPWU_MASK 0x4
#define KXG03_INT2_SRC2_INT2_ZNWU_MASK 0x2
#define KXG03_INT2_SRC2_INT2_ZPWU_MASK 0x1
#define KXG03_ACCEL_ODR_WAKE_LPMODE_W_MASK 0x80
//The max over sampling rate (or max number of samples averaged) varies with ODR
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_MASK 0x70
//accelerometer ODR in wake mode
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_MASK 0xf
#define KXG03_ACCEL_ODR_SLEEP_LPMODE_S_MASK 0x80
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_MASK 0x70
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_MASK 0xf
//Accelerometer sleep mode full scale range select.
#define KXG03_ACCEL_CTL_ACC_FS_S_MASK 0xc0
//Accelerometer wake mode full scale range select.
#define KXG03_ACCEL_CTL_ACC_FS_W_MASK 0xc
//Gyroscope angular velocity range wake mode
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_MASK 0xc0
//Gyroscope bandwidth selection in wake mode.
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_MASK 0x30
//gyroscope ODR in wake mode
#define KXG03_GYRO_ODR_WAKE_ODRG_W_MASK 0xf
//Gyroscope angular velocity range sleep mode
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_MASK 0xc0
//Gyroscope bandwidth selection in sleep mode.
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_MASK 0x30
//gyroscope ODR in sleep mode
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_MASK 0xf
#define KXG03_STDBY_AUX2_STDBY_S_MASK 0x80
#define KXG03_STDBY_AUX1_STDBY_S_MASK 0x40
#define KXG03_STDBY_GYRO_STDBY_S_MASK 0x20
#define KXG03_STDBY_AUX2_STDBY_W_MASK 0x8
#define KXG03_STDBY_AUX1_STDBY_W_MASK 0x4
#define KXG03_STDBY_GYRO_STDBY_W_MASK 0x2
#define KXG03_STDBY_ACC_STDBY_MASK 0x1
#define KXG03_CTL_REG_1_RST_MASK 0x80
#define KXG03_CTL_REG_1_TEMP_STDBY_S_MASK 0x10
#define KXG03_CTL_REG_1_TEMP_STDBY_W_MASK 0x8
#define KXG03_CTL_REG_1_ACC_STPOL_MASK 0x2
#define KXG03_CTL_REG_1_ACC_ST_MASK 0x1
#define KXG03_INT_PIN_CTL_IEN2_MASK 0x80
//Interrupt polarity select for INT2 pin.
#define KXG03_INT_PIN_CTL_IEA2_MASK 0x40
//Interrupt latch mode select for INT2 pin
#define KXG03_INT_PIN_CTL_IEL2_MASK 0x30
#define KXG03_INT_PIN_CTL_IEN1_MASK 0x8
#define KXG03_INT_PIN_CTL_IEA1_MASK 0x4
//Interrupt latch mode select for INT1 pin
#define KXG03_INT_PIN_CTL_IEL1_MASK 0x3
#define KXG03_INT_PIN1_SEL_BFI_P1_MASK 0x80
#define KXG03_INT_PIN1_SEL_WMI_P1_MASK 0x40
#define KXG03_INT_PIN1_SEL_WUF_P1_MASK 0x20
#define KXG03_INT_PIN1_SEL_BTS_P1_MASK 0x10
#define KXG03_INT_PIN1_SEL_DRDY_AUX2_P1_MASK 0x8
#define KXG03_INT_PIN1_SEL_DRDY_AUX1_P1_MASK 0x4
#define KXG03_INT_PIN1_SEL_DRDY_ACC_P1_MASK 0x2
#define KXG03_INT_PIN1_SEL_DRDY_GYRO_P1_MASK 0x1
#define KXG03_INT_PIN2_SEL_BFI_P2_MASK 0x80
#define KXG03_INT_PIN2_SEL_WMI_P2_MASK 0x40
#define KXG03_INT_PIN2_SEL_WUF_P2_MASK 0x20
#define KXG03_INT_PIN2_SEL_BTS_P2_MASK 0x10
#define KXG03_INT_PIN2_SEL_DRDY_AUX2_P2_MASK 0x8
#define KXG03_INT_PIN2_SEL_DRDY_AUX1_P2_MASK 0x4
#define KXG03_INT_PIN2_SEL_DRDY_ACC_P2_MASK 0x2
#define KXG03_INT_PIN2_SEL_DRDY_GYRO_P2_MASK 0x1
#define KXG03_INT_MASK1_BFIE_MASK 0x80
#define KXG03_INT_MASK1_WMIE_MASK 0x40
#define KXG03_INT_MASK1_WUFE_MASK 0x20
#define KXG03_INT_MASK1_BTSE_MASK 0x10
#define KXG03_INT_MASK1_DRDY_AUX2_MASK 0x8
#define KXG03_INT_MASK1_DRDY_AUX1_MASK 0x4
#define KXG03_INT_MASK1_DRDY_ACC_MASK 0x2
#define KXG03_INT_MASK1_DRDY_GYRO_MASK 0x1
#define KXG03_INT_MASK2_NXWUE_MASK 0x20
#define KXG03_INT_MASK2_PXWUE_MASK 0x10
#define KXG03_INT_MASK2_NYWUE_MASK 0x8
#define KXG03_INT_MASK2_PYWUE_MASK 0x4
#define KXG03_INT_MASK2_NZWUE_MASK 0x2
#define KXG03_INT_MASK2_PZWUE_MASK 0x1
#define KXG03_FSYNC_CTL_FSYNC_MODE_MASK 0x30
#define KXG03_FSYNC_CTL_FSYNC_SEL_MASK 0x7
#define KXG03_WAKE_SLEEP_CTL1_BTS_EN_MASK 0x80
#define KXG03_WAKE_SLEEP_CTL1_WUF_EN_MASK 0x40
#define KXG03_WAKE_SLEEP_CTL1_MAN_SLEEP_MASK 0x20
#define KXG03_WAKE_SLEEP_CTL1_MAN_WAKE_MASK 0x10
//the Output Data Rate for the wake up (motion detection).
#define KXG03_WAKE_SLEEP_CTL1_OWUF_MASK 0x7
//debounce counter clear mode.
#define KXG03_WAKE_SLEEP_CTL2_C_MODE_MASK 0x1
#define KXG03_BUF_CTL2_BUF_TEMP_W_MASK 0x40
#define KXG03_BUF_CTL2_BUF_ACC_W_X_MASK 0x20
#define KXG03_BUF_CTL2_BUF_ACC_W_Y_MASK 0x10
#define KXG03_BUF_CTL2_BUF_ACC_W_Z_MASK 0x8
#define KXG03_BUF_CTL2_BUF_GYR_W_X_MASK 0x4
#define KXG03_BUF_CTL2_BUF_GYR_W_Y_MASK 0x2
#define KXG03_BUF_CTL2_BUF_GYR_W_Z_MASK 0x1
#define KXG03_BUF_CTL3_BUF_TEMP_W_MASK 0x40
#define KXG03_BUF_CTL3_BUF_ACC_W_X_MASK 0x20
#define KXG03_BUF_CTL3_BUF_ACC_W_Y_MASK 0x10
#define KXG03_BUF_CTL3_BUF_ACC_W_Z_MASK 0x8
#define KXG03_BUF_CTL3_BUF_GYR_W_X_MASK 0x4
#define KXG03_BUF_CTL3_BUF_GYR_W_Y_MASK 0x2
#define KXG03_BUF_CTL3_BUF_GYR_W_Z_MASK 0x1
#define KXG03_BUF_CTL4_BUF_AUX2_S_MASK 0x8
#define KXG03_BUF_CTL4_BUF_AUX1_S_MASK 0x4
#define KXG03_BUF_CTL4_BUF_AUX2_W_MASK 0x2
#define KXG03_BUF_CTL4_BUF_AUX1_W_MASK 0x1
#define KXG03_BUF_EN_BUFE_MASK 0x80
#define KXG03_BUF_EN_BUF_SYM_MASK 0xc
#define KXG03_BUF_EN_BUF_M_MASK 0x3


#if 0 // OLD VERSION




 /* registers */
//
#define KXG03_TEMP_OUT_L 0x00
//
#define KXG03_TEMP_OUT_H 0x01
//
#define KXG03_GYRO_XOUT_L 0x02
//
#define KXG03_GYRO_XOUT_H 0x03
//
#define KXG03_GYRO_YOUT_L 0x04
//
#define KXG03_GYRO_YOUT_H 0x05
//
#define KXG03_GYRO_ZOUT_L 0x06
//
#define KXG03_GYRO_ZOUT_H 0x07
//
#define KXG03_ACC_XOUT_L 0x08
//
#define KXG03_ACC_XOUT_H 0x09
//
#define KXG03_ACC_YOUT_L 0x0A
//
#define KXG03_ACC_YOUT_H 0x0B
//
#define KXG03_ACC_ZOUT_L 0x0C
//
#define KXG03_ACC_ZOUT_H 0x0D
//Auxiliary Sensor #1 output data bytes AUX1_OUT1 through AUX1_OUT6
#define KXG03_AUX1_OUT1 0x0E
//
#define KXG03_AUX1_OUT2 0x0F
//
#define KXG03_AUX1_OUT3 0x10
//
#define KXG03_AUX1_OUT4 0x11
//
#define KXG03_AUX1_OUT5 0x12
//
#define KXG03_AUX1_OUT6 0x13
//Auxiliary Sensor #2 output data bytes AUX2_OUT1 through AUX2_OUT6
#define KXG03_AUX2_OUT1 0x14
//
#define KXG03_AUX2_OUT2 0x15
//
#define KXG03_AUX2_OUT3 0x16
//
#define KXG03_AUX2_OUT4 0x17
//
#define KXG03_AUX2_OUT5 0x18
//
#define KXG03_AUX2_OUT6 0x19
//Number of ODR cycles spent in wake state as measured in accelerometer ODRa_wake/ODRa_sleep periods. Data byte WAKE_CNT_L and WAKE_CNT_H.
#define KXG03_WAKE_CNT_L 0x1A
//
#define KXG03_WAKE_CNT_H 0x1B
//Number of ODR cycles spent in sleep state as measured in accelerometer ODRa_wake/ODRa_sleep periods. Data byte SLEEP_CNT_L and SLEEP_CNT_H.
#define KXG03_SLEEP_CNT_L 0x1C
//
#define KXG03_SLEEP_CNT_H 0x1D
//Reports the number of data packets (ODR cycles) currently stored in the buffer.
#define KXG03_BUF_SMPLEV_L 0x1E
//
#define KXG03_BUF_SMPLEV_H 0x1F
//Reports the number of data packets lost since buffer has been filled.
#define KXG03_BUF_PAST_L 0x20
//
#define KXG03_BUF_PAST_H 0x21
//Reports the status of Auxiliary Sensors AUX1 and AUX2.
#define KXG03_AUX_STATUS 0x22
//
#define KXG03_WHO_AM_I 0x30
//Individual Identification (serial number).
#define KXG03_SN1_MIR 0x31
//
#define KXG03_SN2_MIR 0x32
//
#define KXG03_SN3_MIR 0x33
//
#define KXG03_SN4_MIR 0x34
//
#define KXG03_36STATUS1 0x36
//
#define KXG03_INT1_SRC1 0x37
//
#define KXG03_INT1_SRC2 0x38
//Reading this register releases int1 source registers
#define KXG03_39INT1_L 0x39
//
#define KXG03_STATUS2 0x3A
//
#define KXG03_INT2_SRC1 0x3B
//
#define KXG03_INT2_SRC2 0x3C
//Reading this register releases int2 source registers
#define KXG03_INT2_L 0x3D
//Accelerometer Wake Mode Control register.
#define KXG03_3EACCEL_ODR_WAKE 0x3E
//
#define KXG03_3FACCEL_ODR_SLEEP 0x3F
//
#define KXG03_40ACCEL_CTL 0x40
//Gyroscope Wake Mode Control register.
#define KXG03_41GYRO_ODR_WAKE 0x41
//
#define KXG03_42GYRO_ODR_SLEEP 0x42
//
#define KXG03_43STDBY 0x43
//Special control register 1
#define KXG03_CTL_REG_1 0x44
//
#define KXG03_INT_PIN_CTL 0x45
//Physical interrupt pin INT1 select register.
#define KXG03_46INT_PIN1_SEL 0x46
//
#define KXG03_INT_PIN2_SEL 0x47
//Buffer Full Interrupt enable/mask bit.
#define KXG03_48INT_MASK1 0x48
//which axis and direction of detected motion can cause an interrupt.
#define KXG03_INT_MASK2 0x49
//External Synchronous control register.
#define KXG03_FSYNC_CTL 0x4A
//
#define KXG03_WAKE_SLEEP_CTL1 0x4B
//WUF and BTS threshold mode.
#define KXG03_WAKE_SLEEP_CTL2 0x4C
//
#define KXG03_WUF_TH 0x4D
//
#define KXG03_WUF_COUNTER 0x4E
//
#define KXG03_BTS_TH 0x4F
//
#define KXG03_BTS_COUNTER 0x50
//
#define KXG03_AUX_I2C_CTL_REG 0x51
//
#define KXG03_AUX_I2C_SAD1 0x52
//
#define KXG03_AUX_I2C_REG1 0x53
//
#define KXG03_AUX_I2C_CTL1 0x54
//
#define KXG03_AUX_I2C_BIT1 0x55
//
#define KXG03_AUX_I2C_ODR1_W 0x56
//
#define KXG03_AUX_I2C_ODR1_S 0x57
//
#define KXG03_AUX_I2C_SAD2 0x58
//
#define KXG03_AUX_I2C_REG2 0x59
//
#define KXG03_AUX_I2C_CTL2 0x5A
//
#define KXG03_AUX_I2C_BIT2 0x5B
//
#define KXG03_AUX_I2C_ODR2_W 0x5C
//
#define KXG03_AUX_I2C_ODR2_S 0x5D
//Buffer watermark threshold level L
#define KXG03_BUF_WMITH_L 0x75
//Buffer watermark threshold level H
#define KXG03_BUF_WMITH_H 0x76
//Buffer Trigger mode threshold L
#define KXG03_BUF_TRIGTH_L 0x77
//Buffer Trigger mode threshold H
#define KXG03_BUF_TRIGTH_H 0x78
//Wake mode channels to buffer
#define KXG03_BUF_CTL2 0x79
//Sleep mode channels to buffer
#define KXG03_BUF_CTL3 0x7A
//
#define KXG03_BUF_CTL4 0x7B
//
#define KXG03_BUF_EN 0x7C
//
#define KXG03_BUF_STATUS 0x7D
//
#define KXG03_BUF_CLEAR 0x7E
//
#define KXG03_BUF_READ 0x7F

 /* registers bits */ 
//Aux2 command sequence failure flag
#define KXG03_AUX_STATUS_AUX2FAIL (0x01 << 7)
//Aux2 data read error flag.
#define KXG03_AUX_STATUS_AUX2ERR (0x01 << 6)
//Aux1 has not been enabled or ASIC has successfully sent disable cmd.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_DISABLED (0x0 << 4)
//ASIC is attempting to enable aux sensor via enable sequence.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_WAITING_ENABLE (0x1 << 4)
//ASIC is attempting to disable aux sensor via disable sequence.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_WAITING_DISABLE (0x2 << 4)
//ASIC has successfully sent aux enable cmd.
#define KXG03_AUX_STATUS_AUX2ST_AUX2_RUNNING (0x3 << 4)
//Aux1 command sequence failure flag
#define KXG03_AUX_STATUS_AUX1FAIL (0x01 << 3)
//Aux1 data read error flag.
#define KXG03_AUX_STATUS_AUX1ERR (0x01 << 2)
//Aux1 has not been enabled or ASIC has successfully sent disable cmd.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_DISABLED (0x0 << 0)
//ASIC is attempting to enable aux sensor via enable sequence.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_WAITING_ENABLE (0x1 << 0)
//ASIC is attempting to disable aux sensor via disable sequence.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_WAITING_DISABLE (0x2 << 0)
//ASIC has successfully sent aux enable cmd.
#define KXG03_AUX_STATUS_AUX1ST_AUX1_RUNNING (0x3 << 0)
//reports Logical OR of non-masked interrupt sources sent to INT1
#define KXG03_36STATUS1_80INT1 (0x01 << 7)
//Reset indicator.
#define KXG03_36STATUS1_40POR (0x01 << 6)
//Auxiliary sensor #2 active flag.
#define KXG03_36STATUS1_20AUX2_ACT (0x01 << 5)
//Auxiliary sensor #1 active flag.
#define KXG03_36STATUS1_10AUX1_ACT (0x01 << 4)
//
#define KXG03_36STATUS1_08AUX_ERR (0x01 << 3)
//
#define KXG03_36STATUS1_04WAKE_SLEEP (0x01 << 2)
//
#define KXG03_36STATUS1_02GYRO_RUN (0x01 << 1)
//
#define KXG03_36STATUS1_01GYRO_START (0x01 << 0)
//Buffer full interrupt.
#define KXG03_INT1_SRC1_INT1_BFI (0x01 << 7)
//Buffer water mark interrupt.
#define KXG03_INT1_SRC1_INT1_WMI (0x01 << 6)
//Wake up function interrupt.
#define KXG03_INT1_SRC1_INT1_WUFS (0x01 << 5)
//Back to sleep interrupt.
#define KXG03_INT1_SRC1_INT1_BTS (0x01 << 4)
//Aux2 data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_AUX2 (0x01 << 3)
//Aux1 data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_AUX1 (0x01 << 2)
//Accel data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_ACC (0x01 << 1)
//Gyro data ready interrupt.
#define KXG03_INT1_SRC1_INT1_DRDY_GYRO (0x01 << 0)
//"Wake up event detected on x-axis
#define KXG03_INT1_SRC2_INT1_XNWU (0x01 << 5)
//"Wake up event detected on x-axis
#define KXG03_INT1_SRC2_INT1_XPWU (0x01 << 4)
//"Wake up event detected on y-axis
#define KXG03_INT1_SRC2_INT1_YNWU (0x01 << 3)
//"Wake up event detected on y-axis
#define KXG03_INT1_SRC2_INT1_YPWU (0x01 << 2)
//"Wake up event detected on z-axis
#define KXG03_INT1_SRC2_INT1_ZNWU (0x01 << 1)
//"Wake up event detected on z-axis
#define KXG03_INT1_SRC2_INT1_ZPWU (0x01 << 0)
//
#define KXG03_STATUS2_INT2 (0x01 << 7)
//
#define KXG03_STATUS2_POR (0x01 << 6)
//
#define KXG03_STATUS2_AUX2_ACT (0x01 << 5)
//
#define KXG03_STATUS2_AUX1_ACT (0x01 << 4)
//
#define KXG03_STATUS2_AUX_ERR (0x01 << 3)
//
#define KXG03_STATUS2_WAKESLEEP (0x01 << 2)
//
#define KXG03_STATUS2_GYRO_RUN (0x01 << 1)
//
#define KXG03_STATUS2_GYRO_START (0x01 << 0)
//
#define KXG03_INT2_SRC1_INT2_BFI (0x01 << 7)
//
#define KXG03_INT2_SRC1_INT2_WMI (0x01 << 6)
//
#define KXG03_INT2_SRC1_INT2_WUFS (0x01 << 5)
//
#define KXG03_INT2_SRC1_INT2_BTS (0x01 << 4)
//
#define KXG03_INT2_SRC1_INT2_DRDY_AUX2 (0x01 << 3)
//
#define KXG03_INT2_SRC1_INT2_DRDY_AUX1 (0x01 << 2)
//
#define KXG03_INT2_SRC1_INT2_DRDY_ACC (0x01 << 1)
//
#define KXG03_INT2_SRC1_INT2_DRDY_GYRO (0x01 << 0)
//"Wake up event detected on x-axis
#define KXG03_INT2_SRC2_INT2_XNWU (0x01 << 5)
//"Wake up event detected on x-axis
#define KXG03_INT2_SRC2_INT2_XPWU (0x01 << 4)
//"Wake up event detected on y-axis
#define KXG03_INT2_SRC2_INT2_YNWU (0x01 << 3)
//"Wake up event detected on y-axis
#define KXG03_INT2_SRC2_INT2_YPWU (0x01 << 2)
//"Wake up event detected on z-axis
#define KXG03_INT2_SRC2_INT2_ZNWU (0x01 << 1)
//"Wake up event detected on z-axis
#define KXG03_INT2_SRC2_INT2_ZPWU (0x01 << 0)
//Accel low power mode is disabled in wake state. Accel operates at max sampling rate and navg_wake is ignored.
#define KXG03_ACCEL_ODR_WAKE_LPMODE_W_DISABLED (0x0 << 7)
//Accel low power mode is enabled in wake state. Accel operates in duty cycle mode with number of samples set by navg_wake.
#define KXG03_ACCEL_ODR_WAKE_LPMODE_W_ENABLED (0x1 << 7)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG1 (0x0 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG2 (0x1 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG4 (0x2 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG8 (0x3 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG16 (0x4 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG32 (0x5 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG64 (0x6 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_AVG128 (0x7 << 4)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_0P781 (0x0 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_1P563 (0x1 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_3P125 (0x2 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_6P25 (0x3 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_12P5 (0x4 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_25 (0x5 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_50 (0x6 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_100 (0x7 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_200 (0x8 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_400 (0x9 << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_800 (0xa << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_1600 (0xb << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_3K2 (0xc << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_6K4 (0xd << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_12K8 (0xe << 0)
//
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_51K2 (0xf << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_LPMODE_S_DISABLED (0x0 << 7)
//
#define KXG03_ACCEL_ODR_SLEEP_LPMODE_S_ENABLED (0x1 << 7)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG1 (0x0 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG2 (0x1 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG4 (0x2 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG8 (0x3 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG16 (0x4 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG32 (0x5 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG64 (0x6 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_AVG128 (0x7 << 4)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_0P781 (0x0 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_1P563 (0x1 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_3P125 (0x2 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_6P25 (0x3 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_12P5 (0x4 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_25 (0x5 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_50 (0x6 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_100 (0x7 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_200 (0x8 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_400 (0x9 << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_800 (0xa << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_1600 (0xb << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_3K2 (0xc << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_6K4 (0xd << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_12K8 (0xe << 0)
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_51K2 (0xf << 0)
//
#define KXG03_ACCEL_CTL_ACC_FS_S_2G (0x0 << 6)
//
#define KXG03_ACCEL_CTL_ACC_FS_S_4G (0x1 << 6)
//
#define KXG03_ACCEL_CTL_ACC_FS_S_8G (0x2 << 6)
//
#define KXG03_ACCEL_CTL_ACC_FS_S_16G (0x3 << 6)
//
#define KXG03_ACCEL_CTL_ACC_FS_W_2G (0x0 << 2)
//
#define KXG03_ACCEL_CTL_ACC_FS_W_4G (0x1 << 2)
//
#define KXG03_ACCEL_CTL_ACC_FS_W_8G (0x2 << 2)
//
#define KXG03_ACCEL_CTL_ACC_FS_W_16G (0x3 << 2)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_256 (0x0 << 6)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_512 (0x1 << 6)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_1024 (0x2 << 6)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_2048 (0x3 << 6)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_10 (0x0 << 4)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_20 (0x1 << 4)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_40 (0x2 << 4)
//
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_160 (0x3 << 4)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_0P781 (0x0 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_1P563 (0x1 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_3P125 (0x2 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_6P25 (0x3 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_12P5 (0x4 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_25 (0x5 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_50 (0x6 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_100 (0x7 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_200 (0x8 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_400 (0x9 << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_800 (0xa << 0)
//
#define KXG03_GYRO_ODR_WAKE_ODRG_W_1600 (0xb << 0)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_256 (0x0 << 6)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_512 (0x1 << 6)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_1024 (0x2 << 6)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_2048 (0x3 << 6)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_10 (0x0 << 4)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_20 (0x1 << 4)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_40 (0x2 << 4)
//
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_160 (0x3 << 4)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_0P781 (0x0 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_1P563 (0x1 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_3P125 (0x2 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_6P25 (0x3 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_12P5 (0x4 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_25 (0x5 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_50 (0x6 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_100 (0x7 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_200 (0x8 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_400 (0x9 << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_800 (0xa << 0)
//
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_1600 (0xb << 0)
//Aux2 sensor is enabled in sleep state.
#define KXG03_STDBY_AUX2_STDBY_S_ENABLED (0x0 << 7)
//Aux2 sensor is disabled in sleep state.
#define KXG03_STDBY_AUX2_STDBY_S_DISABLED (0x1 << 7)
//Aux1 sensor is enabled in sleep state.
#define KXG03_STDBY_AUX1_STDBY_S_ENABLED (0x0 << 6)
//Aux1 sensor is disabled in sleep state.
#define KXG03_STDBY_AUX1_STDBY_S_DISABLED (0x1 << 6)
//Gyro sensor is enabled in sleep state.
#define KXG03_STDBY_GYRO_STDBY_S_ENABLED (0x0 << 5)
//Gyro sensor is disabled in sleep state.
#define KXG03_STDBY_GYRO_STDBY_S_DISABLED (0x1 << 5)
//Aux2 sensor is enabled in wake state.
#define KXG03_STDBY_AUX2_STDBY_W_ENABLED (0x0 << 3)
//Aux2 sensor is disabled in wake state.
#define KXG03_STDBY_AUX2_STDBY_W_DISABLED (0x1 << 3)
//Aux1 sensor is enabled in wake state.
#define KXG03_STDBY_AUX1_STDBY_W_ENABLED (0x0 << 2)
//Aux1 sensor is disabled in wake state.
#define KXG03_STDBY_AUX1_STDBY_W_DISABLED (0x1 << 2)
//Gyro sensor is enabled in wake state.
#define KXG03_STDBY_GYRO_STDBY_W_ENABLED (0x0 << 1)
//Gyro sensor is disabled in wake state.
#define KXG03_STDBY_GYRO_STDBY_W_DISABLED (0x1 << 1)
//Accel sensor is enabled.
#define KXG03_STDBY_ACC_STDBY_ENABLED (0x0 << 0)
//Accel sensor is disabled.
#define KXG03_STDBY_ACC_STDBY_DISABLED (0x1 << 0)
//Active high soft reset.
#define KXG03_CTL_REG_1_RST (0x01 << 7)
//Temperature output is enabled in sleep mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_S_ENABLED (0x0 << 4)
//Temperature output is disabled in sleep mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_S_DISABLED (0x1 << 4)
//Temperature output is enabled in wake mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_W_ENABLED (0x0 << 3)
//Temperature output is disabled in wake mode.
#define KXG03_CTL_REG_1_TEMP_STDBY_W_DISABLED (0x1 << 3)
//Accel self-test polarity is not inverted..
#define KXG03_CTL_REG_1_ACC_STPOL_NOT_INVERTED (0x0 << 1)
//Accel self-test polarity is inverted..
#define KXG03_CTL_REG_1_ACC_STPOL_INVERTED (0x1 << 1)
//Accel self-test is enabled.
#define KXG03_CTL_REG_1_ACC_ST (0x01 << 0)
//Active high enable for INT2 pin.
#define KXG03_INT_PIN_CTL_IEN2 (0x01 << 7)
//
#define KXG03_INT_PIN_CTL_IEA2_ACTIVE_LOW (0x0 << 6)
//
#define KXG03_INT_PIN_CTL_IEA2_ACTIVE_HIGH (0x1 << 6)
//
#define KXG03_INT_PIN_CTL_IEL2_LATCHED (0x0 << 4)
//
#define KXG03_INT_PIN_CTL_IEL2_PULSED_50US (0x1 << 4)
//
#define KXG03_INT_PIN_CTL_IEL2_PULSED_200US (0x2 << 4)
//
#define KXG03_INT_PIN_CTL_IEL2_REALTIME (0x3 << 4)
//Active high enable for INT1 pin.
#define KXG03_INT_PIN_CTL_IEN1 (0x01 << 3)
//Interrupt polarity select for INT1 pin.
#define KXG03_INT_PIN_CTL_IEA1 (0x01 << 2)
//
#define KXG03_INT_PIN_CTL_IEA1_ACTIVE_LOW (0x0 << 2)
//
#define KXG03_INT_PIN_CTL_IEA1_ACTIVE_HIGH (0x1 << 2)
//
#define KXG03_INT_PIN_CTL_IEL1_LATCHED (0x0 << 0)
//
#define KXG03_INT_PIN_CTL_IEL1_PULSED_50US (0x1 << 0)
//
#define KXG03_INT_PIN_CTL_IEL1_PULSED_200US (0x2 << 0)
//
#define KXG03_INT_PIN_CTL_IEL1_REALTIME (0x3 << 0)
//Buffer Full Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_BFI_P1 (0x01 << 7)
//Water Mark Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_WMI_P1 (0x01 << 6)
//Wake Up Function Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_WUF_P1 (0x01 << 5)
//Back To Sleep Function Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_BTS_P1 (0x01 << 4)
//Data Ready Aux2 Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_AUX2_P1 (0x01 << 3)
//Data Ready AUX1 Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_AUX1_P1 (0x01 << 2)
//Data Ready Accelerometer Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_ACC_P1 (0x01 << 1)
//Data Ready Gyroscope Interrupt for INT1 pin.
#define KXG03_INT_PIN1_SEL_DRDY_GYRO_P1 (0x01 << 0)
//
#define KXG03_INT_PIN2_SEL_BFI_P2 (0x01 << 7)
//
#define KXG03_INT_PIN2_SEL_WMI_P2 (0x01 << 6)
//
#define KXG03_INT_PIN2_SEL_WUF_P2 (0x01 << 5)
//
#define KXG03_INT_PIN2_SEL_BTS_P2 (0x01 << 4)
//
#define KXG03_INT_PIN2_SEL_DRDY_AUX2_P2 (0x01 << 3)
//
#define KXG03_INT_PIN2_SEL_DRDY_AUX1_P2 (0x01 << 2)
//
#define KXG03_INT_PIN2_SEL_DRDY_ACC_P2 (0x01 << 1)
//
#define KXG03_INT_PIN2_SEL_DRDY_GYRO_P2 (0x01 << 0)
//
#define KXG03_INT_MASK1_BFIE (0x01 << 7)
//
#define KXG03_INT_MASK1_WMIE (0x01 << 6)
//
#define KXG03_INT_MASK1_WUFE (0x01 << 5)
//
#define KXG03_INT_MASK1_BTSE (0x01 << 4)
//
#define KXG03_INT_MASK1_DRDY_AUX2 (0x01 << 3)
//
#define KXG03_INT_MASK1_DRDY_AUX1 (0x01 << 2)
//
#define KXG03_48INT_MASK1_02DRDY_ACC (0x01 << 1)
//
#define KXG03_48INT_MASK1_01DRDY_GYRO (0x01 << 0)
//"x negative (x-) mask for WUF/BTS
#define KXG03_INT_MASK2_XNWUE (0x01 << 5)
//
#define KXG03_INT_MASK2_XPWUE (0x01 << 4)
//
#define KXG03_INT_MASK2_YNWUE (0x01 << 3)
//
#define KXG03_INT_MASK2_YPWUE (0x01 << 2)
//
#define KXG03_INT_MASK2_ZNWUE (0x01 << 1)
//
#define KXG03_INT_MASK2_ZPWUE (0x01 << 0)
//
#define KXG03_FSYNC_CTL_FSYNC_MODE_0 (0x0 << 4)
//
#define KXG03_FSYNC_CTL_FSYNC_MODE_1 (0x1 << 4)
//
#define KXG03_FSYNC_CTL_FSYNC_MODE_2 (0x2 << 4)
//
#define KXG03_FSYNC_CTL_FSYNC_MODE_3 (0x3 << 4)
//SYNC function disabled
#define KXG03_FSYNC_CTL_FSYNC_SEL_DISABLED (0x0 << 0)
//State of SYNC pin is stored in gyro x LSB bit
#define KXG03_FSYNC_CTL_FSYNC_SEL_GYRO_X_LSB (0x1 << 0)
//State of SYNC pin is stored in gyro y LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_GYRO_Y_LSB (0x2 << 0)
//State of SYNC pin is stored in gyro.z LSB bt
#define KXG03_FSYNC_CTL_FSYNC_SEL_GYRO_Z_LSB (0x3 << 0)
//State of SYNC pin is stored in accel x LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_ACCEL_X_LSB (0x4 << 0)
//State of SYNC pin is stored in accel y LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_ACCEL_Y_LSB (0x5 << 0)
//State of SYNC pin is stored in accel z LSB bit.
#define KXG03_FSYNC_CTL_FSYNC_SEL_ACCEL_Z_LSB (0x6 << 0)
//State of SYNC pin is stored in temperature LSB bit
#define KXG03_FSYNC_CTL_FSYNC_SEL_TEMPERATURE_LSB (0x7 << 0)
//Active high back-to-sleep function enable
#define KXG03_WAKE_SLEEP_CTL1_BTS_EN (0x01 << 7)
//Active high wake-up function enable.
#define KXG03_WAKE_SLEEP_CTL1_WUF_EN (0x01 << 6)
//Forces transition to sleep state.
#define KXG03_WAKE_SLEEP_CTL1_MAN_SLEEP (0x01 << 5)
//Forces transition to wake state.
#define KXG03_WAKE_SLEEP_CTL1_MAN_WAKE (0x01 << 4)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_0P781 (0x0 << 0)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_1P563 (0x1 << 0)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_3P125 (0x2 << 0)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_6P25 (0x3 << 0)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_12P5 (0x4 << 0)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_25 (0x5 << 0)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_50 (0x6 << 0)
//
#define KXG03_WAKE_SLEEP_CTL1_OWUF_100 (0x7 << 0)
//
#define KXG03_WAKE_SLEEP_CTL2_TH_MODE_ABSOLUTE_THRESHOLD (0x0 << 1)
//
#define KXG03_WAKE_SLEEP_CTL2_TH_MODE_RELATIVE_THRESHOLD (0x1 << 1)
//
#define KXG03_WAKE_SLEEP_CTL2_C_MODE_COUNTER_CLEAR (0x0 << 0)
//
#define KXG03_WAKE_SLEEP_CTL2_C_MODE_COUNTER_DECREASE (0x1 << 0)
//
#define KXG03_BUF_CTL2_BUF_TEMP_W (0x01 << 6)
//
#define KXG03_BUF_CTL2_BUF_ACC_W_X (0x01 << 5)
//
#define KXG03_BUF_CTL2_BUF_ACC_W_Y (0x01 << 4)
//
#define KXG03_BUF_CTL2_BUF_ACC_W_Z (0x01 << 3)
//
#define KXG03_BUF_CTL2_BUF_GYR_W_X (0x01 << 2)
//
#define KXG03_BUF_CTL2_BUF_GYR_W_Y (0x01 << 1)
//
#define KXG03_BUF_CTL2_BUF_GYR_W_Z (0x01 << 0)
//
#define KXG03_BUF_CTL3_BUF_TEMP_W (0x01 << 6)
//
#define KXG03_BUF_CTL3_BUF_ACC_W_X (0x01 << 5)
//
#define KXG03_BUF_CTL3_BUF_ACC_W_Y (0x01 << 4)
//
#define KXG03_BUF_CTL3_BUF_ACC_W_Z (0x01 << 3)
//
#define KXG03_BUF_CTL3_BUF_GYR_W_X (0x01 << 2)
//
#define KXG03_BUF_CTL3_BUF_GYR_W_Y (0x01 << 1)
//
#define KXG03_BUF_CTL3_BUF_GYR_W_Z (0x01 << 0)
//
#define KXG03_BUF_CTL4_BUF_AUX2_S (0x01 << 3)
//
#define KXG03_BUF_CTL4_BUF_AUX1_S (0x01 << 2)
//
#define KXG03_BUF_CTL4_BUF_AUX2_W (0x01 << 1)
//
#define KXG03_BUF_CTL4_BUF_AUX1_W (0x01 << 0)
//
#define KXG03_BUF_EN_BUFE (0x01 << 7)
//
#define KXG03_BUF_EN_BUF_SYM_SYMBOL_MODE_DISABLED (0x0 << 2)
//
#define KXG03_BUF_EN_BUF_SYM_SINGLE_SYMBOL_MODE_ENABLED (0x1 << 2)
//
#define KXG03_BUF_EN_BUF_SYM_DUAL_SYMBOL_TRANS_MODE_ENABLED (0x2 << 2)
//
#define KXG03_BUF_EN_BUF_SYM_DUAL_SYMBOL_FRAME_MODE_ENABLED (0x3 << 2)
//
#define KXG03_BUF_EN_BUF_M_FIFO (0x0 << 0)
//
#define KXG03_BUF_EN_BUF_M_STREAM (0x1 << 0)
//
#define KXG03_BUF_EN_BUF_M_TRIGGER (0x2 << 0)
//
#define KXG03_BUF_EN_BUF_M_FILO (0x3 << 0)

 /*registers bit masks */
//None
#define KXG03_AUX_STATUS_AUX2FAIL_MASK 0x80
//None
#define KXG03_AUX_STATUS_AUX2ERR_MASK 0x40
//
#define KXG03_AUX_STATUS_AUX2ST_MASK 0x30
//None
#define KXG03_AUX_STATUS_AUX1FAIL_MASK 0x8
//None
#define KXG03_AUX_STATUS_AUX1ERR_MASK 0x4
//
#define KXG03_AUX_STATUS_AUX1ST_MASK 0x3
//None
#define KXG03_STATUS1_INT1_MASK 0x80
//None
#define KXG03_STATUS1_POR_MASK 0x40
//None
#define KXG03_STATUS1_AUX2_ACT_MASK 0x20
//None
#define KXG03_STATUS1_AUX1_ACT_MASK 0x10
//None
#define KXG03_STATUS1_AUX_ERR_MASK 0x8
//None
#define KXG03_STATUS1_WAKE_SLEEP_MASK 0x4
//None
#define KXG03_STATUS1_GYRO_RUN_MASK 0x2
//None
#define KXG03_STATUS1_GYRO_START_MASK 0x1
//None
#define KXG03_INT1_SRC1_INT1_BFI_MASK 0x80
//None
#define KXG03_INT1_SRC1_INT1_WMI_MASK 0x40
//None
#define KXG03_INT1_SRC1_INT1_WUFS_MASK 0x20
//None
#define KXG03_INT1_SRC1_INT1_BTS_MASK 0x10
//None
#define KXG03_INT1_SRC1_INT1_DRDY_AUX2_MASK 0x8
//None
#define KXG03_INT1_SRC1_INT1_DRDY_AUX1_MASK 0x4
//None
#define KXG03_INT1_SRC1_INT1_DRDY_ACC_MASK 0x2
//None
#define KXG03_INT1_SRC1_INT1_DRDY_GYRO_MASK 0x1
//None
#define KXG03_INT1_SRC2_INT1_XNWU_MASK 0x20
//None
#define KXG03_INT1_SRC2_INT1_XPWU_MASK 0x10
//None
#define KXG03_INT1_SRC2_INT1_YNWU_MASK 0x8
//None
#define KXG03_INT1_SRC2_INT1_YPWU_MASK 0x4
//None
#define KXG03_INT1_SRC2_INT1_ZNWU_MASK 0x2
//None
#define KXG03_INT1_SRC2_INT1_ZPWU_MASK 0x1
//None
#define KXG03_STATUS2_INT2_MASK 0x80
//None
#define KXG03_STATUS2_POR_MASK 0x40
//None
#define KXG03_STATUS2_AUX2_ACT_MASK 0x20
//None
#define KXG03_STATUS2_AUX1_ACT_MASK 0x10
//None
#define KXG03_STATUS2_AUX_ERR_MASK 0x8
//None
#define KXG03_STATUS2_WAKESLEEP_MASK 0x4
//None
#define KXG03_STATUS2_GYRO_RUN_MASK 0x2
//None
#define KXG03_STATUS2_GYRO_START_MASK 0x1
//None
#define KXG03_INT2_SRC1_INT2_BFI_MASK 0x80
//None
#define KXG03_INT2_SRC1_INT2_WMI_MASK 0x40
//None
#define KXG03_INT2_SRC1_INT2_WUFS_MASK 0x20
//None
#define KXG03_INT2_SRC1_INT2_BTS_MASK 0x10
//None
#define KXG03_INT2_SRC1_INT2_DRDY_AUX2_MASK 0x8
//None
#define KXG03_INT2_SRC1_INT2_DRDY_AUX1_MASK 0x4
//None
#define KXG03_INT2_SRC1_INT2_DRDY_ACC_MASK 0x2
//None
#define KXG03_INT2_SRC1_INT2_DRDY_GYRO_MASK 0x1
//None
#define KXG03_INT2_SRC2_INT2_XNWU_MASK 0x20
//None
#define KXG03_INT2_SRC2_INT2_XPWU_MASK 0x10
//None
#define KXG03_INT2_SRC2_INT2_YNWU_MASK 0x8
//None
#define KXG03_INT2_SRC2_INT2_YPWU_MASK 0x4
//None
#define KXG03_INT2_SRC2_INT2_ZNWU_MASK 0x2
//None
#define KXG03_INT2_SRC2_INT2_ZPWU_MASK 0x1
//
#define KXG03_ACCEL_ODR_WAKE_LPMODE_W_MASK 0x80
//The max over sampling rate (or max number of samples averaged) varies with ODR
#define KXG03_ACCEL_ODR_WAKE_NAVG_W_MASK 0x70
//accelerometer ODR in wake mode
#define KXG03_ACCEL_ODR_WAKE_ODRA_W_MASK 0xf
//
#define KXG03_ACCEL_ODR_SLEEP_LPMODE_S_MASK 0x80
//
#define KXG03_ACCEL_ODR_SLEEP_NAVG_S_MASK 0x70
//
#define KXG03_ACCEL_ODR_SLEEP_ODRA_S_MASK 0xf
//Accelerometer sleep mode full scale range select.
#define KXG03_ACCEL_CTL_ACC_FS_S_MASK 0xc0
//Accelerometer wake mode full scale range select.
#define KXG03_ACCEL_CTL_ACC_FS_W_MASK 0xc
//Gyroscope angular velocity range wake mode
#define KXG03_GYRO_ODR_WAKE_GYRO_FS_W_MASK 0xc0
//Gyroscope bandwidth selection in wake mode.
#define KXG03_GYRO_ODR_WAKE_GYRO_BW_W_MASK 0x30
//gyroscope ODR in wake mode
#define KXG03_GYRO_ODR_WAKE_ODRG_W_MASK 0xf
//Gyroscope angular velocity range sleep mode
#define KXG03_GYRO_ODR_SLEEP_GYRO_FS_S_MASK 0xc0
//Gyroscope bandwidth selection in sleep mode.
#define KXG03_GYRO_ODR_SLEEP_GYRO_BW_S_MASK 0x30
//gyroscope ODR in sleep mode
#define KXG03_GYRO_ODR_SLEEP_ODRG_S_MASK 0xf
//
#define KXG03_STDBY_AUX2_STDBY_S_MASK 0x80
//
#define KXG03_STDBY_AUX1_STDBY_S_MASK 0x40
//
#define KXG03_STDBY_GYRO_STDBY_S_MASK 0x20
//
#define KXG03_STDBY_AUX2_STDBY_W_MASK 0x8
//
#define KXG03_STDBY_AUX1_STDBY_W_MASK 0x4
//
#define KXG03_STDBY_GYRO_STDBY_W_MASK 0x2
//
#define KXG03_STDBY_ACC_STDBY_MASK 0x1
//None
#define KXG03_CTL_REG_1_RST_MASK 0x80
//
#define KXG03_CTL_REG_1_TEMP_STDBY_S_MASK 0x10
//
#define KXG03_CTL_REG_1_TEMP_STDBY_W_MASK 0x8
//
#define KXG03_CTL_REG_1_ACC_STPOL_MASK 0x2
//None
#define KXG03_CTL_REG_1_ACC_ST_MASK 0x1
//None
#define KXG03_INT_PIN_CTL_IEN2_MASK 0x80
//Interrupt polarity select for INT2 pin.
#define KXG03_INT_PIN_CTL_IEA2_MASK 0x40
//Interrupt latch mode select for INT2 pin
#define KXG03_INT_PIN_CTL_IEL2_MASK 0x30
//None
#define KXG03_INT_PIN_CTL_IEN1_MASK 0x8
//None
#define KXG03_INT_PIN_CTL_IEA1_MASK 0x4
//Interrupt latch mode select for INT1 pin
#define KXG03_INT_PIN_CTL_IEL1_MASK 0x3
//None
#define KXG03_INT_PIN1_SEL_BFI_P1_MASK 0x80
//None
#define KXG03_INT_PIN1_SEL_WMI_P1_MASK 0x40
//None
#define KXG03_INT_PIN1_SEL_WUF_P1_MASK 0x20
//None
#define KXG03_INT_PIN1_SEL_BTS_P1_MASK 0x10
//None
#define KXG03_INT_PIN1_SEL_DRDY_AUX2_P1_MASK 0x8
//None
#define KXG03_INT_PIN1_SEL_DRDY_AUX1_P1_MASK 0x4
//None
#define KXG03_INT_PIN1_SEL_DRDY_ACC_P1_MASK 0x2
//None
#define KXG03_INT_PIN1_SEL_DRDY_GYRO_P1_MASK 0x1
//None
#define KXG03_INT_PIN2_SEL_BFI_P2_MASK 0x80
//None
#define KXG03_INT_PIN2_SEL_WMI_P2_MASK 0x40
//None
#define KXG03_INT_PIN2_SEL_WUF_P2_MASK 0x20
//None
#define KXG03_INT_PIN2_SEL_BTS_P2_MASK 0x10
//None
#define KXG03_INT_PIN2_SEL_DRDY_AUX2_P2_MASK 0x8
//None
#define KXG03_INT_PIN2_SEL_DRDY_AUX1_P2_MASK 0x4
//None
#define KXG03_INT_PIN2_SEL_DRDY_ACC_P2_MASK 0x2
//None
#define KXG03_INT_PIN2_SEL_DRDY_GYRO_P2_MASK 0x1
//None
#define KXG03_INT_MASK1_BFIE_MASK 0x80
//None
#define KXG03_INT_MASK1_WMIE_MASK 0x40
//None
#define KXG03_INT_MASK1_WUFE_MASK 0x20
//None
#define KXG03_INT_MASK1_BTSE_MASK 0x10
//None
#define KXG03_INT_MASK1_DRDY_AUX2_MASK 0x8
//None
#define KXG03_INT_MASK1_DRDY_AUX1_MASK 0x4
//None
#define KXG03_INT_MASK1_DRDY_ACC_MASK 0x2
//None
#define KXG03_INT_MASK1_DRDY_GYRO_MASK 0x1
//None
#define KXG03_INT_MASK2_XNWUE_MASK 0x20
//None
#define KXG03_INT_MASK2_XPWUE_MASK 0x10
//None
#define KXG03_INT_MASK2_YNWUE_MASK 0x8
//None
#define KXG03_INT_MASK2_YPWUE_MASK 0x4
//None
#define KXG03_INT_MASK2_ZNWUE_MASK 0x2
//None
#define KXG03_INT_MASK2_ZPWUE_MASK 0x1
//
#define KXG03_FSYNC_CTL_FSYNC_MODE_MASK 0x30
//
#define KXG03_FSYNC_CTL_FSYNC_SEL_MASK 0x7
//None
#define KXG03_WAKE_SLEEP_CTL1_BTS_EN_MASK 0x80
//None
#define KXG03_WAKE_SLEEP_CTL1_WUF_EN_MASK 0x40
//None
#define KXG03_WAKE_SLEEP_CTL1_MAN_SLEEP_MASK 0x20
//None
#define KXG03_WAKE_SLEEP_CTL1_MAN_WAKE_MASK 0x10
//the Output Data Rate for the wake up (motion detection).
#define KXG03_WAKE_SLEEP_CTL1_OWUF_MASK 0x7
//debounce counter clear mode.
#define KXG03_WAKE_SLEEP_CTL2_C_MODE_MASK 0x1
//None
#define KXG03_BUF_CTL2_BUF_TEMP_W_MASK 0x40
//None
#define KXG03_BUF_CTL2_BUF_ACC_W_X_MASK 0x20
//None
#define KXG03_BUF_CTL2_BUF_ACC_W_Y_MASK 0x10
//None
#define KXG03_BUF_CTL2_BUF_ACC_W_Z_MASK 0x8
//None
#define KXG03_BUF_CTL2_BUF_GYR_W_X_MASK 0x4
//None
#define KXG03_BUF_CTL2_BUF_GYR_W_Y_MASK 0x2
//None
#define KXG03_BUF_CTL2_BUF_GYR_W_Z_MASK 0x1
//None
#define KXG03_BUF_CTL3_BUF_TEMP_W_MASK 0x40
//None
#define KXG03_BUF_CTL3_BUF_ACC_W_X_MASK 0x20
//None
#define KXG03_BUF_CTL3_BUF_ACC_W_Y_MASK 0x10
//None
#define KXG03_BUF_CTL3_BUF_ACC_W_Z_MASK 0x8
//None
#define KXG03_BUF_CTL3_BUF_GYR_W_X_MASK 0x4
//None
#define KXG03_BUF_CTL3_BUF_GYR_W_Y_MASK 0x2
//None
#define KXG03_BUF_CTL3_BUF_GYR_W_Z_MASK 0x1
//None
#define KXG03_BUF_CTL4_BUF_AUX2_S_MASK 0x8
//None
#define KXG03_BUF_CTL4_BUF_AUX1_S_MASK 0x4
//None
#define KXG03_BUF_CTL4_BUF_AUX2_W_MASK 0x2
//None
#define KXG03_BUF_CTL4_BUF_AUX1_W_MASK 0x1
//None
#define KXG03_BUF_EN_BUFE_MASK 0x80
//
#define KXG03_BUF_EN_BUF_SYM_MASK 0xc
//
#define KXG03_BUF_EN_BUF_M_MASK 0x3


#endif
