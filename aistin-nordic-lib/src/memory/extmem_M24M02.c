/**
  @file extmem_M24M02.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdio.h>

#include "hw_select.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "simple_uart.h"
#include "extmem.h"
#include "twiface.h"
#include "debug.h"


#ifdef MEM_M24M02


static
TwiDevice M24M02,
	*This = &M24M02;
#define This_ M24M02


static inline
uint32_t limitSize(uint32_t addr, uint32_t size)
{
	if (addr >= EXTMEM_SIZE) return 0;
	if (addr + size > EXTMEM_SIZE) size = EXTMEM_SIZE - addr;
	return size;
}


/**@brief Wait until the memory chip is ready with the previous operation.
 */
void Extmem_waitMemReady()
{
}


/**@brief Read status of the memory chip.
 */
uint16_t Extmem_readStatus()
{
	return 0;
}


/**@brief Read memory chip's ID code.
 */
void Extmem_readDeviceID(uint8_t *id, int idSize)
{
}


uint32_t Extmem_erase(uint32_t memAddress, uint32_t size)
{
	// TODO
	return size;
}


/**@brief Erase memory chip.
 */
void Extmem_chipErase()
{
		Extmem_waitMemReady();
}


/**@brief Put the chip into deep sleep mode.
 */
void Extmem_deepSleep(void)
{
}



/**@brief Put the chip into ultra deep sleep mode.
 */
void Extmem_ultraDeepSleep(void)
{
}


/**@brief Read free amount of bytes which may be laid out over several memory pages.
 */
uint32_t Extmem_read(uint32_t memAddress, uint8_t* data, uint32_t size)
{
		Extmem_waitMemReady();
		This_.devAddr = M24M02_DEVADDR | (memAddress>>16);
    Twiface_writeByteReg(This, (memAddress>>8) & 0xff, memAddress & 0xff, false);
		//if (size > 255) size = 255; // FIXME - FIXME - FIXME!
		Twiface_readData(This, data, size, false);
	// FIXME: should there be NACK after last byte or something?
		return size;
}


/**@brief Write a piece of data that fits into a single memory page.
 */
uint16_t Extmem_writeToOnePage(uint32_t memAddress, uint8_t* data, uint16_t size)
{
	// write at most 256 bytes, but less if memAddress is at the middle of a mem page
	// note: the area to be written must be clear
	// char str[80];
	uint16_t room = 256 - (memAddress & 0xff);
	if (size > room) size = room; // this many bytes still fits into that page
	//if (size > 4) size = 4; // FIXME - FIXME - FIXME!
	//sprintf(str, "memaddr=%d size=%d", memAddress, size);
	//debugln(str);
	This_.devAddr = M24M02_DEVADDR | (memAddress>>16);
	//debugln("address");
	Twiface_writeByteReg(This, (memAddress>>8) & 0xff, memAddress & 0xff, false);
	// There must not be STOP between address and data writing!
	//if (size > 255) size = 255; // FIXME - FIXME - FIXME!
	//uint8_t addr[2] = {0, 0};
	//Twidevice_writeData(MEM_TWIUNIT, addr, sizeof addr);
	//nrf_delay_ms(10); // TEST
	//debugln("data");
	//data[0] = 0; data[1] = 0; // TEST
	Twiface_writeData(This, data, size, false);
	nrf_delay_ms(10); // TEST
	return size;
}


/**@brief Write free amount of bytes which may need several memory pages.
 */
uint32_t Extmem_write(uint32_t memAddress, uint8_t* data, uint32_t size)
{
		// write freely, no need to take care of memory pages
		// note: the area to be written must be clear
  	//debugln(__func__);
		size = limitSize(memAddress, size);
		uint32_t left = size;
		while (left > 0) {
			uint16_t wrote = Extmem_writeToOnePage(memAddress, data, left);
			memAddress += wrote;
			data += wrote;
			left -= wrote;
		}
		return size;
}


/**@brief Test function for the memory chip. Fills in the memory and
 * then scans it to compare if the values were preserved. Returns true
 * if so, otherwise false (memory error).
 */
bool Extmem_test(uint32_t startAddress, uint32_t size)
{
	bool ok = true;
	size = limitSize(startAddress, size);
	uint32_t addr = startAddress;
	uint32_t n = 0;
	uint8_t data[256];
	for (int i = 0; i < sizeof data; i++) data[i] = i;
	debug("filling in memory...");
	while (n < size) {
		if ((n & 0x1fff) == 0) {
			char str[40];
			sprintf(str, "\r\naddr %6d..", n);
			debug(str);
		}
		addr += Extmem_write(addr, data, sizeof data);
		n += sizeof data;
		//break; // TESTING ONE PAGE ONLY
	}
	debug("\r\nreading out and comparing memory...");
	n = 0;
	addr = startAddress;
	while (n < size) {
		char str[40];
		if ((n & 0x1fff) == 0) {
			sprintf(str, "\r\naddr %6d..", n);
			debug(str);
		}
		for (int i = 0; i < sizeof data; i++) data[i] = 0xbe;
		Extmem_read(addr, data, sizeof data);
		for (int i = 0; i < sizeof data; i++) {
			//sprintf(str, "%02x", (int)data[i]);
			//debug(str);
			if (data[i] != i) {
				char str[40];
				sprintf(str, " %6d! ", addr + i);
				debug(str);
				ok = false;
			}
		}
		addr += sizeof data;
		n += sizeof data;
		//break; // TESTING ONE PAGE ONLY
	}
	debug("\r\ndone!\r\n");
	return ok;
}


/**@brief Frees bus. Note that the SPI module shares resources with TWI module.
 */
void Extmem_freeBus(void)
{
	// ensure there's no operations in progress
	Extmem_waitMemReady();
	//spi_master_releaseChipSelect(spi_base_address); // to be sure
}


/**@brief Initializes Extmem-c-module (this).
 */
bool Extmem_init(void)
{
	Twiface_initDevice(
		This,	M24M02_NAME, M24M02_TWIUNIT, M24M02_TWIFREQ,
		M24M02_DEVADDR, M24M02_SDA, M24M02_SCL);
	return true;
}


#endif


#if 0
// Arduino example code

//Test extEEPROM library.
//Writes the EEPROM full of 32-bit integers and reads them back to verify.
//Wire a button from digital pin 6 to ground, this is used as a start button
//so the sketch doesn't do unnecessary EEPROM writes every time it's reset.
//Jack Christensen 09Jul2014

#include <extEEPROM.h>    //http://github.com/JChristensen/extEEPROM/tree/dev
#include <Streaming.h>    //http://arduiniana.org/libraries/streaming/
#include <Wire.h>         //http://arduino.cc/en/Reference/Wire

//Two 24LC256 EEPROMs on the bus
const uint32_t totalKBytes = 64;         //for read and write test functions
extEEPROM eep(kbits_2048, 1, 256);         //device size, number of devices, page size

//const uint8_t btnStart = 6;              //start button

void setup(void)
{
    //pinMode(btnStart, INPUT_PULLUP);
    Serial.begin(115200);
    while(!Serial);
    uint8_t eepStatus = eep.begin(twiClock100kHz);      //go fast!
    if (eepStatus) {
      Serial.println("extEEPROM.begin() failed, status = " + String(eepStatus));
        //Serial << endl << F("extEEPROM.begin() failed, status = ") << eepStatus << endl;
        while (true);
    }
}

void loop(void)
{
    Serial.println("Press button to start...");
    while (!Serial.available());
    while(Serial.available()) Serial.read();
//    while (digitalRead(btnStart) == HIGH) delay(10);    //wait for button push

    uint8_t chunkSize = 64;    //this can be changed, but must be a multiple of 4 since we're writing 32-bit integers
//    eeErase(chunkSize, 0, totalKBytes * 1024 - 1);
    eeWrite(chunkSize);
    eeRead(chunkSize);

    dump(0, 16);            //the first 16 bytes
    dump(32752, 32);        //across the device boundary
    dump(65520, 16);        //the last 16 bytes
}

//write test data (32-bit integers) to eeprom, "chunk" bytes at a time
void eeWrite(uint8_t chunk)
{
    chunk &= 0xFC;                //force chunk to be a multiple of 4
    uint8_t data[chunk];
    uint32_t val = 0;
    Serial.println("Writing...");
    uint32_t msStart = millis();
    
    for (uint32_t addr = 0; addr < totalKBytes * 1024; addr += chunk) {
        if ( (addr &0xFFF) == 0 ) Serial.println(addr, HEX);
        for (uint8_t c = 0; c < chunk; c += 4) {
            data[c+0] = val >> 24;
            data[c+1] = val >> 16;
            data[c+2] = val >> 8;
            data[c+3] = val;
            ++val;
        }
        eep.write(addr, data, chunk);
    }
    uint32_t msLapse = millis() - msStart;
    Serial.println("Write lapse: "+ String(msLapse)+ " ms");
}

//read test data (32-bit integers) from eeprom, "chunk" bytes at a time
void eeRead(uint8_t chunk)
{
    chunk &= 0xFC;                //force chunk to be a multiple of 4
    uint8_t data[chunk];
    uint32_t val = 0, testVal;
    Serial.println("Reading...");
    uint32_t msStart = millis();
    
    for (uint32_t addr = 0; addr < totalKBytes * 1024; addr += chunk) {
        if ( (addr &0xFFF) == 0 ) Serial.println(addr, HEX);
        eep.read(addr, data, chunk);
        for (uint8_t c = 0; c < chunk; c += 4) {
            testVal =  ((uint32_t)data[c+0] << 24) + ((uint32_t)data[c+1] << 16) + ((uint32_t)data[c+2] << 8) + (uint32_t)data[c+3];
            if (testVal != val) {Serial.println("Error @ addr " + String(addr+c, HEX) + " Expected " + String(val) + " Read " + String(testVal) + " 0x" + String(testVal, HEX)); }
            ++val;
        }
    }
    uint32_t msLapse = millis() - msStart;
    Serial.println("Last value: "+ String(--val, HEX) +" Read lapse: " + String(msLapse) + " ms");
}

//write 0xFF to eeprom, "chunk" bytes at a time
void eeErase(uint8_t chunk, uint32_t startAddr, uint32_t endAddr)
{
    chunk &= 0xFC;                //force chunk to be a multiple of 4
    uint8_t data[chunk];
    Serial.println("Erasing...");
    for (int i = 0; i < chunk; i++) data[i] = 0xFF;
    uint32_t msStart = millis();
    
    for (uint32_t a = startAddr; a <= endAddr; a += chunk) {
        if ( (a &0xFFF) == 0 ) Serial.println(a, HEX);
        eep.write(a, data, chunk);
    }
    uint32_t msLapse = millis() - msStart;
    Serial.println("Erase lapse: "+ String(msLapse) +" ms");
}

//dump eeprom contents, 16 bytes at a time.
//always dumps a multiple of 16 bytes.
void dump(uint32_t startAddr, uint32_t nBytes)
{
    Serial.println();
    Serial.println("EEPROM DUMP 0x" + String(startAddr, HEX) +" 0x"+ String(nBytes, HEX) +' '+ String(startAddr, HEX) +' '+ String(nBytes));
    uint32_t nRows = (nBytes + 15) >> 4;

    uint8_t d[16];
    for (uint32_t r = 0; r < nRows; r++) {
        uint32_t a = startAddr + 16 * r;
        eep.read(a, d, 16);
        Serial.print("0x");
        if ( a < 16 * 16 * 16 ) Serial.print('0');
        if ( a < 16 * 16 ) Serial.print('0');
        if ( a < 16 ) Serial.print('0');
        Serial.print(String(a,HEX)+' ');
        for ( int c = 0; c < 16; c++ ) {
            if ( d[c] < 16 ) Serial.print('0');
            Serial.print(String(d[c],HEX));// << ( c == 7 ? "  " : " " );
        }
        Serial.println();
    }
}
#endif


#if 0
/*-----------------------------------------------------------------------------*
 * extEEPROM.cpp - Arduino library to support external I2C EEPROMs.            *
 *                                                                             *
 * This library will work with most I2C serial EEPROM chips between 2k bits    *
 * and 2048k bits (2M bits) in size. Multiple EEPROMs on the bus are supported *
 * as a single address space. I/O across block, page and device boundaries     *
 * is supported. Certain assumptions are made regarding the EEPROM             *
 * device addressing. These assumptions should be true for most EEPROMs        *
 * but there are exceptions, so read the datasheet and know your hardware.     *
 *                                                                             *
 * The library should also work for EEPROMs smaller than 2k bits, assuming     *
 * that there is only one EEPROM on the bus and also that the user is careful  *
 * to not exceed the maximum address for the EEPROM.                           *
 *                                                                             *
 * Library tested with:                                                        *
 *   Microchip 24AA02E48 (2k bit)                                              *
 *   24xx32 (32k bit, thanks to Richard M)                                     *
 *   Microchip 24LC256 (256k bit)                                              *
 *   Microchip 24FC1026 (1M bit, thanks to Gabriele B on the Arduino forum)    *
 *   ST Micro M24M02 (2M bit)                                                  *
 *                                                                             *
 * Library will NOT work with Microchip 24xx1025 as its control byte does not  *
 * conform to the following assumptions.                                       *
 *                                                                             *
 * Device addressing assumptions:                                              *
 * 1. The I2C address sequence consists of a control byte followed by one      *
 *    address byte (for EEPROMs <= 16k bits) or two address bytes (for         *
 *    EEPROMs > 16k bits).                                                     *
 * 2. The three least-significant bits in the control byte (excluding the R/W  *
 *    bit) comprise the three most-significant bits for the entire address     *
 *    space, i.e. all chips on the bus. As such, these may be chip-select      *
 *    bits or block-select bits (for individual chips that have an internal    *
 *    block organization), or a combination of both (in which case the         *
 *    block-select bits must be of lesser significance than the chip-select    *
 *    bits).                                                                   *
 * 3. Regardless of the number of bits needed to address the entire address    *
 *    space, the three most-significant bits always go in the control byte.    *
 *    Depending on EEPROM device size, this may result in one or more of the   *
 *    most significant bits in the I2C address bytes being unused (or "don't   *
 *    care").                                                                  *
 * 4. An EEPROM contains an integral number of pages.                          *
 *                                                                             *
 * To use the extEEPROM library, the Arduino Wire library must also            *
 * be included.                                                                *
 *                                                                             *
 * Jack Christensen 23Mar2013 v1                                               *
 * 29Mar2013 v2 - Updated to span page boundaries (and therefore also          *
 * device boundaries, assuming an integral number of pages per device)         *
 * 08Jul2014 v3 - Generalized for 2kb - 2Mb EEPROMs.                           *
 *                                                                             *
 * External EEPROM Library by Jack Christensen is licensed under CC BY-SA 4.0, *
 * http://creativecommons.org/licenses/by-sa/4.0/                              *
 *-----------------------------------------------------------------------------*/

#include <extEEPROM.h>
#include <Wire.h>

// Constructor.
// - deviceCapacity is the capacity of a single EEPROM device in
//   kilobits (kb) and should be one of the values defined in the
//   eeprom_size_t enumeration in the extEEPROM.h file. (Most
//   EEPROM manufacturers use kbits in their part numbers.)
// - nDevice is the number of EEPROM devices on the I2C bus (all must
//   be identical).
// - pageSize is the EEPROM's page size in bytes.
// - eepromAddr is the EEPROM's I2C address and defaults to 0x50 which is common.
extEEPROM::extEEPROM(eeprom_size_t deviceCapacity, byte nDevice, unsigned int pageSize, uint8_t eepromAddr)
{
    _dvcCapacity = deviceCapacity;
    _nDevice = nDevice;
    _pageSize = pageSize;
    _eepromAddr = eepromAddr;
    _totalCapacity = _nDevice * _dvcCapacity * 1024UL / 8;
    _nAddrBytes = deviceCapacity > kbits_16 ? 2 : 1;       //two address bytes needed for eeproms > 16kbits

    //determine the bitshift needed to isolate the chip select bits from the address to put into the control byte
    uint16_t kb = _dvcCapacity;
    if ( kb <= kbits_16 ) _csShift = 8;
    else if ( kb >= kbits_512 ) _csShift = 16;
    else {
        kb >>= 6;
        _csShift = 12;
        while ( kb >= 1 ) {
            ++_csShift;
            kb >>= 1;
        }
    }
}

//initialize the I2C bus and do a dummy write (no data sent)
//to the device so that the caller can determine whether it is responding.
//when using a 400kHz bus speed and there are multiple I2C devices on the
//bus (other than EEPROM), call extEEPROM::begin() after any initialization
//calls for the other devices to ensure the intended I2C clock speed is set.
byte extEEPROM::begin(twiClockFreq_t twiFreq)
{
    Wire.begin();
    TWBR = ( (F_CPU / twiFreq) - 16) / 2;
    Wire.beginTransmission(_eepromAddr);
    if (_nAddrBytes == 2) Wire.write(0);      //high addr byte
    Wire.write(0);                            //low addr byte
    return Wire.endTransmission();
}

//Write bytes to external EEPROM.
//If the I/O would extend past the top of the EEPROM address space,
//a status of EEPROM_ADDR_ERR is returned. For I2C errors, the status
//from the Arduino Wire library is passed back through to the caller.
byte extEEPROM::write(unsigned long addr, byte *values, unsigned int nBytes)
{
    uint8_t ctrlByte;       //control byte (I2C device address & chip/block select bits)
    uint8_t txStatus = 0;   //transmit status
    uint16_t nWrite;        //number of bytes to write
    uint16_t nPage;         //number of bytes remaining on current page, starting at addr

    if (addr + nBytes > _totalCapacity) {   //will this write go past the top of the EEPROM?
        return EEPROM_ADDR_ERR;             //yes, tell the caller
    }

    while (nBytes > 0) {
        nPage = _pageSize - ( addr & (_pageSize - 1) );
        //find min(nBytes, nPage, BUFFER_LENGTH) -- BUFFER_LENGTH is defined in the Wire library.
        nWrite = nBytes < nPage ? nBytes : nPage;
        nWrite = BUFFER_LENGTH - _nAddrBytes < nWrite ? BUFFER_LENGTH - _nAddrBytes : nWrite;
        ctrlByte = _eepromAddr | (byte) (addr >> _csShift);
        Wire.beginTransmission(ctrlByte);
        if (_nAddrBytes == 2) Wire.write( (byte) (addr >> 8) );   //high addr byte
        Wire.write( (byte) addr );                                //low addr byte
        Wire.write(values, nWrite);
        txStatus = Wire.endTransmission();
        if (txStatus != 0) return txStatus;

        //wait up to 50ms for the write to complete
        for (uint8_t i=100; i; --i) {
            delayMicroseconds(500);                     //no point in waiting too fast
            Wire.beginTransmission(ctrlByte);
            if (_nAddrBytes == 2) Wire.write(0);        //high addr byte
            Wire.write(0);                              //low addr byte
            txStatus = Wire.endTransmission();
            if (txStatus == 0) break;
        }
        if (txStatus != 0) return txStatus;

        addr += nWrite;         //increment the EEPROM address
        values += nWrite;       //increment the input data pointer
        nBytes -= nWrite;       //decrement the number of bytes left to write
    }
    return txStatus;
}

//Read bytes from external EEPROM.
//If the I/O would extend past the top of the EEPROM address space,
//a status of EEPROM_ADDR_ERR is returned. For I2C errors, the status
//from the Arduino Wire library is passed back through to the caller.
byte extEEPROM::read(unsigned long addr, byte *values, unsigned int nBytes)
{
    byte ctrlByte;
    byte rxStatus;
    uint16_t nRead;             //number of bytes to read
    uint16_t nPage;             //number of bytes remaining on current page, starting at addr

    if (addr + nBytes > _totalCapacity) {   //will this read take us past the top of the EEPROM?
        return EEPROM_ADDR_ERR;             //yes, tell the caller
    }

    while (nBytes > 0) {
        nPage = _pageSize - ( addr & (_pageSize - 1) );
        nRead = nBytes < nPage ? nBytes : nPage;
        nRead = BUFFER_LENGTH < nRead ? BUFFER_LENGTH : nRead;
        ctrlByte = _eepromAddr | (byte) (addr >> _csShift);
        Wire.beginTransmission(ctrlByte);
        if (_nAddrBytes == 2) Wire.write( (byte) (addr >> 8) );   //high addr byte
        Wire.write( (byte) addr );                                //low addr byte
        rxStatus = Wire.endTransmission();
        if (rxStatus != 0) return rxStatus;        //read error

        Wire.requestFrom(ctrlByte, nRead);
        for (byte i=0; i<nRead; i++) values[i] = Wire.read();

        addr += nRead;          //increment the EEPROM address
        values += nRead;        //increment the input data pointer
        nBytes -= nRead;        //decrement the number of bytes left to write
    }
    return 0;
}

//Write a single byte to external EEPROM.
//If the I/O would extend past the top of the EEPROM address space,
//a status of EEPROM_ADDR_ERR is returned. For I2C errors, the status
//from the Arduino Wire library is passed back through to the caller.
byte extEEPROM::write(unsigned long addr, byte value)
{
    return write(addr, &value, 1);
}

//Read a single byte from external EEPROM.
//If the I/O would extend past the top of the EEPROM address space,
//a status of EEPROM_ADDR_ERR is returned. For I2C errors, the status
//from the Arduino Wire library is passed back through to the caller.
//To distinguish error values from valid data, error values are returned as negative numbers.
int extEEPROM::read(unsigned long addr)
{
    uint8_t data;
    int ret;
    
    ret = read(addr, &data, 1);
    return ret == 0 ? data : -ret;
}

#endif


#if 0
/*-----------------------------------------------------------------------------*
 * extEEPROM.h - Arduino library to support external I2C EEPROMs.              *
 *                                                                             *
 * This library will work with most I2C serial EEPROM chips between 2k bits    *
 * and 2048k bits (2M bits) in size. Multiple EEPROMs on the bus are supported *
 * as a single address space. I/O across block, page and device boundaries     *
 * is supported. Certain assumptions are made regarding the EEPROM             *
 * device addressing. These assumptions should be true for most EEPROMs        *
 * but there are exceptions, so read the datasheet and know your hardware.     *
 *                                                                             *
 * The library should also work for EEPROMs smaller than 2k bits, assuming     *
 * that there is only one EEPROM on the bus and also that the user is careful  *
 * to not exceed the maximum address for the EEPROM.                           *
 *                                                                             *
 * Library tested with:                                                        *
 *   Microchip 24AA02E48 (2k bit)                                              *
 *   24xx32 (32k bit, thanks to Richard M)                                     *
 *   Microchip 24LC256 (256k bit)                                              *
 *   Microchip 24FC1026 (1M bit, thanks to Gabriele B on the Arduino forum)    *
 *   ST Micro M24M02 (2M bit)                                                  *
 *                                                                             *
 * Library will NOT work with Microchip 24xx1025 as its control byte does not  *
 * conform to the following assumptions.                                       *
 *                                                                             *
 * Device addressing assumptions:                                              *
 * 1. The I2C address sequence consists of a control byte followed by one      *
 *    address byte (for EEPROMs <= 16k bits) or two address bytes (for         *
 *    EEPROMs > 16k bits).                                                     *
 * 2. The three least-significant bits in the control byte (excluding the R/W  *
 *    bit) comprise the three most-significant bits for the entire address     *
 *    space, i.e. all chips on the bus. As such, these may be chip-select      *
 *    bits or block-select bits (for individual chips that have an internal    *
 *    block organization), or a combination of both (in which case the         *
 *    block-select bits must be of lesser significance than the chip-select    *
 *    bits).                                                                   *
 * 3. Regardless of the number of bits needed to address the entire address    *
 *    space, the three most-significant bits always go in the control byte.    *
 *    Depending on EEPROM device size, this may result in one or more of the   *
 *    most significant bits in the I2C address bytes being unused (or "don't   *
 *    care").                                                                  *
 * 4. An EEPROM contains an integral number of pages.                          *
 *                                                                             *
 * To use the extEEPROM library, the Arduino Wire library must also            *
 * be included.                                                                *
 *                                                                             *
 * Jack Christensen 23Mar2013 v1                                               *
 * 29Mar2013 v2 - Updated to span page boundaries (and therefore also          *
 * device boundaries, assuming an integral number of pages per device)         *
 * 08Jul2014 v3 - Generalized for 2kb - 2Mb EEPROMs.                           *
 *                                                                             *
 * External EEPROM Library by Jack Christensen is licensed under CC BY-SA 4.0, *
 * http://creativecommons.org/licenses/by-sa/4.0/                              *
 *-----------------------------------------------------------------------------*/

#ifndef extEEPROM_h
#define extEEPROM_h

#include <Arduino.h>

//EEPROM size in kilobits. EEPROM part numbers are usually designated in k-bits.
enum eeprom_size_t {
    kbits_2 = 2,
    kbits_4 = 4,
    kbits_8 = 8,
    kbits_16 = 16,
    kbits_32 = 32,
    kbits_64 = 64,
    kbits_128 = 128,
    kbits_256 = 256,
    kbits_512 = 512,
    kbits_1024 = 1024,
    kbits_2048 = 2048
};

enum twiClockFreq_t { twiClock100kHz = 100000, twiClock400kHz = 400000 };

//EEPROM addressing error, returned by write() or read() if upper address bound is exceeded
const uint8_t EEPROM_ADDR_ERR = 9;

class extEEPROM
{
    public:
        extEEPROM(eeprom_size_t deviceCapacity, byte nDevice, unsigned int pageSize, byte eepromAddr = 0x50);
        byte begin(twiClockFreq_t twiFreq = twiClock100kHz);
        byte write(unsigned long addr, byte *values, unsigned int nBytes);
        byte write(unsigned long addr, byte value);
        byte read(unsigned long addr, byte *values, unsigned int nBytes);
        int read(unsigned long addr);

    private:
        uint8_t _eepromAddr;            //eeprom i2c address
        uint16_t _dvcCapacity;          //capacity of one EEPROM device, in kbits
        uint8_t _nDevice;               //number of devices on the bus
        uint16_t _pageSize;             //page size in bytes
        uint8_t _csShift;               //number of bits to shift address for chip select bits in control byte
        uint16_t _nAddrBytes;           //number of address bytes (1 or 2)
        unsigned long _totalCapacity;   //capacity of all EEPROM devices on the bus, in bytes
};

#endif

#endif
