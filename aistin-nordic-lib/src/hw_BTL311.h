#ifndef __HW_BTL311__
#define __HW_BTL311__


#include "hw_BTL3X1.h"


#define BLE_NAME "BTL311"


// Protocol selection
#define AISTIN_PROTOCOL_SKIIOT
//#define AISTIN_PROTOCOL_KIONIX


// External memory selection
//#define MEM_NONE
#define MEM_M24M02


// Sensor selections

// Accelerometer
//#define ACC_NONE					
#define ACC_KMX62				
#define ACC_KX122				

// Magnetometer
//#define MAG_NONE					
#define	MAG_KMX62				

// Gyroscope
//#define GYR_NONE					
#define GYR_KXG03				

// Humidity sensor
//#define HUM_NONE					
//#define HUM_HTS221			
#define HUM_SHT31				

// Barometer
//#define BAR_NONE				
#define BAR_LPS25				

// Infrared detector
#define INR_NONE


#endif
