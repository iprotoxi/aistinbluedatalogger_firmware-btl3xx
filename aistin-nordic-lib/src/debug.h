#ifndef DEBUG_H
#define DEBUG_H


#define debug(str) Debug_string((uint8_t*)(str))
#define debugln(str) Debug_stringln((uint8_t*)(str))
#define Debug DebugMode
#define debuglnf debugfln


#if 1


#define DEBUG

#include <stdio.h> // for sprintf
#include "simple_uart.h"

extern bool DebugMode;

void Debug_init(void);

void Debug_close(void);

bool Debug_check(void);

uint8_t Debug_getCh(void);

void Debug_char(uint8_t c);

void Debug_string(uint8_t *str);

void Debug_stringln(uint8_t *str);

void Debug_hex(uint8_t *data, int size);

int debugf(const char *format, ...);

int debugfln(const char *format, ...);
		

#else // no debug

		
#define Debug_init()

#define Debug_puts(x)
		
#define Debug_hex(data, size)

#define debugf(x, ...)

#define debugfln(x, ...)


#endif


#endif

