/**
  @file aistin.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdio.h>
#include "aistin.h"
#include "aistin_ble.h"
#include "hw_select.h"
#include "timing.h"
#include "main.h"
#include "debug.h"


#define SwapEndian16(x8) {uint8_t t=(x8)[0];(x8)[0]=(x8)[1];(x8)[1]=t;}
#define SwitchEndian16(dest, src) {(dest)[0] = (src)[1]; (dest)[1] = (src)[0];}

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define AistinEndian16(dest, src) SwitchEndian16(dest, src)
#else
#define AistinEndian16(dest, src) {(dest)[0] = (src)[0]; (dest)[1] = (src)[1];}
#endif


uint8_t Aistin_getDataID8(uint16_t dataID16)
{
	// 8-bit dataID16 supports the most important things
	if (dataID16 < AISTIN_ID_SENSORBASE) return dataID16 >> 8;
	return ((dataID16 & 0x07f0) >> 4) | 0x80;
	
}


uint16_t Aistin_getDataID16(uint8_t dataID8)
{
	return AISTIN_ID_SENSORBASE + (((uint16_t)(dataID8 & 0x7f)) << 4);
}


void Aistin_initItem(AistinItem *ai, uint16_t dataID16)
{
	ai->dataID16 = dataID16;
	ai->dataSize = 0;
}


int Aistin_writeData3x16(uint8_t *buffer, uint8_t *bigEndian16)
{
	// NOTE! ARM sometimes jammed when tried to access uint8_t buffer
  // as uint16_t because memory address might be odd, instead of even.
	SwitchEndian16(buffer, bigEndian16);
	SwitchEndian16(buffer + 2, bigEndian16 + 2);
	SwitchEndian16(buffer + 4, bigEndian16 + 4);
	return 6;
}


int Aistin_fixEndian(Xyz16 *xyz16)
{
	SwapEndian16(xyz16->x);
	SwapEndian16(xyz16->y);
	SwapEndian16(xyz16->z);
	return sizeof(Xyz16);
}


int Aistin_writeDataWithTime4x16(uint8_t *buffer, uint8_t *bigEndian16)
{
	// Values given by sensors
	SwitchEndian16(buffer, bigEndian16);
	SwitchEndian16(buffer + 2, bigEndian16 + 2);
	SwitchEndian16(buffer + 4, bigEndian16 + 4);
	// The last one is TIME field produced by MCU
	AistinEndian16(buffer + 6, bigEndian16 + 6);
	return 8;
}
	

int Aistin_writeData3x12(uint8_t *buffer, uint8_t *bigEndian16)
{
	// store coordinates so that they are easy to read when
	// converted to a hex string, i.e. "XXXYYYZZZ0"
	buffer[0] = bigEndian16[1];
	buffer[1] = (bigEndian16[0] & 0xf0) | ((bigEndian16[3] & 0xf0)>>4);
	buffer[2] = (bigEndian16[3]<<4) | (bigEndian16[2]>>4);
	buffer[3] = bigEndian16[5];
	buffer[4] = bigEndian16[4] & 0xf0; // lower half does not contain data
	return 5;
}
	

static
int formatMessage(char *str, uint8_t seqNr, int dataID16, uint8_t *data, int dataSize)
{
	int i = 0;
	#if 1
	i = sprintf(str, ">D~%s-%02x@%04x:", BleName, seqNr, dataID16);

	if (dataID16 == AISTIN_ID_B168_AIRPRESSURE)
		i += sprintf(&str[i], " %0.1f %d.%02d",
						data[0] * 4096.0 + data[1] * 16.0 + data[2] / 16.0,
						(int)data[2], (int)data[3] * 100 / 256);
	else
	if (dataID16 == AISTIN_ID_B188_AIRHUMIDITY)
		i += sprintf(&str[i], " %d.%02d %d.%02d",
						(int)data[0], (int)data[1] * 100 / 256,
						(int)data[2], (int)data[3] * 100 / 256);	
	else {
		int n;
		for (n = 0; n < dataSize; n++) i += sprintf(&str[i], "%02x", data[n]);
	}
	#endif
	return i;
}


int Aistin_asciiFormat(char *str, AistinItem *ai, uint8_t seqNr)
{
		return formatMessage(str, seqNr,
							ai->dataID16, ai->data.bytes, ai->dataSize);
}
