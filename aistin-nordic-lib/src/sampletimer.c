/**
  @file sampletimer.c
  @brief Part of Aistin library
	@copyright 2015-2016 iProtoXi Oy
*/

#include <string.h>
#include "debug.h"
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "hw_select.h"
#include "app_timer.h"
#include "nrfface.h"
#include "sampletimer.h"


#define APP_TIMER_PRESCALER        0 /**< Value of the RTC1 PRESCALER register. */


static app_timer_id_t m_timer_id;
bool Sampletimer_timeout;


static
	void timeoutHandler(void * p_context)
	{
		UNUSED_PARAMETER(p_context);
		Sampletimer_timeout = true;
	}

	
bool Sampletimer_getTimeout()	
{
	uint8_t nested;
	sd_nvic_critical_region_enter(&nested);
	bool timeout = Sampletimer_timeout;
	Sampletimer_timeout = false;
	sd_nvic_critical_region_exit(nested);
	return timeout;
}


void Sampletimer_init(void)
{
	uint32_t err_code;
	// here we assume APP_TIMER_INIT is already done
	err_code = app_timer_create(&m_timer_id,
															APP_TIMER_MODE_REPEATED,
															timeoutHandler);
	APP_ERROR_CHECK(err_code);
}


void Sampletimer_start(int interval_ms)
{
	uint32_t interval = APP_TIMER_TICKS(interval_ms, APP_TIMER_PRESCALER);
	nrfAssert( app_timer_start(m_timer_id, interval, NULL) );
	Sampletimer_timeout = false;
}


static
	void stopTimer()
	{
		uint32_t err_code;
		err_code = app_timer_stop(m_timer_id);
		APP_ERROR_CHECK(err_code);
	}


void Sampletimer_restart(int interval_ms)
{
	stopTimer();
	Sampletimer_start(interval_ms);
}
