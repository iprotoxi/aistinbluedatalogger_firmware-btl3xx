/**
  @file hw_select.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/
#ifndef __HW_SELECT__
#define __HW_SELECT__


#ifndef HW_SET_BY_PROJECT
// Select your hw version here, or in the project settings of your IDE.
//
//#define HW_BTL311
//#define HW_BTL321
#endif


#ifdef HW_BTL311
#include "hw_BTL311.h"
#endif

#ifdef HW_BTL312
#include "hw_BTL312.h"
#endif

#ifdef HW_BTL322
#include "hw_BTL322.h"
#endif

#ifdef HW_BTL321
#include "hw_BTL321.h"
#endif


#endif
