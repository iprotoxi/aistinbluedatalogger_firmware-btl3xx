#ifndef LEDS_H
#define LEDS_H

#include <inttypes.h>

void Leds_init(void);
void Leds_off(void);
void Leds_setColor(uint8_t r, uint8_t g, uint8_t b);
void Leds_indicateBooting(void);
void Leds_indicateRunning(void);
void Leds_indicateConnection(void);
void Leds_indicateNoConnection(void);

#endif
