/**
  @file gyroscope_KXG03.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "softdevice_handler.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "gyroscope.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "leds.h"
#include "aistin.h"
#include "twiface.h"
#include "timing.h"
#include "kxg03.h"


#ifdef GYR_KXG03


static
TwiDevice KXG03,
	*This = &KXG03;


#define INT_PIN		KXG03_INT_PIN


static
app_gpiote_user_id_t m_app_gpiote_my_id;
static
int8_t interruptSignal;


static
	void KXG03_clearInterrupt(void)
	{
		uint8_t v;
		Twiface_readReg(&KXG03, KXG03_INT1_L, &v, sizeof v);
	}


static
void gyroscopeInterrupt(uint32_t event_pins_low_to_high, uint32_t event_pins_high_to_low)
{
	/*if (event_pins_high_to_low & (1 << INT_PIN))*/ interruptSignal = 1;
}


static
void initInterrupt(void)
{
	uint32_t err_code;
	nrf_gpio_cfg_sense_input(
		INT_PIN, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);
	err_code = app_gpiote_user_register(
		&m_app_gpiote_my_id, 0, (1 << INT_PIN), gyroscopeInterrupt);
	APP_ERROR_CHECK(err_code);
	err_code = app_gpiote_user_enable(m_app_gpiote_my_id);
	APP_ERROR_CHECK(err_code);
}


static
	void KXG03_setGyroscopeDataReadyInterrupt(void)
	{
		uint8_t v;
		//Twiface_readReg(&KXG03, KXG03_INT1_SRC1, &v, sizeof v);
		//v |= KXG03_INT1_SRC1_INT1_DRDY_GYRO;
		//Twiface_writeReg(&KXG03, KXG03_INT1_SRC1, v);	
		Twiface_readReg(&KXG03, KXG03_INT_PIN1_SEL, &v, sizeof v);
		v |= KXG03_INT_PIN1_SEL_DRDY_GYRO_P1;
		Twiface_writeReg(&KXG03, KXG03_INT_PIN1_SEL, v);	
		//Twiface_readReg(&KXG03, KXG03_INT_PIN_CTL, &v, sizeof v);
		//v &= ~KXG03_INT_PIN_CTL_IEA1;
		//v |= KXG03_INT_PIN_CTL_IEN1;
		v = KXG03_INT_PIN_CTL_IEN1 |
				KXG03_INT_PIN_CTL_IEA1_ACTIVE_LOW |
				KXG03_INT_PIN_CTL_IEL1_PULSED_50US;
		Twiface_writeReg(&KXG03, KXG03_INT_PIN_CTL, v);
		Twiface_writeReg(&KXG03, KXG03_INT_MASK1, 
			KXG03_INT_MASK1_DRDY_GYRO | KXG03_INT_MASK1_DRDY_ACC); // enable int	
		KXG03_clearInterrupt();
		//Twiface_writeReg(This, KXG03_WAKE_SLEEP_CTL1, 
		//	KXG03_WAKE_SLEEP_CTL1_MAN_WAKE | KXG03_WAKE_SLEEP_CTL1_OWUF_100);
	}


/**@brief Return true if interrupt has been occurred since the previous call.
 */
bool Gyroscope_getInterruptSignal()
{
	
	uint8_t nested;
	sd_nvic_critical_region_enter(&nested);
	bool is = (bool)interruptSignal;
	if (is) interruptSignal = 0;
	sd_nvic_critical_region_exit(nested);
	if (is) KXG03_clearInterrupt();
	return is;
}


static
void setupChip()
{
	//Twiface_writeReg(This, KXG03_STDBY, KXG03_STDBY_GYRO_STDBY_W_ENABLED); 
	//writeReg(0x43, 0x03); 
	//Twiface_writeReg(This, 0x43, 0x00);
	for (int i = 0; i < 5; i++) {
		Twiface_writeReg(This, KXG03_STDBY, 0);//0xed); // enable gyro only 
		Twiface_writeReg(This, KXG03_ACCEL_ODR_WAKE, 0x00); // ACCEL_ODR_WAKE 
		Twiface_writeReg(This, KXG03_GYRO_ODR_WAKE, 0x37);
		Twiface_writeReg(This, KXG03_WAKE_SLEEP_CTL1, 0x17);
		uint8_t reg;
		int limit = 10000;
		while (limit--) {
			 Twiface_readReg(This, KXG03_STATUS1, &reg, sizeof reg);
			 if (reg & KXG03_STATUS1_GYRO_RUN) return;
		}
		debugln("error: gyro failed to start");
	}
}


/**@brief Function for reading 3D-values X, Y and Z. This is used
 * by/via TwiDevice.read* -functions.
 */
static
bool readXyz16(Xyz16 *xyz16)
{
	return
		Twiface_readReg(
			This, KXG03_GYRO_XOUT_L, (uint8_t*)xyz16, sizeof(Xyz16));
}


int Gyroscope_readXYZ3x16(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 6 || !This->ready) return 0;
	Xyz16 xyz;
	readXyz16(&xyz);
	return Aistin_writeData3x16(buffer, &xyz.x[0]);
}


int Gyroscope_readXYZ3x16andTime8(Xyz16time8 *xyz16time8)
{
	#if 1
	return Twidevice_readXyz16time8(This, xyz16time8);
	#else
	if (!This->ready) return 0;
	readXyz16(&timed8Xyz16->xyz16);
	Aistin_fixEndian(&timed8Xyz16->xyz16);
	timed8Xyz16->rtcTime8 = Timing_read1000Hz8();
	return sizeof(Timed8Xyz16);
	#endif
}


int Gyroscope_readXYZ3x12(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 5 || !This->ready) return 0;
	Xyz16 xyz;
	readXyz16(&xyz);
	return Aistin_writeData3x12(buffer, &xyz.x[0]);
}


/**@brief Initializes Gyroscope-c-module (this).
 */
bool Gyroscope_init()
{
	Twiface_initDevice(
		This, KXG03_NAME, KXG03_TWIUNIT, KXG03_TWIFREQ,
		KXG03_DEVADDR, KXG03_SDA, KXG03_SCL);
	This->readXyz16 = readXyz16;
	setupChip();
	#ifdef SAMPLE_BY_SENSOR_INTERRUPT
	initInterrupt();
	KXG03_setGyroscopeDataReadyInterrupt();
	#endif
	return true;
}


#endif
