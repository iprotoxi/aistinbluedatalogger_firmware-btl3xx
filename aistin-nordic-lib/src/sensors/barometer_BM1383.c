/**
  @file barometer_BM1383.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "barometer.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "twiface.h"


#ifdef BAR_BM1383


static
TwiDevice BM1383;
static
TwiDevice *This = &BM1383;


#define BM1383_REG_PRESSURE			  0x1c
#define BM1383_REG_TEMPERATURE		0x1a
#define BM1383_POWER_DOWN					0x12
#define BM1383_SLEEP							0x13
#define BM1383_MODE_CONTROL				0x14


static
	void startMeasurement()
	{
		// start next measurement, max average takes 147 ms
		Twiface_writeReg(This, BM1383_MODE_CONTROL, 0xc1);
	}
	
	
static
void setupChip()
{
  Twiface_writeReg(This, BM1383_POWER_DOWN, 0x01);
  Twiface_writeReg(This, BM1383_SLEEP, 0x01);
	startMeasurement();
}


/**@brief Initializes Barometer-c-module (this).
 */
bool Barometer_init()
{
	Twiface_initDevice(
		This, BM1383_NAME, BM1383_TWIUNIT, BM1383_TWIFREQ,
		BM1383_DEVADDR, BM1383_SDA, BM1383_SCL);
	setupChip();
	return true;
}


static
void printValues(uint8_t *pres, int16_t temp)
{
	if (DebugMode) {
		float tempf = temp / 32.0;
		uint32_t bars = ((uint32_t)pres[0]<<16) | ((uint32_t)pres[1]<<8) | (pres[2]<<2);
		float barsf = bars / 8192.0;
		debugf("bars=%4.2f temp=%2.2f\r", barsf, tempf);
	}
}


#define FILTER_SIZE		 25

static
	uint32_t filter(uint32_t x)
	{
		static uint32_t history[FILTER_SIZE];
		static int in = 0, count = 0;
		static int32_t sum = 0;
		sum -= history[in];
		history[in] = x;
		sum += x;
		in = (in + 1) % FILTER_SIZE;
		if (count < FILTER_SIZE) count++;
		return (sum + count / 2) / count;
	}


int Barometer_read2x16(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 4 || !This->ready) return 0;
	uint8_t bar[3];
	uint8_t temp[2];
	uint32_t rawbar;
	Twiface_readReg(This, BM1383_REG_PRESSURE, bar, sizeof bar);
	Twiface_readReg(This, BM1383_REG_TEMPERATURE, temp, sizeof temp);
	//printValues(bar, (int16_t)temp[0] * 256 + temp[1]);
	rawbar = ((uint32_t)bar[0]<<16) | ((uint32_t)bar[1]<<8) | (bar[2]<<2);
	rawbar = filter(rawbar);
	int16_t temp88 = ((int16_t)temp[0] * 256 + temp[1])<<3;
	uint16_t bar115;// = (uint16_t)bar[0] * 256 + bar[1] + (((uint16_t)bar[2] + 32)>>6);
	bar115 = (rawbar + 128)>>8;
	buffer[0] = bar115 >> 8;
	buffer[1] = bar115 & 0xff;
	buffer[2] = temp88 >> 8;
	buffer[3] = temp88 & 0xff;
	//debuglnf("bars_ave=%4.2f temp88=%4.2f", rawbar / 8192.0, temp88 / 256.0);
	startMeasurement();
	return 4;
}


#endif
