/** @file
 *
 * @brief Gyroscope support for iProtoXi nRF51822 firmware.
 */

#include <stdint.h>
#include <stdbool.h>
#include "hw_select.h"
#include "twiface.h"


#if defined(GYR_NONE) || defined(GYR_NULL)


uint8_t Gyroscope_init()
{
	return 0;
}


bool Gyroscope_getInterruptSignal()
{
	return false;
}


int Gyroscope_readXYZ3x16andTime8(Xyz16time8 *xyz16time8)
{
	#ifdef GYR_NULL
	#error "GYR_NULL not implemented"
	#else
	return 0;
	#endif
}


int Gyroscope_readXYZ3x16(uint8_t *buffer, int bufferSize)
{
	#ifdef GYR_NULL
	if (bufferSize < 6) return 0;
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = 0;
	buffer[5] = 0;
	return 6;
	#else
	return 0;
	#endif
}


int Gyroscope_readXYZ3x12(uint8_t *buffer, int bufferSize)
{
	#ifdef GYR_NULL
	if (bufferSize < 5) return 0;
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = 0;
	return 5;
	#else
	return 0;
	#endif
}


#endif
