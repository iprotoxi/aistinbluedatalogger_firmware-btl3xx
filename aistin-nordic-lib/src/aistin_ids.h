/**
  @file aistin_ids.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#ifndef AISTIN_IDS
#define AISTIN_IDS


/* Aistin Unique Data Identifiers (AUDI) */


#define AISTIN_ID_8607_LOGGER_CONFIG			0x8607
#define _AISTIN_8607bit__SEND								0x01
#define _AISTIN_8607bit__LOG								0x02


#define AISTIN_ID_SENSORBASE							0xb000
#define AISTIN_ID_0001_NOTREALTIMEbit			0x0001

// Two-dimensional sensors: 0xb1XX
#define AISTIN_ID_B168_AIRPRESSURE				0xb168
#define AISTIN_ID_B188_AIRHUMIDITY				0xb188
#define AISTIN_ID_B1C8_SURFACETEMP				0xb1c8
#define AISTIN_ID_B1E8_CARBONDIOXIDE 		0xb1e8

// Three-dimensional sensors: 0xb3XX
#define AISTIN_ID_B328_ACCELERATION			0xb328
#define AISTIN_ID_B348_MAGNETICFIELD			0xb348
#define AISTIN_ID_B368_ROTATION					0xb368

#define AISTIN_ID_B32E_ACCELERATION_WITH_TIME			0xb32E
#define AISTIN_ID_B34E_MAGNETICFIELD_WITH_TIME			0xb34E
#define AISTIN_ID_B36E_ROTATION_WITH_TIME					0xb36E

// Combinations
#define AISTIN_ID_B316_ACC_MAG_ROT						0xb316

// Application specific codes
#define AISTIN_ID_B710_ACC12_MAG12_ROT12_IR	0xb710 // Customer case, real time data
#define AISTIN_ID_B711_ACC12_MAG12_ROT12_IR	0xb711 // Customer case, data from log
#define AISTIN_ID_B720_PRESS_HUM						0xb720 // Customer case, real time data
#define AISTIN_ID_B721_PRESS_HUM						0xb721 // Customer case, data from log
#define AISTIN_ID_B730_ACC_MAG_ROT_2TIME8		0xb730 // Kionix, real time data
#define AISTIN_ID_B731_ACC_MAG_ROT_2TIME8		0xb731 // Kionix, data from log


#endif
