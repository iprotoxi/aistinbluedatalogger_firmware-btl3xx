/**
  @file humidity.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include "humidity.h"
#include "hw_select.h"


#if defined(HUM_NULL) || defined(HUM_NONE)


bool Humidity_init(void)
{
	return true;
}


int Humidity_read2x16(uint8_t *buffer, int bufferSize)
{
    #ifdef HUM_NULL
	if (bufferSize < 4) return 0;
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	return 4;
    #else
    return 0;
    #endif
}


#endif
