/**
  @file nrfface.h
  @brief Part of Aistin library
  @copyright 2015 iProtoXi Oy
*/

#include "nrf_delay.h"


#define nrfAssert(x) APP_ERROR_CHECK(x)

#define clear(x) memset(&(x), 0, sizeof(x))
#define delay_ms(x)  nrf_delay_ms(x)
#define delay_us(x)  nrf_delay_us(x)
