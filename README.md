Aistin-nodic-public is a free version of iProtoXi's firmware for Aistin Blue board BTL3XX.

This project is created with free version of Keil uVision, with packages for Nordic Semiconductor's microcontroller nRF51822.

Contact jaakko.piippo@iprotoxi.fi for more information.
