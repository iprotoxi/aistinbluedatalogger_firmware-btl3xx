/**
  @file sampler_kionix.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <string.h>
#include "debug.h"
#include "hw_select.h"
#include "sampler.h"
#include "accelerom.h"
#include "gyroscope.h"
#include "magnetometer.h"
#include "barometer.h"
#include "softdevice_handler.h"
#include "leds.h"
#include "nrf_delay.h"
#include "app_timer.h"
#include "ble_control.h"
#include "protocol.h"
#include "humidity.h"
#include "aistin.h"
#include "aistin_ble.h"
#include "nrf_gpio.h"
#include "sampletimer.h"


#define PASSIVE_INTERVAL_ms  	 	2000
#define ACTIVE_INTERVAL_ms    	  10
#define ENVIRONMENT_DECIMATOR		   2


// makes some of the code a bit cleaner...
#define withSizeof(member)		member, sizeof(member)
#define clear(member)					memset(&member, 0, sizeof(member))


#ifdef SAMPLE_BY_SENSOR_INTERRUPT
static
	AistinItem ai;
static
	int8_t hasAccmagData, hasGyroData;

static
	void motionReady(bool isConnected)
	{
		Leds_off();
		Aistin_initItem(&ai, AISTIN_ID_B730_ACC_MAG_ROT_2TIME8);
		ai.dataSize = sizeof(ai.data._B730);
		Protocol_sendUsb_asciiAistin(&ai);
		if (isConnected) Protocol_sendBle(&ai);
		hasAccmagData = hasGyroData = 0;
	}

	
static
	void readMotion(bool isConnected)
	{
		static uint8_t seq;
		if (Accelerom_getInterruptSignal()) {
			Leds_setColor(1, 0, 0);
			Accelerom_readXYZ3x16andTime8(&ai.data._B730.accelerationWithTime);
			Leds_off();
			Magnetom_readXYZ3x16(&ai.data._B730.magneticfield);
			hasAccmagData++;
		}
		if (Gyroscope_getInterruptSignal()) {
			Leds_setColor(0, 1, 0);
			Gyroscope_readXYZ3x16andTime8(&ai.data._B730.rotationWithTime);
			Leds_off();
			hasGyroData++;
		}
		// We need to send the 'ai' when all data is there,
		// or the other sensor is interrupting second time:
		if (hasAccmagData /*&& hasGyroData*/) {
			if (!hasGyroData) {
				Leds_setColor(0, 0, 1);
				Gyroscope_readXYZ3x16andTime8(&ai.data._B730.rotationWithTime);
				Leds_off();
			}
			ai.data._B730.accelerationWithTime.rtcTime8 = seq & 127; // remove this line to get time stamp back
			ai.data._B730.rotationWithTime.rtcTime8 = seq & 127; // remove this line to get time stamp back
			motionReady(isConnected);
			seq++;
		}
		else
		if (hasAccmagData > 1) {
			clear(ai.data._B730.rotationWithTime.xyz16);
			ai.data._B730.rotationWithTime.rtcTime8 = 0xfa; // gyro failed
			motionReady(isConnected);
			hasAccmagData = hasGyroData = 0;
		}
		else
		if (hasGyroData > 1) {
			clear(ai.data._B730.accelerationWithTime.xyz16);
			clear(ai.data._B730.magneticfield);
			ai.data._B730.accelerationWithTime.rtcTime8 = 0xfb; // accl. failed
			motionReady(isConnected);
			hasAccmagData = hasGyroData = 0;
		}
	}


#else
#ifdef AISTIN_PROTOCOL_BLUE
static
	void readEnvironment(bool isConnected)
	{
		int i;
		AistinItem ai;
		Aistin_initItem(&ai, AISTIN_ID_B720_PRESS_HUM);
		i  = Barometer_read2x16(withSizeof(ai.data._B720.pressure));
		i += Humidity_read2x16(withSizeof(ai.data._B720.humidity));
		ai.dataSize = i;
		Protocol_sendUsb_asciiAistin(&ai); // debug
		if (isConnected) Protocol_sendBle(&ai);
	}
#endif
	
	
static
	void checkEnvironment(bool connected)
	{
		#ifdef AISTIN_PROTOCOL_BLUE
		static int decimate = 0;
		if (decimate == 0) {
			nrf_delay_ms(10); // Due to Basic-For-Android slowness...
			readEnvironment(connected);
		}
		decimate++;
		if (decimate >= ENVIRONMENT_DECIMATOR) decimate = 0;
		#endif
	}
	
		
static
	void readMotion(bool isConnected)
	{
		AistinItem ai;
		if (Sampletimer_getTimeout()) {
			Leds_setColor(1, 0, 0);
			#ifdef AISTIN_PROTOCOL_BLUE
			Aistin_initItem(&ai, AISTIN_ID_B710_ACC12_MAG12_ROT12_IR);
			Accelerom_readXYZ3x12(withSizeof(ai.data._B710.acceleration));
			Magnetom_readXYZ3x12(withSizeof(ai.data._B710.magneticfield));
			Gyroscope_readXYZ3x12(withSizeof(ai.data._B710.rotation));
			ai.dataSize = sizeof(ai.data._B710);
			#else
			//Aistin_initItem(&ai, AISTIN_ID_B316_ACC_MAG_ROT);
			Aistin_initItem(&ai, AISTIN_ID_B730_ACC_MAG_ROT_2TIME8);
			Accelerom_readXYZ3x16andTime8(&ai.data._B730.accelerationWithTime);
			Magnetom_readXYZ3x16(&ai.data._B730.magneticfield);
			Gyroscope_readXYZ3x16andTime8(&ai.data._B730.rotationWithTime);
			ai.dataSize = sizeof(ai.data._B730);
			#endif
			Protocol_sendUsb_asciiAistin(&ai);
			if (isConnected) Protocol_sendBle(&ai);
			checkEnvironment(isConnected);
		}
	}
#endif


static
bool checkConnection()
{
	static bool wasConnected;
	if (Ble_control_isConnected()) 
	{
		Leds_setColor(0, 0, 1);
		if (!wasConnected)
		{
			wasConnected = true;
			debug("connected!\r\n");
			nrf_delay_ms(200);
			#ifdef SAMPLE_BY_SENSOR_INTERRUPT
			#else
			Sampletimer_restart(ACTIVE_INTERVAL_ms);
			#endif
			//sendHistory();
		}
		return true;
	}
	else
	{
		if (wasConnected)
		{
			wasConnected = false;
			debugln("disconnected!");
			nrf_delay_ms(200);
			#ifdef SAMPLE_BY_SENSOR_INTERRUPT
			#else
			Sampletimer_restart(PASSIVE_INTERVAL_ms);
			#endif
		}
		//Leds_off();
	}
	return false;
}


void Sampler_init(void)
{
	// Timer must be initialized and started to get HW timer running.
	Sampletimer_init();
	Sampletimer_start(PASSIVE_INTERVAL_ms);
	#ifdef SAMPLE_BY_SENSOR_INTERRUPT
	#else
	#endif
}


void Sampler_run(void)
{
	bool connected;
	connected = checkConnection();
	readMotion(connected);
	Leds_off();
}
