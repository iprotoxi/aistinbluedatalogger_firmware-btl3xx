/**
  @file main_kionix.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "hw_select.h"
#include "hw_init.h"
#include "debug.h"
#include "app_timer.h"
#include "app_gpiote.h"
#include "nrf_delay.h"
#include "ble_control.h"
#include "softdevice_handler.h"
#include "leds.h"
#include "accelerom.h"
#include "magnetometer.h"
#include "gyroscope.h"
#include "extmem.h"
#include "sampler.h"
#include "humidity.h"
#include "barometer.h"
#include "twiface.h"
#include "rtc.h"
#include "timing.h"
#include "pstorage.h"
#include "ble_dfu.h"
#include "sensors.h"
#include "datalog.h"
#include "nrfface.h"


const char* BleName = BLE_NAME" 1k50";

#define AISTIN_SOS_WELLCOME_STR	 "\r\n*** Aistin-SOS - nRF51822 - \"%s\" ***"


#define APP_TIMER_PRESCALER      0  /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS     6  /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE  5  /**< Size of timer operation queues. */

#define APP_GPIOTE_MAX_USERS		 8


static
void initNrfSwModules(void)
{
	debugln("Initializing software modules...");
	#if 1
	APP_TIMER_INIT(
		APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS,
		APP_TIMER_OP_QUEUE_SIZE, false);
	#endif
	APP_GPIOTE_INIT(APP_GPIOTE_MAX_USERS);
	uint32_t retval;
	retval = pstorage_init();
	if(retval != NRF_SUCCESS) ;
}


static
void initExtmem(void)
{
	Extmem_init();
	//Extmem_dump(0, 400000);
	#ifdef EXTMEM_TEST
	Extmem_test(0, EXTMEM_SIZE);
	#endif
}


static
void initMCU(void)
{
	Hw_init();
	Debug_init();
	debuglnf(AISTIN_SOS_WELLCOME_STR, BleName);
}


static
	void bootReadyLedEffect(void)
	{
		for (int i = 0; i <= 16; i++) {
			int8_t c = i & 3;
			Leds_setColor(c == 0, c == 1, c == 2);
			nrf_delay_ms(100);
		}
	}
	

/**@brief Function for initializing everything at start-up
*/
static
void initAll()
{
		initMCU();
		Leds_init();
		Leds_setColor(1, 1, 1);
		Twiface_init();
		#if 0
		Twiface_scanForDevices(
			MCU_I2C_SDA, MCU_I2C_SCL, TWI_FREQUENCY_FREQUENCY_K100);
		Twiface_scanForDevices(
			MCU_BRD_RTS_I2C2_SDA, MCU_BRD_CTS_I2C2_SCL, TWI_FREQUENCY_FREQUENCY_K100);
		#endif
		initNrfSwModules();
		#if 1
		Sensors_init();
		initExtmem();
		//Datalog_init();
		Sampler_init();
		#endif
		Ble_control_init();
		//Rtc_init();
}


void powerManage(void)
{
		//Hw_init(); // TEST
		//Hw_setSensorPowers(false);
    //err_code = sd_power_system_off(); // TEST
		nrfAssert( sd_app_evt_wait() );
}


/**@brief Function for the application main entry.
 */
int main(void)
{
	(void)initAll();
	bootReadyLedEffect();
	debugln("Entering run loop.");
	Debug_close(); // comment away if you want debug messages
	while (1)
	{
		Timing_run();
		Sampler_run();
		powerManage();
	}
}
