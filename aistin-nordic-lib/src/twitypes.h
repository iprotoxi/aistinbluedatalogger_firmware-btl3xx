/**
  @file twitypes.h
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#ifndef TWITYPES_H
#define TWITYPES_H


#include <stdint.h>


// Data types used by TWI-sensors. Note that byte order may vary
// from sensor to sensor. Be sure to switch to big-endian when sending
// the data as Aistin messages (Aistin uses big-endian).


// unaligned 16-bit data from sensors
typedef uint8_t iusen16[2];
// unaligned 16-bit data from mcu
typedef uint8_t iumcu16[2];


typedef
	struct { 
		iusen16 x;
		iusen16 y;
		iusen16 z;
	} Xyz16;
	

typedef
	struct { 
		Xyz16 xyz16;
		iumcu16 rtcTime16; // byte order might differ from x, y, z since it's from mcu
	} Xyz16time16;

	
typedef
	struct { 
		Xyz16 xyz16;
		uint8_t rtcTime8;
	} Xyz16time8;

	
#endif
