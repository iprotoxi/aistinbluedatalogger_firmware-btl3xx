/**
  @file magnetometer_KMX62.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "nrf_delay.h"
#include "hw_select.h"
#include "accelerom.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "leds.h"
#include "kmx62.h"
#include "twiface.h"
#include "aistin.h"
#include "accmagncombo_KMX62.h"
#include "magnetometer.h"


#ifdef MAG_KMX62


extern TwiDevice KMX62;
static TwiDevice *This = &KMX62;


/**@brief Initializes Magnetometer-c-module (this).
 */
bool Magnetom_init()
{
	return Accmagncombo_KMX62_setupChip();
}


/**@brief Set sensor output rate.
 * @param[in]   minOutputHz  Minimum rate that is required
 * @return      Rate that was actually set.
 */
int Magnetom_setOutputRate(int minOutputHz)
{
	const int supportedHz[] = {
		6, 12, 25, 50, 100, 200, 400, 800, 1600
	};
	const int odcntlForHz[] = {
		KMX62_ODCNTL_OSM_6p25, KMX62_ODCNTL_OSM_12p5,
		KMX62_ODCNTL_OSM_25, KMX62_ODCNTL_OSM_50,
		KMX62_ODCNTL_OSM_100, KMX62_ODCNTL_OSM_200,
		KMX62_ODCNTL_OSM_400, KMX62_ODCNTL_OSM_800,
		KMX62_ODCNTL_OSM_1600
	};
	const int n = sizeof(supportedHz) / sizeof(int);
	int i;
	uint8_t get, set;
	uint8_t ov, dv;
	for (i = 0; i < n; i++) {
		if (minOutputHz <= supportedHz[i]) break;
	}
	if (i >= n) {
		debugln("requested output data rate out of range");
		i--;
	}
	set = odcntlForHz[i];
	// according to spec, sensors must be disabled first
	Twiface_readReg(&KMX62, KMX62_CTRL_REG2, &ov, sizeof ov);
	dv &= ~(KMX62_CTRL_REG2_MAG_EN | KMX62_CTRL_REG2_ACCEL_EN);
	Twiface_writeReg(&KMX62, KMX62_CTRL_REG2, dv);
	Twiface_readReg(&KMX62, KMX62_ODCNTL, &get, sizeof get);
	set |= get & (~KMX62_ODCNTL_OSM_MASK);
	Twiface_writeReg(&KMX62, KMX62_ODCNTL, set);
	Twiface_writeReg(&KMX62, KMX62_CTRL_REG2, ov);
	return supportedHz[i];
}


/**@brief Function for reading 3D-values X, Y and Z.
 */
static
bool readXYZ(uint8_t *xyz16)
{
	return Twiface_readReg(&KMX62, KMX62_MAG_XOUT_L, xyz16, 6);
}


int Magnetom_readXYZ3x16(Xyz16 *xyz16)
{
	if (!This->ready) return 0;
	uint8_t xyz[6];
	readXYZ(xyz);
	return Aistin_writeData3x16((uint8_t*)xyz16, xyz);
}


int Magnetom_readXYZ3x12(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 5 || !This->ready) return 0;
	uint8_t xyz[6];
	readXYZ(xyz);
	return Aistin_writeData3x12(buffer, xyz);
}


#endif
