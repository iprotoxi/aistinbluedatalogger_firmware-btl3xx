/**
  @file accelerometer.c
  @brief Part of Aistin library
	@copyright 2015 iProtoXi Oy
*/

#include <stdint.h>
#include "hw_select.h"
#include "accelerom.h"


#if defined(ACC_NONE) || defined(ACC_NULL)


bool Accelerom_getInterruptSignal()
{	
	return 0;
}


bool Accelerom_init()
{
	return 0;
}


int Accelerom_setOutputRate(int minOutputHz)
{
	return 0;
}


int Accelerom_readXYZ3x16andTime8(Xyz16time8 *xyz16time8)
{
	#ifdef ACC_NULL
	#error "ACC_NULL not implemented"
	#else
	return 0;
	#endif
}


int Accelerom_readXYZ3x16(uint8_t *buffer, int bufferSize)
{
	#ifdef ACC_NULL
	if (bufferSize < 6) return 0;
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = 0;
	buffer[5] = 0;
	return 6;
	#else
	return 0;
	#endif
}


int Accelerom_readXYZ3x12(uint8_t *buffer, int bufferSize)
{
	#ifdef ACC_NULL
	if (bufferSize < 5) return 0;
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = 0;
	return 5;
	#else
	return 0;
	#endif
}


#endif
