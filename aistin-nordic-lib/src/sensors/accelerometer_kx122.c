/** @file
 *
 * @brief Accelerometer support for nRF51822 firmware.
 */

#include <stdint.h>
#include <string.h>

#include "simple_uart.h"
#include "twi_master2.h"
#include "softdevice_handler.h"
#include "hw_select.h"
#include "accelerom.h"
#include "debug.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"
#include "aistin.h"
#include "nrfface.h"
#include "twiface.h"
#include "kx122.h"
#include "accelerometer_KX122.h"


#ifdef ACC_KX122


static
TwiDevice KX122, *This = &KX122;

#define INT_PIN		KX122_INT_PIN


static
app_gpiote_user_id_t m_app_gpiote_my_id;
static
int8_t interruptSignal;


static
	void clearInterrupt(void)
	{
		//uint8_t v;
		//Twiface_readReg(&KX122, KX122_INT1_L, &v, sizeof v);
	}


static
	void sensorInterrupt(uint32_t event_pins_low_to_high, uint32_t event_pins_high_to_low)
	{
		if (event_pins_high_to_low & (1 << INT_PIN)) interruptSignal = 1;
	}


static
	void initInterrupt(void)
	{
		nrf_gpio_cfg_sense_input(
			INT_PIN, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);
		nrfAssert( app_gpiote_user_register(
			&m_app_gpiote_my_id, (1 << INT_PIN), (1 << INT_PIN), sensorInterrupt) );
		nrfAssert( app_gpiote_user_enable(m_app_gpiote_my_id) );
	}


static
	void setDataReadyInterrupt(void)
	{
		#if 1
		// Disable interrupt lines for TEST PURPOSES
		//Twiface_writeReg(&KX122, KX122_INT_CTRL1, 0x00);	
		//Twiface_writeReg(&KX122, KX122_INT_CTRL5, 0x00);			
		Twiface_writeReg(&KX122, KX122_INT_CTRL1, 0x20);	
		Twiface_writeReg(&KX122, KX122_INT_CTRL5, 0x20);			
		#else
		// TODO!!!TODO!!!TODO!!! BELOW IS THE CODE FROM GYRO
		uint8_t v;
		//Twiface_readReg(&KXG03, KXG03_INT1_SRC1, &v, sizeof v);
		//v |= KXG03_INT1_SRC1_INT1_DRDY_GYRO;
		//Twiface_writeReg(&KXG03, KXG03_INT1_SRC1, v);	
		Twiface_readReg(&KXG03, KXG03_INT_PIN1_SEL, &v, sizeof v);
		v |= KXG03_INT_PIN1_SEL_DRDY_GYRO_P1;
		Twiface_writeReg(&KXG03, KXG03_INT_PIN1_SEL, v);	
		//Twiface_readReg(&KXG03, KXG03_INT_PIN_CTL, &v, sizeof v);
		//v &= ~KXG03_INT_PIN_CTL_IEA1;
		//v |= KXG03_INT_PIN_CTL_IEN1;
		v = KXG03_INT_PIN_CTL_IEN1 |
				KXG03_INT_PIN_CTL_IEA1_ACTIVE_LOW /*|
				KXG03_INT_PIN_CTL_IEL1_PULSED_50US*/;
		Twiface_writeReg(&KXG03, KXG03_INT_PIN_CTL, v);
		Twiface_writeReg(&KXG03, KXG03_INT_MASK1, 
			KXG03_INT_MASK1_DRDY_GYRO | KXG03_INT_MASK1_DRDY_ACC); // enable int	
		KXG03_clearInterrupt();
		//Twiface_writeReg(This, KXG03_WAKE_SLEEP_CTL1, 
		//	KXG03_WAKE_SLEEP_CTL1_MAN_WAKE | KXG03_WAKE_SLEEP_CTL1_OWUF_100);
		#endif
	}


/**@brief Return true if interrupt has been occurred since the previous call.
 */
bool KX122_getInterruptSignal()
{
	
	uint8_t nested;
	sd_nvic_critical_region_enter(&nested);
	bool is = (bool)interruptSignal;
	if (is) interruptSignal = 0;
	sd_nvic_critical_region_exit(nested);
	if (is) clearInterrupt();
	return is;
}


static
	void setupChip()
	{
		// Init KX122 streaming data
		Twiface_writeReg(This, KX122_CTRL_REG2, KX122_CTRL_REG2_SRST); // SRST set to 1	
		delay_ms(RESET_DELAY_MSEC);
		#if 0
		bool reset_pending = true;
		uint8_t reg;
		do {
				 Twiface_readReg(This, KX122_CTRL_REG2, &reg, sizeof reg);
				 if ((reg & KX122_CTRL_REG2_SRST)) {
							delay_ms(RESET_DELAY_MSEC);
					} else {
						reset_pending = false;
					}
		} while (reset_pending);
		#endif		
		#if 0
		/* ensure that PC1 is cleared before updating control registers */
		Twiface_writeReg(This, KX122_CTRL_REG1, 0);
		/* Set res and g-range */
		Twiface_writeReg(This, KX122_CTRL_REG1, (KX122_RES_16bit | KX122_GRP4_G_4G));
		/* Set odr */
		Twiface_writeReg(This, KX122_DATA_CTRL, KX122_ODR100F);
		/* Read ctrl1 and set pci bit On */
		uint8_t read_reg;
		Twiface_readReg(This, KX122_CTRL_REG1, &read_reg, sizeof read_reg);		
		read_reg |= KX122_PC1_ON;
		/* Write ctrl */
		Twiface_writeReg(This, KX122_CTRL_REG1, read_reg);	
		#endif
	}


/**@brief Function for reading 3D-values X, Y and Z. This is used
 * by/via TwiDevice.read* -functions.
 */
static
bool readXyz16(Xyz16 *xyz16)
{
	return
		Twiface_readReg(
			This, KX122_XOUT_L, (uint8_t*)xyz16, sizeof(Xyz16));
}


int KX122_readXYZ3x16(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 6 || !This->ready) return 0;
	Xyz16 xyz;
	readXyz16(&xyz);
	return Aistin_writeData3x16(buffer, &xyz.x[0]);
}


int KX122_readXYZ3x16andTime8(Xyz16time8 *xyz16time8)
{
	#if 1
	return Twidevice_readXyz16time8(This, xyz16time8);
	#else
	if (!This->ready) return 0;
	readXyz16(&timed8Xyz16->xyz16);
	Aistin_fixEndian(&timed8Xyz16->xyz16);
	timed8Xyz16->rtcTime8 = Timing_read1000Hz8();
	return sizeof(Timed8Xyz16);
	#endif
}


int KX122_readXYZ3x12(uint8_t *buffer, int bufferSize)
{
	if (bufferSize < 5 || !This->ready) return 0;
	Xyz16 xyz;
	readXyz16(&xyz);
	return Aistin_writeData3x12(buffer, &xyz.x[0]);
}


/**@brief Initializes Accelerom-c-module (this).
 */
bool KX122_init()
{
	Twiface_initDevice(
		This, KX122_NAME, KX122_TWIUNIT, KX122_TWIFREQ,
		KX122_DEVADDR, KX122_SDA, KX122_SCL);
	This->readXyz16 = readXyz16;
	setupChip();
	#ifdef SAMPLE_BY_SENSOR_INTERRUPT
	//initInterrupt();
	setDataReadyInterrupt();
	#endif
	return true;
}


#endif


#if 0
// Old code


/**@brief Function for reading 3D-acceleration values X, Y and Z.
 */
void KX122_readXYZCXYZ(uint8_t *_12Bytes)
{
	uint8_t xyz[12];
	readReg(KX122_XOUT_L, xyz, sizeof xyz);
	
	_12Bytes[0] = xyz[1];
	_12Bytes[1] = xyz[0];
	_12Bytes[2] = xyz[3];
	_12Bytes[3] = xyz[2];
	_12Bytes[4] = xyz[5];
	_12Bytes[5] = xyz[4];
	_12Bytes[6] = xyz[7];
	_12Bytes[7] = xyz[6];
	_12Bytes[8] = xyz[9];
	_12Bytes[9] = xyz[8];
	_12Bytes[10] = xyz[11];
	_12Bytes[11] = xyz[10];
}

/**@brief Function for reading 3D-acceleration values X, Y and Z.
 */
void KX122_readXYZ(uint8_t *_6Bytes)
{
	uint8_t acc_xyz[6];
	readReg(KX122_XOUT_L, acc_xyz, sizeof acc_xyz);
	_6Bytes[0] = acc_xyz[1];
	_6Bytes[1] = acc_xyz[0];
	_6Bytes[2] = acc_xyz[3];
	_6Bytes[3] = acc_xyz[2];
	_6Bytes[4] = acc_xyz[5];
	_6Bytes[5] = acc_xyz[4];	
}


#endif
